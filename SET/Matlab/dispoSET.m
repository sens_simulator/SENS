% RECUPERATION DES DONNEES ET PREPARATION POUR LES CALCULS

clear
close all
clc

%--------------------------------------------------------------------------
% À MODIFIER

%choix du dispositif
typedispo='44-12-10-50-pp';

% Dot, oxyde et electrodes
dot='Si';
ox='SiO2';
elec='Al';

% Dossier où se trouvent les fichiers de sortie du calcul de structure électronique
dossier_origine='/Users/gianina.frimu/Documents/data_SENS/SET/Elec_Struc/';

% Dossier où vont être stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';

% Dossier où sont stockés les scripts Matlab
dossier_scripts='/Users/gianina.frimu/Documents/SENS/SET/Matlab/';
%--------------------------------------------------------------------------


dossier1=[dossier_origine,typedispo,'/'];
dossier2=[dossier_destination,typedispo,'/'];
if isfolder(dossier2)==0,mkdir(dossier2),end

%Charger les paramètres  des îlots Silicium et des barrières aluminium
load([dossier_scripts,'constantes_',elec,dot,ox,'.mat'])

%Charger les données du fichier dispo.txt...
dispo=load([dossier1,'dispo.txt']);


Nptx=dispo(1); %nombre de points suivant l'axe Ox
Npty=dispo(1);
Npty2=dispo(2);
Nptz=dispo(1);
a=dispo(3); %rayon du dot (A)
hG=dispo(4); %epaisseurs de barri�res (A)
hD=dispo(5);
L=2*a+hG+hD;
hgr=dispo(6);
Nemax=dispo(7);%Nombre maximum d'électrons dans le dot
Nbpolar=dispo(8); %nombre de tensions V_DS
Nbpolarg=dispo(9); %nombre de tensions V_GS
V=zeros(Nbpolar,1);
for i=1:Nbpolar,
    V(i)=dispo(9+i);
end
Vg=zeros(Nbpolarg,1);
for j=1:Nbpolarg,
    Vg(j)=dispo(9+Nbpolar+j);
end

V2=zeros(1,10*length(V)-9);
V2g=zeros(1,10*length(Vg)-9);

% Interpolation des polarisations
if length(V)==1, % Pas d'interpolation...
    V2=V;
else
    for iv=1:length(V2)-1
        index=ceil(iv/10);
        V2(iv)=V(index)+(V(index+1)-V(index))/10*mod(iv-1,10);
    end
        %V2=V(1):(V(2)-V(1))/10:V(end)+eps;
end
V2(end)=V(end);


if length(Vg)==1, % Pas d'interpolation...
    V2g=Vg;
else
    for ivg=1:length(V2g)-1
        index=ceil(ivg/10);
        V2g(ivg)=Vg(index)+(Vg(index+1)-Vg(index))/10*mod(ivg-1,10);
    end
end
V2g(end)=Vg(end);
%utile pour l'interpolation des �nergies, ndgrid � la place de meshgrid.
[Vi1,Vi1g]=ndgrid(V,Vg);
[Vi2,Vi2g]=ndgrid(V2,V2g);

%sauvegarde du dispo .mat
save([dossier2,typedispo,'.mat'],...
    'Nptx','Npty','Npty2','Nptz','a','hG','hD','hgr','Nemax','Nbpolar',...
    'Nbpolarg','V','Vg','V2','V2g','Vi1','Vi1g','Vi2','Vi2g','L','dot','ox','elec')
save([dossier2,'V.mat'],'V','Vg','V2','V2g','Vi1','Vi1g','Vi2','Vi2g');



%Convertir les données + mise dans les matrices

%%axe - les axes sont désormais en mètres
axex=load([dossier1,'axe/axex_a',num2str(a,'%1.0f'),'.txt'])*abohr;
axex2=axex(1):(axex(2)-axex(1))/2:axex(end);
axey=load([dossier1,'axe/axey_a',num2str(a,'%1.0f'),'.txt'])*abohr;
axe2y=load([dossier1,'axe/axey2_a',num2str(a,'%1.0f'),'.txt'])*abohr;
axez=load([dossier1,'axe/axez_a',num2str(a,'%1.0f'),'.txt'])*abohr;
[X,Y,Z]=meshgrid(axex,axey,axez);
[X2,Y2,Z2]=meshgrid(axex2,axey,axez);
%sauvegarde des axes .mat
save([dossier2,'Axe.mat'],...
    'axex','axey','axe2y','axez','axex2','X','Y','Z','X2','Y2','Z2');


%  Initialisation
Vpoli=cell(Nbpolar,Nbpolarg);
Vpoli2=cell(Nbpolar,Nbpolarg);
Psii=cell(Nbpolar,Nbpolarg,Nemax);

Psimax=cell(Nbpolar,Nbpolarg,Nemax);
v0g0=zeros(Nbpolar,Nbpolarg);
eG=zeros(Nbpolar,Nbpolarg);
eD=zeros(Nbpolar,Nbpolarg);
v0d0=zeros(Nbpolar,Nbpolarg);
Ei=zeros(Nbpolar,Nbpolarg,Nemax);
ymax=zeros(Nbpolar,Nbpolarg,Nemax);
xmax=zeros(Nbpolar,Nbpolarg,Nemax);
a_app=zeros(Nbpolar,Nbpolarg,Nemax);
%v0gmax=zeros(Nbpolar,Nbpolarg,Nemax);
%v0dmax=zeros(Nbpolar,Nbpolarg,Nemax);
%VpolSee=cell(Nbpolar);

% %Vpol
for iv=1:length(V),
    for ivg=1:length(Vg),
        disp (['Vpol pour Vg=',num2str(Vg(ivg),'%2.5f'),' V= ',num2str(V(iv),'%2.5f')])
        Vpol=load([dossier1,'Vpol/Vpol_V',num2str(V(iv),'%2.5f'),'_Vg',num2str(Vg(ivg),'%2.5f'),'.txt']);
        Vpoli{iv,ivg}=reshape(Vpol,Nptx,Npty,Nptz);
        %Vpol en milieu de barri�re x=0
         v0g0(iv,ivg)=interp3(axex,axey,axez,Vpoli{iv,ivg},(-a-hG/2)*10^-10,0,0);
         %v0g0a=interpn(axex,axey,axez,Vpoli{iv,ivg},(-a)*10^-10,0,0);
         %v0d0a=interpn(axex,axey,axez,Vpoli{iv,ivg},(a)*10^-10,0,0);
         v0d0(iv,ivg)=interp3(axex,axey,axez,Vpoli{iv,ivg},(a+hD/2)*10^-10,0,0);
        %Vpol complet(y jusqu'� la grille)
         %eG(iv,ivg)=(hG/2)*(v0g0a);
         %eD(iv,ivg)=(hG/2)*(v0d0a-V(iv));
        disp (['Vpol2 pour Vg=',num2str(Vg(ivg)),' V= ',num2str(V(iv))])
        Vpol2=load([dossier1,'Vpol2/Vpol2_V',num2str(V(iv),'%2.5f'),'_Vg',num2str(Vg(ivg),'%2.5f'),'.txt']);
        Vpoli2{iv,ivg}=reshape(Vpol2,Nptx,Npty2,Nptz);

        for ie=1:Nemax,
            % / Energies \
            disp (['E pour Vg=',num2str(Vg(ivg)),'V V= ',num2str(V(iv)),...
                'V et Nbe =',num2str(ie)])
            E=load([dossier1,'E/e_e',num2str(ie),'_V',num2str(V(iv),'%2.5f'),'_Vg',num2str(Vg(ivg),'%2.5f'),'.txt']);
            Ei(iv,ivg,ie)=E(1,2)*Ebohr;

            % / PSI \
            disp (['Psi pour Vg=',num2str(Vg(ivg)),'V V= ',num2str(V(iv)),...
                'V et Nbe =',num2str(ie)])
            Psi=load([dossier1,'Psi/psi_e',num2str(ie),'_V',num2str(V(iv),'%2.5f'),'_Vg',num2str(Vg(ivg),'%2.5f'),'.txt']);
            Psi=reshape(Psi,Nptx,Npty,Nptz);
            Psii{iv,ivg,ie}=Psi*Psibohr;


%              determination des indices pour Psi^2 max
%{
            Psim=find(abs(Psi.^2)==max(max(max(abs(Psi.^2)))),1);
            Psimax{iv,ivg,ie}=[mod(mod(Psim,40*40),40);...
                floor(mod(Psim,Nptx*Npty)/Nptx);floor(Psim/(Npty*Nptx))];
            xmax(iv,ivg,ie)=axex(Psimax{iv,ivg,ie}(1));
            ymax(iv,ivg,ie)=axey(Psimax{iv,ivg,ie}(2));
            a_app(iv,ivg,ie)=sqrt(a^2-(ymax(iv,ivg,ie)*1e10)^2);
%             recuperation des Vpol
%             � x=hG/2,y=(Psimax)
            %v0gmax(iv,ivg,ie)=interpn(axex,axey,axez,Vpoli{iv,ivg},...
             %   (-a-hG/2)*10^-10,axey(Psimax{iv,ivg,ie}(2)),0);
            %v0dmax(iv,ivg,ie)=interpn(axex,axey,axez,Vpoli{iv,ivg},...
              %  (a+hD/2)*10^-10,axey(Psimax{iv,ivg,ie}(2)),0);
%}
        end
    end
    %{
    %  creation de l'approx lin�aire de J.S�e de Vpol.
    for xx=1:length(axex),
        if axex(xx)*10^10<(-a-hG),
            VpolSee{iv}(xx)=0;
        elseif axex(xx)*10^10>=(-a-hG) && axex(xx)*10^10<(-a),
            VpolSee{iv}(xx)=-V(iv)/(L+2*a*(epsox/epsdot-1))*(axex(xx)*10^10+a+hG);
        elseif axex(xx)*10^10>=-a && axex(xx)*10^10<a,
            VpolSee{iv}(xx)=-V(iv)/(L+2*a*(epsox/epsdot-1))*(epsox*axex(xx)/epsdot)-V(iv)/(L+2*a*(epsox/epsdot-1))*(epsox*a/epsdot+hG);
        elseif axex(xx)*10^10>=a && axex(xx)*10^10<(a+hD),
            VpolSee{iv}(xx)=V(iv)/(L+2*a*(epsox/epsdot-1))*(-axex(xx)+a+hD)-V(iv);
        else
            VpolSee{iv}(xx)=-V(iv);
        end
    end
    %}
end

Vpol=Vpoli;
clear Vpoli
Vpol2=Vpoli2;
clear Vpoli2
E=Ei;
clear Ei
Psi=Psii;
clear Psii


%interpolation �nergies
E2=zeros(length(V2),length(V2g),Nemax);
v0g02=zeros(length(V2),length(V2g));
v0d02=zeros(length(V2),length(V2g));
%v0gmax2=zeros(length(V2),length(V2g),Nemax);
%v0dmax2=zeros(length(V2),length(V2g),Nemax);
if length(V)==1,
    v0g02(:,:)=interp1(Vi1g,v0g0(:,:),Vi2g);
    v0d02(:,:)=interp1(Vi1g,v0d0(:,:),Vi2g);
    for in=1:Nemax,
        E2(:,:,in)=interp1(Vi1g,E(:,:,in),Vi2g);
        %v0gmax2(:,:,in)=interp1(Vi1g,v0gmax(:,:,in),Vi2g);
        %v0dmax2(:,:,in)=interp1(Vi1g,v0dmax(:,:,in),Vi2g);
    end
elseif length(Vg)==1,
    v0g02(:,:)=interp1(Vi1,v0g0(:,:),Vi2);
    v0d02(:,:)=interp1(Vi1,v0d0(:,:),Vi2);
    for in=1:Nemax,
        E2(:,:,in)=interp1(Vi1,E(:,:,in),Vi2);
        %v0dmax2(:,:,in)=interp1(Vi1,v0dmax(:,:,in),Vi2);
        %v0gmax2(:,:,in)=interp1(Vi1,v0gmax(:,:,in),Vi2);

    end
else
    v0g02(:,:)=interpn(Vi1,Vi1g,v0g0(:,:),Vi2,Vi2g,'cubic');
    v0d02(:,:)=interpn(Vi1,Vi1g,v0d0(:,:),Vi2,Vi2g,'cubic');
    for in=1:Nemax,
        E2(:,:,in)=interpn(Vi1,Vi1g,E(:,:,in),Vi2,Vi2g,'cubic');
        %v0gmax2(:,:,in)=interp2(Vi1,Vi1g,v0gmax(:,:,in),Vi2,Vi2g);
        %v0dmax2(:,:,in)=interp2(Vi1,Vi1g,v0dmax(:,:,in),Vi2,Vi2g);
    end
end


%Potentiels (conduction, Fermi, mi-barri�re)
EFD=repmat(Phidot-Phimetal,[length(V2),length(V2g),Nemax])-repmat(V2',[1,length(V2g),Nemax]);
ECD=EFD-Efmetal;
EFG=repmat(Phidot-Phimetal,[length(V2),length(V2g),Nemax]);
ECG=EFG-Efmetal;
Vbarr=repmat(Phidot-Phiox,[length(V2),length(V2g),Nemax]);
%V0GSee=-repmat(V2',[1,length(V2g),Nemax])/(L+2*a*(epsox/epsdot-1))*hG/2;
%V0DSee=repmat(V2',[1,length(V2g),Nemax])*(1/(L+2*a* (epsox/epsdot-1))*(hD/2)-1);
V0G0=repmat(v0g02,[1,1,Nemax]);
V0D0=repmat(v0d02,[1,1,Nemax]);
%V0GMAX=v0gmax2;
%V0DMAX=v0dmax2;




disp('Sauvegardes...')
%--------------------------------------------------------------------------
%-------- . MAT
%--------------------------------------------------------------------------
save([dossier2,'Psi.mat'],'Psi','-v7.3');

save([dossier2,'Vpol.mat'],'Vpol','Vpol2');
save([dossier2,'Energies.mat'],'E','EFD','EFG','ECD','ECG','E2','V0G0',...
    'V0D0','Vbarr');
disp('dispo, fini')

%FIGURE
figure
Color=jet(Nemax);
for in=1:Nemax,
    surf(Vg,V,E(:,:,in)); hold on
end

figure
plot(V2g,V0G0(1,:,1),'k','LineWidth',2);
hold on
plot(V2g,V0D0(1,:,1),'g','LineWidth',2);
legend('V0G0','V0D0')

figure
plot(Vg,abs(eG(1,:)),'k'); hold on
plot(Vg,abs(eD(1,:)),'r');
