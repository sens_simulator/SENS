% calcul de l'épaisseur effective des barrières

clear;close all;clc
tic

%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-10-50-pp';
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

%Constantes
eps0=8.8e-12; %en ev
qe=1.602176487e-19; %en C
hb=1.05459e-34/qe; % en eV.s
kb=8.617e-5; %en eV
me=9.1e-31; %en kg

% Constantes physiques du mat�riau (en eV)

% Constantes déchange (UA => SI)
abohr=0.529177e-10;
Ebohr=27.211; %eV
Psibohr=(0.529177e-10)^(-3/2);

%--------------------------------------------------------------------------
load([dossier,dispo,'/',dispo,'.mat'])
load([dossier,dispo,'/Axe.mat']);

xp=axex2;
dx=xp(2)-xp(1);

for i=2:length(axex2)-1
    if ((xp(i-1)+xp(i))/2<=(-a-hG)*1e-10)&&((xp(i)+xp(i+1))/2>(-a-hG)*1e-10)
        iminG=i+1; %dans l'oxyde
    elseif (xp(i)<-a*1e-10)&&(xp(i+1)>=-a*1e-10)
        imaxG=i; %dans l'oxyde
    elseif (xp(i)>a*1e-10)&&(xp(i-1)<=a*1e-10)
        iminD=i; %dans l'oxyde
    elseif ((xp(i-1)+xp(i))/2<(a+hD)*1e-10)&&((xp(i)+xp(i+1))/2>=(a+hD)*1e-10)
        imaxD=i-1; %dans l'oxyde
    end
end

hG_eff=xp(imaxG)-xp(iminG)+dx;
hD_eff=xp(imaxD)-xp(iminD)+dx;
dh=dx/2;



save([dossier,dispo,'/epaisseur.mat'],...
    'hG','hD','hG_eff','hD_eff','dh');