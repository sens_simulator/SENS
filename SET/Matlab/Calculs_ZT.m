% calcul Ge, alpha par Seebeck, facteur de puissance et figure de mérite

clear
close all
clc

%--------------------------------------------------------------------------
% À MODIFIER
H='discret'; % élargissement des niveaux 
%H='discret';
% Dossier où sont stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
dispos='44-12-10-50-pp'; % diamètre-épaisseur oxyde source-oxyde drain-oxyde grille
T=100; % temperature en K
%dT=[0:2:20]; % Difference de temperature en K
dT=[0 5];
%--------------------------------------------------------------------------


qe=1.602176487e-19;

% Charger le fichier de courant pour dT = 0
dossierm=[dossier_destination,dispos,'/',H,'/'];
load([dossierm,'IMA_T',num2str(T),'dT0.mat']);

% INDICE TEL QUE V_DS(iv) = 0
[Z,iv]=min(abs(V2)); %indice iv tel que V2(iv) = 0

Ge=zeros(length(V2g),1);
alpha=zeros(length(V2g),length(dT));
KG=zeros(length(V2g),length(dT));
KD=zeros(length(V2g),length(dT));
f=zeros(length(V2g),length(dT));
ZT=zeros(length(V2g),length(dT));
Voc=zeros(length(V2g),length(dT));

[Z,ivgmax]=min(abs(V2g-5)); % On se limite à Vgs=5V (soucis au-delà)

% Calcul de Ge
I=IMA;
for ivg=1:ivgmax,
    Ge(ivg)=(I(iv+10,ivg)-I(iv-10,ivg))/(V2(iv+10)-V2(iv-10)); %S
end
save([dossierm,'Ge_T',num2str(T),'.txt'],'Ge','V2g')

% Calcul de Voc, K, facteur de puissance et ZT

for iT=1:length(dT),
    %disp(['dT=',num2str(dT(iT))])
    load([dossierm,'IMA_T',num2str(T),'dT',num2str(dT(iT)),'.mat']);
    I=IMA;IQG=(IQFGd-IQFdG)*qe;IQD=(IQFdD-IQFDd)*qe; %J
    for ivg=1:ivgmax,
        Voc(ivg,iT)=-interp1(I(:,ivg),V2,0,'pchip'); %V
        if (-Voc(ivg,iT)<V2(1)||-Voc(ivg,iT)>V2(end)), Voc(ivg,iT)=NaN; end
        alpha(ivg,iT)=(Voc(ivg,iT))/dT(iT); %V/K
        KG(ivg,iT)=interp1(V2,IQG(:,ivg),-Voc(ivg,iT),'pchip')/dT(iT); %W/K
        KD(ivg,iT)=interp1(V2,IQD(:,ivg),-Voc(ivg,iT),'pchip')/dT(iT); %W/K
        f(ivg,iT)=Ge(ivg)*alpha(ivg,iT)*alpha(ivg,iT); %W/K2
        ZT(ivg,iT)=T*Ge(ivg)*alpha(ivg,iT)*alpha(ivg,iT)/(KG(ivg,iT));
    end
end

save([dossierm,'ZT_T',num2str(T),'.mat'],...
    'Ge','V2g','V2','dT','alpha','KG','KD','f','ZT','Voc');

%%
% alpha en fonction de Vgs pour différents dT
[Z,ivgmax]=min(abs(V2g-5));
figure
plot(V2g(1:ivgmax),alpha(1:ivgmax,2:end)*1e3,'--','MarkerSize',4,'LineWidth',2)
legend('\DeltaT = 0K','\DeltaT = 2K','\DeltaT = 4K','\DeltaT = 6K','\DeltaT = 8K','\DeltaT = 10K','\DeltaT = 12K','\DeltaT = 14K','\DeltaT = 16K','\DeltaT = 18K','\DeltaT = 20K')
xlabel('gate voltage (V)','FontName','Latin Modern Roman','FontSize',15)
ylabel('Seebeck coefficient (mV/K)','FontName','Latin Modern Roman','FontSize',15)
legend('FontName','Latin Modern Roman');
%ylim([-0.8 1.2])

% Voc en fonction de dT pour Vgs=2.67V
[Z,ivg_]=min(abs(V2g-2.67));
figure
plot(dT,Voc(ivg_,:)*1e3,'-or','LineWidth',2,'MarkerSize',4)
xlabel('temperature gradient (K)','FontName','Latin Modern Roman','FontSize',15)
ylabel('open circuit voltage (mV)','FontName','Latin Modern Roman','FontSize',15)


