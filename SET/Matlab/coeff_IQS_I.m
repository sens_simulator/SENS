% Régression quadratique des flux de chaleur en fonction du courant
% électronique pour le calcul des coefficients de non-linéarité bêtas

clear;
%close all;
clc

qe=1.602176487e-19;


%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
% Dossier où sont stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
H='discret';
%H='A_0.05'; % élargissement en kB*T
T=100; %K
deltaT=5; %K
%--------------------------------------------------------------------------

% Chargement des fichiers utiles
dossierm=[dossier_destination,dispo,'/',H,'/'];
load([dossierm,'IMA_T',num2str(T),'dT',num2str(deltaT),'.mat']);

% Régression polynomiale
[Z,ivgmax]=min(abs(V2g-5)); % on se limite à Vgs=5V (soucis au-delà)

IQG=(IQFGd-IQFdG)*qe; %J
IQD=(IQFdD-IQFDd)*qe;

pIQG=zeros(length(V2g),3);
betaG=zeros(length(V2g),1);
peltierG=zeros(length(V2g),1);
pIQD=zeros(length(V2g),3);
betaD=zeros(length(V2g),1);
peltierD=zeros(length(V2g),1);

for ivg=1:length(V2g)
    pIQG(ivg,:)=polyfit(IMA(:,ivg)*1e12,IQG(:,ivg)*1e15,2);
    pIQD(ivg,:)=polyfit(IMA(:,ivg)*1e12,IQD(:,ivg)*1e15,2);
    betaG(ivg)=-pIQG(ivg,1)*1e9; %Ohms
    peltierG(ivg)=pIQG(ivg,2)/TG; %mV/K
    betaD(ivg)=pIQD(ivg,1)*1e9; %Ohms
    peltierD(ivg)=pIQD(ivg,2)/TD; %mV/K
end

% sauvegarde des coefficients calculés
save([dossierm,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat'],'pIQG',...
    'pIQD','betaG','betaD','peltierG','peltierD');

