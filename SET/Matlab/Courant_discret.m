clear;close all;clc
tic

%--------------------------------------------------------------------------
% À MODIFIER
dispos='44-12-10-50-pp'; % diamètre-épaisseur oxyde source-oxyde drain-oxyde grille
% Dossier où sont stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
T=100; %K température moyenne
dT=[0 5];%K différence de température entre la source et le drain
%--------------------------------------------------------------------------

%Constantes
eps0=8.8e-12; %eV
qe=1.602176487e-19; %C
hb=1.05459e-34/qe; %eV.s
kb=8.617e-5; %eV
me=9.1e-31; %kg

% Constantes physiques du matériau (en eV)
% modifiables
mox=0.5*me;
epsox=3.8;
epsdot=12;
Phimetal=4.1;Phidot=4;Phiox=0.9; %eV
Efmetal=11.6; %eV
Velec=1; %volume des electrodes

Vbarmetalox=Phimetal-Phiox;Vbardotox=Phidot-Phiox;

% Constantes déchange (UA => SI)
abohr=0.529177e-10;
Ebohr=27.211; %eV
Psibohr=(0.529177e-10)^(-3/2);

%--------------------------------------------------------------------------
load([dossier_destination,dispos,'/',dispos,'.mat'])
%--------------------------------------------------------------------------
Nmin=0;
Nmax=Nemax;
%--------------------------------------------------------------------------
tmax=10;

%Chargement des donn�es necessaires au calcul des GAMMAs
load([dossier_destination,dispos,'/Energies.mat']);E1=E;
clear E ECD ECG Vbarr V0DSee V0GSee V0DMAX V0GMAX
load([dossier_destination,dispos,'/Axe.mat']);

%potentiels
EFG = Phidot-Phimetal;
ECG = EFG-Efmetal;
Vbarr = Phidot-Phiox;

% Mise en forme des donn�es et extraction des tailles de tableaux
xp=axex; yp=axey; zp=axez;
xp2=xp(1):abs(xp(2)-xp(1))/2:xp(end);xp2=xp2';
[X,Y,Z]=meshgrid(xp,yp,zp);[X2,Y2,Z2]=meshgrid(xp2,yp,zp);
[Xn,Yn,Zn]=ndgrid(xp,yp,zp);[Xn2,Yn2,Zn2]=ndgrid(xp2,yp,zp);
Psi=cell(Nbpolar,Nbpolarg,Nemax);


tic

%--------------------------------------------------------------------------
% Calcul des fr�quences de transferts puis du courant
%--------------------------------------------------------------------------



%Calcul des alpha, necessaires pour le calcul des integrales.

%Extraction des Psi à mi-barri�re
disp('Extraction Psi mi-barri�re')
if (exist([dispos,'/PSI_MIBAR.mat'],'file')==0),
    intpsiG=zeros(length(V),length(Vg),Nemax);
    intpsiD=zeros(length(V),length(Vg),Nemax);
    intdpsixG=zeros(length(V),length(Vg),Nemax);
    intdpsixD=zeros(length(V),length(Vg),Nemax);

    load([dossier_destination,dispos,'/Psi.mat']);

    for iv = 1:length(V),
        for ivg = 1:length(Vg),
            % TROUVER LES V2 CORRESPONDANTS AU V (IDEM VG)
            % INTERPOLATION SE FAIT APRES
            iv0=1+(iv-1)*10;
            ivg0=1+(ivg-1)*10;
            %disp(['iv0=',num2str(iv0),' ivg0=',num2str(ivg0)])

            for ne=1:Nemax,
                psi=(permute(abs(Psi{iv,ivg,ne}),[2,1,3]));
                psiG(:,:)=interp3(xp,yp,zp,psi,-(a+hG/2)*1e-10,yp,zp,'cubic');
                psiD(:,:)=interp3(xp,yp,zp,psi,(a+hD/2)*1e-10,yp,zp,'cubic');

                [~,dpsidx,~]=gradient(abs(Psi{iv,ivg,ne}),yp,xp,zp);
                dpsidx=permute(dpsidx,[2 1 3]);
                dpsixG(:,:)=interp3(X,Y,Z,dpsidx, -(a+hG/2)*1e-10,yp,zp,'cubic');
                dpsixD(:,:)=interp3(X,Y,Z,dpsidx,(a+hD/2)*1e-10,yp,zp,'cubic');

                intpsiG(iv,ivg,ne)=trapz(zp,trapz(yp,psiG,1));
                intpsiD(iv,ivg,ne)=trapz(zp,trapz(yp,psiD,1));
                intdpsixG(iv,ivg,ne)=trapz(zp,trapz(yp,dpsixG,1));
                intdpsixD(iv,ivg,ne)=trapz(zp,trapz(yp,dpsixD,1));
            end
        end
    end

    clear dpsix dpsidxG dpsixD psiG psiD psi

    % Interpolation des int�grales
    disp('Interpolation des int�grales');
    %utile pour l'interpolation des �nergies, ndgrid � la place de meshgrid.
    [Vi1,Vi1g]=ndgrid(V,Vg);
    [Vi2,Vi2g]=ndgrid(V2,V2g);

    PSIG=zeros(length(V2),length(V2g),Nemax);
    PSID=zeros(length(V2),length(V2g),Nemax);
    DPSIG=zeros(length(V2),length(V2g),Nemax);
    DPSID=zeros(length(V2),length(V2g),Nemax);
    for in=1:Nemax,
        PSIG(:,:,in)=interpn(Vi1,Vi1g,intpsiG(:,:,in),Vi2,Vi2g,'cubic');
        PSID(:,:,in)=interpn(Vi1,Vi1g,intpsiD(:,:,in),Vi2,Vi2g,'cubic');
        DPSIG(:,:,in)=interpn(Vi1,Vi1g,intdpsixG(:,:,in),Vi2,Vi2g,'cubic');
        DPSID(:,:,in)=interpn(Vi1,Vi1g,intdpsixD(:,:,in),Vi2,Vi2g,'cubic');
    end

    save([dossier_destination,dispos,'/PSI_MIBAR.mat'],'PSIG','PSID','DPSIG','DPSID');
else
    load([dossier_destination,dispos,'/PSI_MIBAR.mat'])
    disp('Psi_mibar exists');
end


for it=1:length(dT),
    TG=T+dT(it)/2;
    TD=T-dT(it)/2;
    disp('Calcul des fonctions de transferts')

    GdG=zeros(length(V2),length(V2g),Nemax+1);
    GdD=zeros(length(V2),length(V2g),Nemax+1);
    GGd=zeros(length(V2),length(V2g),Nemax+1);
    GDd=zeros(length(V2),length(V2g),Nemax+1);
    IMA=zeros(length(V2),length(V2g));
    IQGd=zeros(length(V2),length(V2g));
    IQdG=zeros(length(V2),length(V2g));
    IQDd=zeros(length(V2),length(V2g));
    IQdD=zeros(length(V2),length(V2g));
    IQFGd=zeros(length(V2),length(V2g));
    IQFdG=zeros(length(V2),length(V2g));
    IQFDd=zeros(length(V2),length(V2g));
    IQFdD=zeros(length(V2),length(V2g));
    Puiss=zeros(length(V2),length(V2g));
    
    for iv = 1:length(V2),

        EFD=Phidot-Phimetal-V2(iv);
        ECD=EFD-Efmetal;


        for ivg=1:length(V2g),


            for ie=1:Nemax,
                Ee=E2(iv,ivg,ie);
                VOG=V0G0(iv,ivg,ie);
                VOD=V0D0(iv,ivg,ie);
                kG=sqrt(2*me/hb^2*(Ee-ECG))/sqrt(qe);
                alG=sqrt(2*mox/(hb^2)*(VOG+Vbarr-Ee))/sqrt(qe);
                rhoG=2*Velec/(2*pi)^2*(2*me/hb^2)^(3/2).*sqrt(Ee-ECG); % En eV^-1.(qe)^3/2.m^-3 (seulement si on considere que V=1m est une approximation costaude...)
                intG=abs(alG*PSIG(iv,ivg,ie)+DPSIG(iv,ivg,ie))^2;

                kD=sqrt(2*me/hb^2*(Ee-ECD))/sqrt(qe);
                alD=sqrt(2*mox/hb^2*(VOD+Vbarr-Ee))/sqrt(qe);
                intD=abs(alD*PSID(iv,ivg,ie)-DPSID(iv,ivg,ie))^2;

                rhoD=2*Velec/(2*pi)^2*(2*me/hb^2)^(3/2).*sqrt(Ee-ECD);
                corr1G=abs(1-(VOG./(VOG+Vbarr-Ee)));
                corr2G=2*(VOG+Vbarr-Ee)./(3.*VOG)...
                    .*(1-(1-(VOG./(VOG+Vbarr-Ee))).^(3/2));
                corr1D=abs(1-((VOD+V2(iv))./(VOD+Vbarr-Ee)));
                corr2D=2*(VOD+Vbarr-Ee)./(3.*(VOD+V2(iv)))...
                    .*(1-(1-((VOD+V2(iv))./(VOD+Vbarr-Ee))).^(3/2));
                corr2D=(corr2D==0)+(isnan(corr2D))+corr2D;
                corr2G=(corr2G==0)+(isnan(corr2G))+corr2G;
                MG=(hb^2/(2*mox))^2*2/Velec*kG.^2./(kG.^2+alG.^2*(me/mox)^2.*corr1G)...
                    .*exp(-alG.*hG*1e-10.*corr2G).*intG;
                MD=(hb^2/(2*mox))^2*2/Velec*kD.^2./(kD.^2+alD.^2*(me/mox)^2.*corr1D)...
                    .*exp(-alD.*hD*1e-10.*corr2D).*intD;
                fermiG=1./(1+exp((Ee-EFG)./(kb*TG)));
                fermiD=1./(1+exp((Ee-EFD)./(kb*TD)));

                GdG(iv,ivg,ie+1)=ie*MG.*rhoG.*(1-fermiG);
                GdD(iv,ivg,ie+1)=ie*MD.*rhoD.*(1-fermiD);
                GGd(iv,ivg,ie)=(13-ie)*MG.*rhoG.*fermiG;
                GDd(iv,ivg,ie)=(13-ie)*MD.*rhoD.*fermiD;
            end

            Nmin=0;
            Nmax=Nemax;
            GammadG=permute(GdG(iv,ivg,Nmin+1:Nmax+1),[3 2 1]);
            GammadG(1)=0;
            GammaGd=permute(GGd(iv,ivg,Nmin+1:Nmax+1),[3 2 1]);
            GammaGd(Nmax-Nmin+1)=0;
            GammadD=permute(GdD(iv,ivg,Nmin+1:Nmax+1),[3 2 1]);
            GammadD(1)=0;
            GammaDd=permute(GDd(iv,ivg,Nmin+1:Nmax+1),[3 2 1]);
            GammaDd(Nmax-Nmin+1)=0;
            Gtot=GammadG+GammaGd+GammadD+GammaDd;

            %Creation de la matrice GAMMA
            GAMMA=zeros(Nmax-Nmin+1);
            for ii=1:length(GAMMA),
                GAMMA(ii,ii)=-Gtot(ii);
            end
            for ii=1:Nmax-Nmin,
                GAMMA(ii,ii+1)=GammadD(ii+1)+GammadG(ii+1);
                GAMMA(ii+1,ii)=GammaDd(ii)+GammaGd(ii);
            end

            %Equation ma�tresse
            P0=zeros(Nmax-Nmin+1,1);P0(1,:)=1;
            GAMMAt=GAMMA.*tmax;
            PI=expm(GAMMAt)*P0;
            %Prob0(iv,ivg)=PI(1);
	          %Prob1(iv,ivg)=PI(2);
	          %Prob2(iv,ivg)=PI(3);
	          %Prob3(iv,ivg)=PI(4);

            %Obtention du courant
            IMA(iv,ivg)=-qe*sqrt(qe)*2*pi/hb*sum(PI.*(GammadG-GammaGd)); %A

            Puiss(iv,ivg)=IMA(iv,ivg)*V2(iv);
            for ne=1:length(PI)-1,
                IQGd(iv,ivg)=IQGd(iv,ivg)+PI(ne)*sqrt(qe)*2*pi/hb*(E2(iv,ivg,ne))*GammaGd(ne); %W/qe
                IQDd(iv,ivg)=IQDd(iv,ivg)+PI(ne)*sqrt(qe)*2*pi/hb*(E2(iv,ivg,ne))*GammaDd(ne);
                IQdG(iv,ivg)=IQdG(iv,ivg)+PI(ne+1)*sqrt(qe)*2*pi/hb*(E2(iv,ivg,ne))*GammadG(ne+1);
                IQdD(iv,ivg)=IQdD(iv,ivg)+PI(ne+1)*sqrt(qe)*2*pi/hb*(E2(iv,ivg,ne))*GammadD(ne+1);
                IQFGd(iv,ivg)=IQFGd(iv,ivg)+PI(ne)*sqrt(qe)*2*pi/hb*(E2(iv,ivg,ne)-EFG)*GammaGd(ne);
                IQFDd(iv,ivg)=IQFDd(iv,ivg)+PI(ne)*sqrt(qe)*2*pi/hb*(E2(iv,ivg,ne)-EFD)*GammaDd(ne);
                IQFdG(iv,ivg)=IQFdG(iv,ivg)+PI(ne+1)*sqrt(qe)*2*pi/hb*(E2(iv,ivg,ne)-EFG)*GammadG(ne+1);
                IQFdD(iv,ivg)=IQFdD(iv,ivg)+PI(ne+1)*sqrt(qe)*2*pi/hb*(E2(iv,ivg,ne)-EFD)*GammadD(ne+1);
            end
        end
    end

disp('SAUVEGARDE IMA');
mkdir([dossier_destination,dispos,'/discret']);
save([dossier_destination,dispos,'/discret/IMA_T',num2str(T),'dT',num2str(dT(it)),'.mat'],...
    'IMA','V2','V2g','GGd','GDd','GdD','GdG','Puiss','IQGd','IQDd','IQdD','IQdG',...
    'IQFGd','IQFDd','IQFdD','IQFdG','TG','TD');%,'Prob0','Prob1','Prob2','Prob3');
    %P=-Puiss.*(Puiss<0);
    %[A,C]=max(max(P))
end
toc

disp('end')
