% alpha*T*I et beta*I^2 pour dT=+/-5K

clear
close all 
clc
qe=1.602176487e-19;

%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
H='discret';
deltaT=5;
Temp=100;
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

load([dossier,dispo,'/discret/IMA_T100dT5.mat']);
load([dossier,dispo,'/discret/CoeffFit_T100dT5.mat']);
IQG5=(IQFGd-IQFdG)*qe;
IQD5=(IQFdD-IQFDd)*qe;
Im5=(IQG5+IQD5)./2;
I5=IMA;
pG5=pIQG; pD5=pIQD;

load([dossier,dispo,'/discret/IMA_T100dT-5.mat']);
load([dossier,dispo,'/discret/CoeffFit_T100dT-5.mat']);
IQG_5=(IQFdG-IQFGd)*qe;
IQD_5=(IQFDd-IQFdD)*qe;
Im_5=(IQG_5+IQD_5)./2;
I_5=IMA;
pD_5=pIQD; pG_5=pIQG;

load([dossier,dispo,'/discret/ZT_T100.mat']);

[Z,ivg1]=min(abs(V2g-2.60));
[Z,ivg2]=min(abs(V2g-3.80));

fig=figure;
G(1)=plot(I5(:,ivg1)*1e12,(IQG5(:,ivg1)*1e15-pG5(ivg1,2)*I5(:,ivg1)*1e12),'b','LineWidth',2,'DisplayName','\beta_{gauche} \times I_{gauche}^2 \DeltaT=5K');
hold on
G(2)=plot(-I_5(:,ivg1)*1e12,-(IQD_5(:,ivg1)*1e15-pD_5(ivg1,2)*I_5(:,ivg1)*1e12),'--b','LineWidth',2,'DisplayName','\beta_{gauche} \times I_{gauche}^2 \DeltaT=-5K');
hold on
G(3)=plot(I5(:,ivg1)*1e12,(IQD5(:,ivg1)*1e15-pD5(ivg1,2)*I5(:,ivg1)*1e12),'r','LineWidth',2,'DisplayName','\beta_{droite} \times I_{droite}^2 \DeltaT=5K');
hold on
G(4)=plot(-I_5(:,ivg1)*1e12,-(IQG_5(:,ivg1)*1e15-pG_5(ivg1,2)*I_5(:,ivg1)*1e12),'--r','LineWidth',2,'DisplayName','\beta_{droite} \times I_{droite}^2 \DeltaT=-5K');

xlim([-1 1])
%xlim([-.005 .005])
ymax=get(gca,'ylim');
xmax=get(gca,'xlim');
L=legend(G);

han=axes(fig,'visible','off'); 
han.Title.Visible='on';
han.XLabel.Visible='on';
han.YLabel.Visible='on';
ylabel(han,'heat current (fW)');
xlabel(han,'electronic current (pA)');

T=100;
figure
[Z,iT]=min(abs(dT-5));
Q(1)=plot(V2g,alpha(:,iT)*T,'--b','LineWidth',2,'DisplayName','\alpha \times T dT=5K');
hold on
Q(2)=plot(V2g,pD5(:,2)*1e-3,'b','LineWidth',2,'DisplayName','pIQD(1) dT=5K');
hold on
[Z,iT]=min(abs(dT+5));
Q(3)=plot(V2g,alpha(:,iT)*T,'--r','LineWidth',2,'DisplayName','\alpha \times T dT=-5K');
hold on
Q(4)=plot(V2g,pD_5(:,2)*1e-3,'r','LineWidth',2,'DisplayName','pIQD(1) dT=-5K');
xlim([2 5])
L=legend(Q)
xlabel('gate voltage (V)')
ylabel('V')

% figure
% [Z,iT]=min(abs(dT-5));
% R(1)=plot(V2g,abs((alpha(:,iT)*T-pD5(:,2)*1e-3)./(alpha(:,iT)*T)),'--b','LineWidth',2,'DisplayName','écart relatif D dT=5K');
% hold on
% R(2)=plot(V2g,abs((alpha(:,iT)*T-pS5(:,2)*1e-3)./(alpha(:,iT)*T)),'b','LineWidth',2,'DisplayName','écart relatif  S dT=5K');
% [Z,iT]=min(abs(dT+5));
% R(3)=plot(V2g,abs((alpha(:,iT)*T-pD_5(:,2)*1e-3)./(alpha(:,iT)*T)),'--r','LineWidth',2,'DisplayName','écart relatif D dT=-5K');
% hold on
% R(4)=plot(V2g,abs((alpha(:,iT)*T-pS_5(:,2)*1e-3)./(alpha(:,iT)*T)),'r','LineWidth',2,'DisplayName','écart relatif S dT=5K');
% L=legend(R)
% xlabel('gate voltage (V)')