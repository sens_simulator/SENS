% alpha, Ge et facteur de puissance dans le cas discret, pour différents T
clear 
close all

%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
Temp=[50 70 100];
deltaT=5;
% Dossier où sont stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------



dossierm=[dossier_destination,dispo,'/'];
load([dossierm,'/discret/ZT_T',num2str(Temp(1)),'.mat'])
[Z,iT]=min(abs(dT-deltaT));
Ge1=Ge;alpha1=alpha(:,iT);f1=f(:,iT);

load([dossierm,'/discret/ZT_T',num2str(Temp(2)),'.mat'])
[Z,iT]=min(abs(dT-deltaT));
Ge2=Ge;alpha2=alpha(:,iT);f2=f(:,iT);

load([dossierm,'/discret/ZT_T',num2str(Temp(3)),'.mat'])
[Z,iT]=min(abs(dT-deltaT));
Ge3=Ge;alpha3=alpha(:,iT);f3=f(:,iT);



%CONDUCTANCE ELECTRONIQUE
subplot('Position',[0.15 0.68 0.80 0.29])
plot([2.92 2.92],[0 8],'--k','LineWidth',1);
hold on
plot([4.92 4.92],[0 8],'--k','LineWidth',1);

plot(V2g,Ge1(:,1)*1e9,'-r','LineWidth',2); hold on
plot(V2g(1:45:end),Ge1(1:45:end,1)*1e9,'+r','MarkerSize',10,'LineWidth',2)
G(1)=plot(V2g,Ge1(:,1)*1e9,'-+r','MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','T=50K');

plot(V2g,Ge2(:,1)*1e9,'--','Color',[0 0.5 0],'LineWidth',2); hold on
plot(V2g(30:45:end),Ge2(30:45:end,1)*1e9,'v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2)
G(2)=plot(V2g(1:20:end),Ge2(1:20:end,1)*1e9,'--v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','T=70K');

plot(V2g,Ge3(:,1)*1e9,'-.b','LineWidth',2); hold on
plot(V2g(30:45:end),Ge3(30:45:end,1)*1e9,'db','MarkerSize',8,'LineWidth',2)
G(3)=plot(V2g,Ge3(:,1)*1e9,'-.db','MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','T=100K');


ylabel('electronic conductance (nS)');
legend(G,'Location','North')
legend boxoff

set(gca,'Xticklabel',{});
set(gca,'YLim',[0 1.3]);
xlim([2 5])
ylim([0 2])
%legend(H,'Location','North')
%grid on

%ALPHA
subplot('Position',[0.15 0.39 0.8 0.29])
plot([2.92 2.92],[-2.5 2.5],'--k','LineWidth',1);
hold on
plot([4.92 4.92],[-2.5 2.5],'--k','LineWidth',1);
plot([V2g(1) V2g(end)],[0 0],'--k','LineWidth',1);

plot(V2g,alpha1*1e3,'-r','LineWidth',2); hold on
plot(V2g(1:40:end),alpha1(1:40:end)*1e3,'+r','MarkerSize',10,'LineWidth',2)

plot(V2g,alpha2*1e3,'--','Color',[0 0.5 0],'LineWidth',2); hold on
plot(V2g(21:40:end),alpha2(21:40:end)*1e3,'v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2)

plot(V2g,alpha3*1e3,'-.b','LineWidth',2); hold on
plot(V2g(25:40:end),alpha3(25:40:end)*1e3,'db','MarkerSize',8,'LineWidth',2)

set(gca,'Xticklabel',{});
set(gca,'YLim',[-1.2 1.2]);
xlim([2 5])
ylim([-2.5 2.5])
ylabel('Seebeck coefficient (mV.K^{-1})');

%FACTEUR DE PUISSANCE
subplot('Position',[0.15 0.1 0.8 0.29])
plot([2.92 2.92],[1 60],'--k','LineWidth',1);
hold on
plot([4.92 4.92],[1 60],'--k','LineWidth',1);

plot(V2g,f1*1e18,'-r','LineWidth',2); hold on
plot(V2g(1:40:end),f1(1:40:end)*1e18,'+r','MarkerSize',10,'LineWidth',2)

plot(V2g,f2*1e18,'--','Color',[0 0.5 0],'LineWidth',2); hold on
plot(V2g(21:40:end),f2(21:40:end)*1e18,'v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2)

plot(V2g,f3*1e18,'-.b','LineWidth',2); hold on
plot(V2g(25:40:end),f3(25:40:end)*1e18,'db','MarkerSize',8,'LineWidth',2)


ylim([0 60])
%semilogy(V2g,f4(:,2)*1e18,'b');

ylabel('power factor (aW.K^{-2})');
xlim([2 5])


set(gca,'YLim',[0 38]);
%grid on
xlabel('gate voltage (V)');