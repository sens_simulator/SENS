% Ge et coefficient de Seebeck dans le cas discret

clear 
close all

%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
% Dossier où sont stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
Temp=[50 70 100];
deltaT=5;
%--------------------------------------------------------------------------


dossierm=[dossier_destination,dispo,'/'];

load([dossierm,'/discret/ZT_T',num2str(Temp(1)),'.mat'])
[Z,iT]=min(abs(dT-deltaT));
Ge1=Ge;alpha1=alpha(:,iT);f1=f(:,iT);

load([dossierm,'/discret/ZT_T',num2str(Temp(2)),'.mat'])
[Z,iT]=min(abs(dT-deltaT));
Ge2=Ge;alpha2=alpha(:,iT);f2=f(:,iT);

load([dossierm,'/discret/ZT_T',num2str(Temp(3)),'.mat'])
[Z,iT]=min(abs(dT-deltaT));
Ge3=Ge;alpha3=alpha(:,iT);f3=f(:,iT);



%CONDUCTANCE ELECTRONIQUE
subplot('Position',[0.15 0.54 0.80 0.4])
plot([2.92 2.92],[0 8],'--k','LineWidth',1);
hold on
plot([4.92 4.92],[0 8],'--k','LineWidth',1);

plot(V2g,Ge1(:,1)*1e9,'-r','LineWidth',2); hold on
plot(V2g(1:20:end),Ge1(1:20:end,1)*1e9,'+r','MarkerSize',5,'LineWidth',2)
%G(1)=plot(V2g,Ge1(:,1)*1e9,'-+r','MarkerSize',5,'LineWidth',2,'Visible','on','DisplayName','T=50K');

plot(V2g,Ge2(:,1)*1e9,'--','Color',[0 0.5 0],'LineWidth',2); hold on
plot(V2g(30:20:end),Ge2(30:20:end,1)*1e9,'v','Color',[0 0.5 0],'MarkerSize',5,'LineWidth',2)
%G(2)=plot(V2g(1:20:end),Ge2(1:20:end,1)*1e9,'--v','Color',[0 0.5 0],'MarkerSize',5,'LineWidth',2,'Visible','on','DisplayName','T=70K');

plot(V2g,Ge3(:,1)*1e9,'-.b','LineWidth',2); hold on
plot(V2g(30:20:end),Ge3(30:20:end,1)*1e9,'db','MarkerSize',5,'LineWidth',2)
%G(3)=plot(V2g,Ge3(:,1)*1e9,'-.db','MarkerSize',5,'LineWidth',2,'Visible','on','DisplayName','T=100K');


ylabel('electronic conductance (nS)','FontName','Latin Modern Roman','FontSize',14);
%legend(G,'Location','North','FontName','Latin Modern Roman','FontSize',14)
%legend boxoff

set(gca,'Xticklabel',{});
set(gca,'YLim',[0 1.3]);
ax=gca;
ax.FontName='Latin Modern Roman';
ax.FontSize=12;
xlim([2 5])
ylim([0 2])
%legend(H,'Location','North')
%grid on

%ALPHA
subplot('Position',[0.15 0.14 0.8 0.4])
plot([2.92 2.92],[-2.5 2.5],'--k','LineWidth',1);
hold on
plot([4.92 4.92],[-2.5 2.5],'--k','LineWidth',1);
plot([V2g(1) V2g(end)],[0 0],'--k','LineWidth',1);

plot(V2g,alpha1*1e3,'-r','LineWidth',2); hold on
plot(V2g(1:20:end),alpha1(1:20:end)*1e3,'+r','MarkerSize',5,'LineWidth',2)

plot(V2g,alpha2*1e3,'--','Color',[0 0.5 0],'LineWidth',2); hold on
plot(V2g(21:20:end),alpha2(21:20:end)*1e3,'v','Color',[0 0.5 0],'MarkerSize',5,'LineWidth',2)

plot(V2g,alpha3*1e3,'-.b','LineWidth',2); hold on
plot(V2g(25:20:end),alpha3(25:20:end)*1e3,'db','MarkerSize',5,'LineWidth',2)

%set(gca,'Xticklabel',{});
set(gca,'YLim',[-1.2 1.2]);
ax=gca;
ax.FontName='Latin Modern Roman';
ax.FontSize=12;
xlim([2 5])
ylim([-2.5 2.5])
ylabel('Seebeck coefficient (mV.K^{-1})','FontName','Latin Modern Roman','FontSize',14);
xlabel('gate voltage (V)','FontName','Latin Modern Roman','FontSize',14);