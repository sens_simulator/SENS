%Figure7 : beta et betaS/betaD en fonction de hG/hD
clear
qe=1.602176487e-19;


%--------------------------------------------------------------------------
% À MODIFIER
deltaT=5; %K
T=100; %K
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

dispo='44-12-10-50-pp';
load([dossier,dispo,'/discret/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pS1b=pIQG(1,1);pD1b=pIQD(1,1);
load([dossier,dispo,'/epaisseur.mat']);
H1b=hG_eff/hD_eff;
dH1b=H1b*(dh/hG_eff+dh*hD_eff);

dispo='44-12-12-50-pp';
load([dossier,dispo,'/discret/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pS1=pIQG(1,1);pD1=pIQD(1,1);
load([dossier,dispo,'/epaisseur.mat']);
H1=hG_eff/hD_eff;
dH1=H1*(dh/hG_eff+dh*hD_eff);

dispo='44-12-14-50-pp';
load([dossier,dispo,'/discret/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pS2=pIQG(1,1);pD2=pIQD(1,1);
load([dossier,dispo,'/epaisseur.mat']);
H2=hG_eff/hD_eff;
dH2=H2*(dh/hG_eff+dh*hD_eff);

dispo='44-12-15-50-pp';
load([dossier,dispo,'/discret/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pS3=pIQG(1,1);pD3=pIQD(1,1);
load([dossier,dispo,'/epaisseur.mat']);
H3=hG_eff/hD_eff;
dH3=H3*(dh/hG_eff+dh*hD_eff);

dispo='44-12-17-50-pp';
load([dossier,dispo,'/discret/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pS4=pIQG(1,1);pD4=pIQD(1,1);
load([dossier,dispo,'/epaisseur.mat']);
H4=hG_eff/hD_eff;
dH4=H4*(dh/hG_eff+dh*hD_eff);

% x=[H1 H2 H3 H4 H5 H6];
% y=[abs(pS1/pD1) abs(pS2/pD2) abs(pS3/pD3) abs(pS4/pD4) abs(pS5/pD5) abs(pS6/pD6)];
% error=[dH1 dH2 dH2 dH4 dH5 dH6];

% x=[H1 H2 H3 H4 H5];
% y=[abs(pS1/pD1) abs(pS2/pD2) abs(pS3/pD3) abs(pS4/pD4) abs(pS5/pD5)];
% error=[dH1 dH2 dH2 dH4 dH5];

x=[H1b H1 H2 H3 H4];
y=[abs(pS1b/pD1b) abs(pS1/pD1) abs(pS2/pD2) abs(pS3/pD3) abs(pS4/pD4)];
error=[dH1b dH1 dH2 dH2 dH4];

plot(x,y,'-sr','LineWidth',1.5,'MarkerSize',3)
hold on
e=errorbar(x,y,error,'horizontal');
X=[x(end:-1:1)-error(end:-1:1) x+error];
Y=[y(end:-1:1) y];
patch(X,Y,'b','FaceAlpha',0.1,'EdgeColor','none')
e.Color=[0, 0.4470, 0.7410];
e.LineWidth=1;
e.LineStyle='none';
xlabel('h_S/h_D','FontName','Latin Modern Roman','FontSize',14)
ylabel('|\beta_S/\beta_D|','FontName','Latin Modern Roman','FontSize',14)
ax=gca;
ax.FontName='Latin Modern Roman';
ax.FontSize=12;
