% I-Vds pour trois dT différents
clear
close all


%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
H='discret';
dT=[0 5 10];
T=100;
% Dossier où sont stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------


dossier=[dossier_destination,dispo,'/'];

load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(dT(1)),'.mat'])
I1=IMA;
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(dT(2)),'.mat'])
I2=IMA;
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(dT(3)),'.mat'])
I3=IMA;

[Z,ivg]=min(abs(V2g-2.67));

hold on
G(1)=plot(V2(1:7:end)*1e3,I1(1:7:end,ivg)*1e12,'-bo','LineWidth',2,'MarkerSize',4,'DisplayName',['\DeltaT=',num2str(dT(1)),'K']);

hold on
G(2)=plot(V2(1:7:end)*1e3,I2(1:7:end,ivg)*1e12,'-rx','LineWidth',2,'MarkerSize',6,'DisplayName',['\DeltaT=',num2str(dT(2)),'K']);

hold on
G(3)=plot(V2(1:7:end)*1e3,I3(1:7:end,ivg)*1e12,'-o','Color',[0 0.5 0],'LineWidth',2,'MarkerSize',4,'DisplayName',['\DeltaT=',num2str(dT(3)),'K']);

plot([0,0],[-2,2],'k--','LineWidth',2)
plot([-6,6],[0,0],'k--','LineWidth',2)


x1=[-6 0 0 -6];y1=[0 0 2 2];
x2=[0 6 6 0];y2=[0 0 2 2];
x3=[-6 0 0 -6];y3=[-2 -2 0 0];
x4=[0 6 6 0];y4=[-2 -2 0 0];

patch(x1,y1,'g','FaceAlpha',0.1)
patch(x2,y2,'r','FaceAlpha',0.1)
patch(x3,y3,'b','FaceAlpha',0.1)
patch(x4,y4,'g','FaceAlpha',0.1)

legend(G,'Location','NorthWest','FontName','Latin Modern Roman','FontSize',16)
legend box off

xlabel('drain voltage (mV)','FontName','Latin Modern Roman','FontSize',16)
ylabel('drain current (pA)','FontName','Latin Modern Roman','FontSize',16)




