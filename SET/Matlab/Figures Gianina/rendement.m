% Calcul du rendement et de la puissance

clear;close all;clc
tic

%--------------------------------------------------------------------------
% À MODIFIER
dispos='44-12-15-50-pp';
H='discret';
deltaT=5;
T=100;
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

dossierm=[dossier,dispos,'/',H,'/'];

load([dossierm,'/ZT_T',num2str(T),'.mat']);
load([dossierm,'/IMA_T',num2str(T),'dT',num2str(deltaT),'.mat'])

T=100;
dT=deltaT;

qe=1.602176487e-19;
[V,VG]=ndgrid(V2,V2g);

I=IMA;
for iv=1:length(V2)
    for ivg=1:length(V2g)
        P(iv,ivg)=-I(iv,ivg)*V2(iv);
    end
end

P(P<0)=NaN;
[Z,ivg]=min(abs(V2g-5));P(:,ivg:end)=NaN;
IQFG=IQFGd-IQFdG;

%max � 108,542 normalement
%Figure Puissance
figure

pcolor(V*1e3,VG,P*1e18);
hold on
plot([0 0],[V2g(1) V2g(end)],'linewidth',2);
plot([V2(1) V2(end)]*1e3,[4.92 4.92],'-.k','linewidth',2);
plot([V2(1) V2(end)]*1e3,[2.92 2.92],'-.k','linewidth',2);

shading interp


% L=colorbar;
% ycmap=jet(100000);
% caxis([0 220]);
% y=caxis;
% LIN=linspace(y(1),y(2),100000);
% p0=interp1(LIN,1:1:100000,0); %trouve l'indice pour que le caxis soit � 0
% ycmap(floor(p0),:)=[1 1 1];
% ycmap(ceil(p0),:)=[1 1 1];
ylabel(colorbar,'power (aW)')
%set(gcf,'Colormap',ycmap)
ylim([2 5])


[~,ivgmax]=max(max(P));
[~,ivmax]=max(P(:,ivgmax));
Pmax=max(max(P));
disp(['Pmax=',num2str(Pmax*1e18),'aW'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%      CALCUL RENDEMENT        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ETA=P./((IQFG)*qe);
%ETA=P./((IQDOT)*1.6e-19);
ETAC=dT/(T+dT/2); %rendement Carnot dT/Tchaud
%E=ETA.*(Puiss<0);
E=ETA;
H=E/ETAC; % rendement
%mE=1*(E>1);

disp(['rendement à Pmax=',num2str(H(ivmax,ivgmax))])
[~,ivgmax]=max(max(H));
[~,ivmax]=max(H(:,ivgmax));
disp(['rendement max=',num2str(H(ivmax,ivgmax))])

%Figure rendement
figure

hold on
%plot([V2(1)*1e3 V2(end)*1e3],[0 0],'--k','linewidth',1);
pcolor(V*1e3,VG,H);
hold on
plot([0 0],[V2g(1) V2g(end)],'linewidth',2);
plot([V2(1) V2(end)]*1e3,[4.92 4.92],'-.k','linewidth',2);
plot([V2(1) V2(end)]*1e3,[2.92 2.92],'-.k','linewidth',2);
shading interp
hold on
L=colorbar;
caxis([0 1]);
ycmap=jet(100000);
y=caxis;
LIN=linspace(y(1),y(2),100000);
p0=interp1(LIN,1:1:100000,0); %trouve l'indice pour que le caxis soit � 0
ycmap(floor(p0),:)=[1 1 1];
ycmap(ceil(p0),:)=[1 1 1];
ylabel(colorbar,'efficiency/Carnot efficiency')
ylim([2 5])

set(gcf,'Colormap',ycmap)
%set(gca,'ticklength',[0.5 0.5]);
%}
