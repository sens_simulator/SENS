% IQ-I pour différents élargissements et deux Vgs différents

clear
close all 
clc
qe=1.602176487e-19;


%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
% Dossier où sont stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------


dossier=[dossier_destination,'/'];


load([dossier,dispo,'/discret/IMA_T100dT5.mat']);
load([dossier,dispo,'/discret/CoeffFit_T100dT5.mat']);
load([dossier,dispo,'/discret/ZT_T100.mat']);
IQGdis=(IQFGd-IQFdG)*qe;
IQDdis=(IQFdD-IQFDd)*qe;
Imdis=(IQGdis+IQDdis)./2;
Idis=IMA;
alphadis=alpha;
pGdis=pIQG;
pDdis=pIQD;
K0dis=KG;

load([dossier,dispo,'/A_0.05/IMA_T100dT5.mat']);
load([dossier,dispo,'/A_0.05/CoeffFit_T100dT5.mat']);
load([dossier,dispo,'/A_0.05/ZT_T100.mat']);

IQG5=(IQFGd-IQFdG)*qe;
IQD5=(IQFdD-IQFDd)*qe;
I5=IMA;
alpha5=alpha;
pG5=pIQG;
pD5=pIQD;
K05=KG;
KD05=KD;
Im5=(IQG5+IQD5)./2;


load([dossier,dispo,'/A_1/IMA_T100dT5.mat']);
load([dossier,dispo,'/A_1/CoeffFit_T100dT5.mat']);
load([dossier,dispo,'/A_1/ZT_T100.mat']);
IQG1=(IQFGd-IQFdG)*qe;
IQD1=(IQFdD-IQFDd)*qe;
I1=IMA;
alpha1=alpha;
pG1=pIQG;
pD1=pIQD;
K01=KG;
KD01=KD;
Im1=(IQG1+IQD1)./2;

[Z,ivg1]=min(abs(V2g-2.67));
[Z,ivg2]=min(abs(V2g-3.80));

fig=figure;

subplot(2,2,1) %Vgs=2.67V

plot(I5(:,ivg1)*1e12,IQG5(:,ivg1)*1e15,'b','LineWidth',2);
hold on
plot(I5(:,ivg1)*1e12,IQD5(:,ivg1)*1e15,'--r','LineWidth',2);
hold on
plot(I5(1:20:end,ivg1)*1e12,IQG5(1:20:end,ivg1)*1e15,'+b','LineWidth',2);
hold on
plot(I5(1:20:end,ivg1)*1e12,IQD5(1:20:end,ivg1)*1e15,'+r','LineWidth',2);
hold on
plot(I5(:,ivg1)*1e12,Im5(:,ivg1)*1e15,'--k','LineWidth',1);
% xlim([min(I5(:,ivg1))*1e12 max(I5(:,iv1))*1e12]);
% ylim([min(IQG5(:,ivg1))*1e15 max(IQD5(:,iv1))*1e15]);
xlim([-2 2])
ymax=get(gca,'ylim');
xmax=get(gca,'xlim');
text(0.9*xmax(2),0.9*ymax(1),{'H = 5\times10^{-2} k_BT','V_{GS} = 2.67 V'},'HorizontalAlignment','right','VerticalAlignment','bottom')

subplot(2,2,2) %Vgs=3.80V

plot(I5(:,ivg2)*1e12,IQG5(:,ivg2)*1e15,'b','LineWidth',2);
hold on
plot(I5(:,ivg2)*1e12,IQD5(:,ivg2)*1e15,'--r','LineWidth',2);
hold on
plot(I5(1:20:end,ivg2)*1e12,IQG5(1:20:end,ivg2)*1e15,'+b','LineWidth',2);
hold on
plot(I5(1:20:end,ivg2)*1e12,IQD5(1:20:end,ivg2)*1e15,'+r','LineWidth',2);
hold on
plot(I5(:,ivg2)*1e12,Im5(:,ivg2)*1e15,'--k','LineWidth',1);

% xlim([min(I5(:,ivg2))*1e12 max(I5(:,iv2))*1e12]);
% ylim([min(IQG5(:,ivg2))*1e15 max(IQD5(:,iv2))*1e15]);
xlim([-7e-5 7e-5])
ymax=get(gca,'ylim');
xmax=get(gca,'xlim');
text(0.9*xmax(2),1.01*ymax(1),{'H = 5\times10^{-2} k_BT','V_{GS} = 3.80 V'},'HorizontalAlignment','right','VerticalAlignment','bottom')

subplot(2,2,3); %Vgs=2.67V

plot(I1(:,ivg1)*1e12,IQG1(:,ivg1)*1e15,'b','LineWidth',2);
hold on
plot(I1(:,ivg1)*1e12,IQD1(:,ivg1)*1e15,'--r','LineWidth',2);
hold on
plot(I1(1:20:end,ivg1)*1e12,IQG1(1:20:end,ivg1)*1e15,'+b','LineWidth',2);
hold on
plot(I1(1:20:end,ivg1)*1e12,IQD1(1:20:end,ivg1)*1e15,'+r','LineWidth',2);
hold on
plot(I1(:,ivg1)*1e12,Im1(:,ivg1)*1e15,'--k','LineWidth',1);

% xlim([min(I1(:,ivg1))*1e12 max(I1(:,iv1))*1e12]);
% ylim([min(IQG1(:,ivg1))*1e15 max(IQD1(:,iv1))*1e15]);
xlim([-2 2])
ymax=get(gca,'ylim');
xmax=get(gca,'xlim');
text(0.9*xmax(2),0.9*ymax(1),{'H = k_BT','V_{GS} = 2.67 V'},'HorizontalAlignment','right','VerticalAlignment','bottom')


subplot(2,2,4); %Vgs=3.80V

plot(I1(:,ivg2)*1e12,IQG1(:,ivg2)*1e15,'b','LineWidth',2);
hold on
plot(I1(:,ivg2)*1e12,IQD1(:,ivg2)*1e15,'--r','LineWidth',2);
hold on
plot(I1(1:20:end,ivg2)*1e12,IQG1(1:20:end,ivg2)*1e15,'+b','LineWidth',2);
hold on
plot(I1(1:20:end,ivg2)*1e12,IQD1(1:20:end,ivg2)*1e15,'+r','LineWidth',2);
hold on
plot(I1(:,ivg2)*1e12,Im1(:,ivg2)*1e15,'--k','LineWidth',1);
% 
% xlim([min(I1(:,ivg2))*1e12 max(I1(:,iv2))*1e12]);
% ylim([min(IQG1(:,ivg2))*1e15 max(IQD1(:,iv2))*1e15]);
xlim([-5e-2 5e-2])
ymax=get(gca,'ylim');
xmax=get(gca,'xlim');
text(0.9*xmax(2),0.9*ymax(1),{'H = k_BT','V_{GS} = 3.80 V'},'HorizontalAlignment','right','VerticalAlignment','bottom')

han=axes(fig,'visible','off'); 
han.Title.Visible='on';
han.XLabel.Visible='on';
han.YLabel.Visible='on';
ylabel(han,'heat current (fW)');
xlabel(han,'electronic current (pA)');