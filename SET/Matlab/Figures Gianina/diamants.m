% Diagramme Diamants
clear
close all


%--------------------------------------------------------------------------
% À MODIFIER
dispos='44-12-12-50-pp';
H='discret';
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
dossierm=[dossier,dispos,'/',H,'/'];
T=100; %K
dT=5; %K
%--------------------------------------------------------------------------

load([dossierm,'IMA_T',num2str(T),'dT',num2str(dT),'.mat'])


[V,VG]=ndgrid(V2,V2g);

pcolor(V*1e3,VG,IMA*1e12)
shading interp
colorbar
caxis([-6 6])

load([dossierm,'/ZT_T100.mat'])
hold on
plot(-Voc(:,2)*1e3,V2g,'-k','LineWidth',1);
plot([V2(1) V2(end)]*1e3,[2.67 2.67],'k--','LineWidth',1)
plot([V2(1) V2(end)]*1e3,[3.80 3.80],'k--','LineWidth',1)
%plot([1 1],[V2g(1) V2g(end)],'--k','LineWidth',1);
ylim([2 5])
colormap(jet)
xlabel('drain voltage (mV)','FontName','Latin Modern Roman','FontSize',17)
ylabel('gate voltage (V)','FontName','Latin Modern Roman','FontSize',17)
ylabel(colorbar,'drain current (pA)','FontName','Latin Modern Roman','FontSize',17)

