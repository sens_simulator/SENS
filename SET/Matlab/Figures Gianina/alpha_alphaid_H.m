% alpha en fonction de alpha id�al pour diff�rents H
clear 
close all

%--------------------------------------------------------------------------
% � MODIFIER
dispo='44-12-15-50-pp';
H='discret';
dT=[0 5 10];
T=100;
VGS=2.67;
% Dossier o� sont stock�s les fichiers de donn�es Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------


load([dossier,dispo,'/Energies.mat']);

qe=1.602176487e-19;
T=100;
deltaT=5;
U=(E2-EFD)./(T);

dossierm=[dossier,dispo,'/discret/'];
load([dossierm,'IMA_T',num2str(T),'dT',num2str(deltaT),'.mat']);

%On se met pour V2 = 0 V
[Z,iv]=min(abs(V2));

H='discret';
dossierm=[dossier,dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);

%FigureFormat(8,6,24,2) 
%set(gca,'Position',[0.15 0.18 0.8 0.8])

plot(U(iv,:,1)*1e3,U(iv,:,1)*1e3,'-.r');
hold on

H='discret';
dossierm=[dossier,dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
iT=find(dT==deltaT);
plot(U(iv,:,1)*1e3,alpha(:,iT)*1e3,'-r','Linewidth',1); hold on
plot(U(iv,1:20:end,1)*1e3,alpha(1:20:end,iT)*1e3,'+r','MarkerSize',8,'LineWidth',2)
G(1)=plot(U(iv,1:20:end,1)*1e3,alpha(1:20:end,iT)*1e3,'-+r','MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','H=0');

H='A_0.05';
dossierm=[dossier,dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
iT=find(dT==deltaT);
plot(U(iv,:,1)*1e3,alpha(:,iT)*1e3,'-','Color',[0 0.5 0]); hold on
plot(U(iv,15:20:end,1)*1e3,alpha(15:20:end,iT)*1e3,'v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2)
G(2)=plot(U(iv,15:20:end,1)*1e3,alpha(15:20:end,iT)*1e3,'-v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','H=0.02');

H='A_0.1';
dossierm=[dossier,dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
iT=find(dT==deltaT);
plot(U(iv,:,1)*1e3,alpha(:,iT)*1e3,'-k'); hold on
plot(U(iv,7:20:end,1)*1e3,alpha(7:20:end,iT)*1e3,'^k','MarkerSize',8,'LineWidth',2)
G(3)=plot(U(iv,5:20:end,1)*1e3,alpha(5:20:end,iT)*1e3,'-^k','MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','H=0.1');


H='A_0.5';
dossierm=[dossier,dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
iT=find(dT==deltaT);
plot(U(iv,:,1)*1e3,alpha(:,iT)*1e3,'-','Color',[0.5 0 0.5]); hold on
plot(U(iv,7:20:end,1)*1e3,alpha(7:20:end,iT)*1e3,'s','Color',[0.5 0 0.5],'MarkerSize',8,'LineWidth',2)
G(4)=plot(U(iv,5:20:end,1)*1e3,alpha(5:20:end,iT)*1e3,'-s','Color',[0.5 0 0.5],'MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','H=0.5');



H='A_1';
dossierm=[dossier,dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
iT=find(dT==deltaT);
plot(U(iv,:,1)*1e3,alpha(:,iT)*1e3,'-b'); hold on
plot(U(iv,5:20:end,1)*1e3,alpha(5:20:end,iT)*1e3,'db','MarkerSize',8,'LineWidth',2)
G(5)=plot(U(iv,5:20:end,1)*1e3,alpha(5:20:end,iT)*1e3,'-db','MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','H=1');

legend(G,'Location','NorthWest')
legend boxoff
xlim([-1 1])
ylim([-1 1])
xlabel('\alpha_{ id�al}(mV.K^{ -1})')
ylabel('\alpha (mV.K^{ -1})')
grid on
