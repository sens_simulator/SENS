% conductance thermique et ZT en fonction de Vgs pour deux élargissements
clear 
close all

qe=1.602176487e-19;

%--------------------------------------------------------------------------
% À MODIFIER
dispos='44-12-15-50-pp';
deltaT=5;
T=100;
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

H='A_0.05';
dossierm=[dossier,dispos,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
iT=find(dT==deltaT);
K1=KG(:,iT);
ZT1=ZT(:,iT);

H='A_1';
dossierm=[dossier,dispos,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
iT=find(dT==deltaT);
K2=KG(:,iT);
ZT2=ZT(:,iT);

% Conductance
hh=figure;
set(hh,'defaultAxesColorOrder',[[0 0.5 0];[0 0 1]]);
subplot('Position',[0.1 0.5 0.80 0.45]) 
yyaxis left
plot(V2g,K1*1e18,'LineWidth',2,'DisplayName','H = 5 \times 10^{-2}k_BT')
xlabel('gate voltage (V)')
ylim([0 50])
ylabel('thermal conductance (aW.K^{-1})','color','k')
hold on
yyaxis right
plot(V2g,K2*1e18,'--','LineWidth',2,'DisplayName','H = k_BT')
xlim([2 5])
ylim([0 500])
legend('H = 5 \times 10^{-2}k_BT','H = k_BT')


% figure de mérite

subplot('Position',[0.1 0.05 0.80 0.45]) 
yyaxis left
% plot(V2g,ZT1,'Color',[0 0.5 0],'LineWidth',2)
plot(V2g,ZT1,'LineWidth',2,'DisplayName','H = 5 \times 10^{-2}k_BT')
xlabel('gate voltage (V)')
ylabel('figure of merit','color','k')
hold on
yyaxis right
% plot(V2g,ZT2,'b--','LineWidth',2)
plot(V2g,ZT2,'--','LineWidth',2,'DisplayName','H = k_BT')
xlim([2 5])
