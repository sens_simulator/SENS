% beta et betaS/betaD en fonction de dT à T constant
clear
close all 
clc
qe=1.602176487e-19;


%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
T=100; %K
H='discret';
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------


deltaT=5;
load([dossier,dispo,'/',H,'/IMA_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pG1=pIQG;pD1=pIQD;
deltaT=10;
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pG2=pIQG;pD2=pIQD;
deltaT=15;
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pG3=pIQG;pD3=pIQD;
deltaT=20;
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pG4=pIQG;pD4=pIQD;
deltaT=25;
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pG5=pIQG;pD5=pIQD;


% FigureFormat(6,6,16,2);
subplot('Position',[0.1 0.5 0.80 0.45])
semilogy([2.82 2.82],[1e8 1e14],'--k','LineWidth',1)
hold on
semilogy([4.86 4.86],[1e8 1e14],'--k','LineWidth',1)

semilogy(V2g,pD1(:,1)*1e9,'-r');
semilogy(V2g(10:30:end),pD1(10:30:end,1)*1e9,'+r','MarkerSize',6);

semilogy(V2g,pD2(:,1)*1e9,'-','Color',[0 0.5 0]);
semilogy(V2g(20:30:end),pD2(20:30:end,1)*1e9,'v','Color',[0 0.5 0],'MarkerSize',6);

semilogy(V2g,pD3(:,1)*1e9,'-k');
semilogy(V2g(1:30:end),pD3(1:30:end,1)*1e9,'^k','MarkerSize',6);

semilogy(V2g,pD4(:,1)*1e9,'-','Color',[0.5 0 0.5]);
semilogy(V2g(1:30:end),pD4(1:30:end,1)*1e9,'s','Color',[0.5 0 0.5],'MarkerSize',6);

semilogy(V2g,pD5(:,1)*1e9,'-b');
semilogy(V2g(1:30:end),pD5(1:30:end,1)*1e9,'db','MarkerSize',6);

set(gca,'XTickLabel',{})
xlim([2 5])
ylabel('non-linearity coefficient \beta_{D} (\Omega)')

%{
%plot(V2g,pGdis(:,1)*1e-12,'-r');
hold on
semilogy([2.82 2.82],[-72 72],'--k','LineWidth',1)
hold on
semilogy([4.86 4.86],[-72 72],'--k','LineWidth',1)
plot(V2g,pG1(:,1)*1e-12,'-','Color',[0 0.5 0]);
plot(V2g,pD1(:,1)*1e-12,'--','Color',[0 0.5 0]);
plot(V2g(1:60:end),pD1(1:60:end,1)*1e-12,'d','Color',[0 0.5 0]);
plot(V2g(1:60:end),pG1(1:60:end,1)*1e-12,'d','Color',[0 0.5 0]);

ylim([-72 72])
set(gca,'XTickLabel',{})

axes('Position',get(gca,'Position'),...
       'XAxisLocation','bottom',...
       'YAxisLocation','right',...
       'Color','none','Box','on');
hold on
plot(V2g,pG4(:,1)*1e-9,'-b');
plot(V2g,pD4(:,1)*1e-9,'-.b');
plot(V2g(60:60:end),pG4(60:60:end,1)*1e-9,'ob');
plot(V2g(60:60:end),pD4(60:60:end,1)*1e-9,'ob');

ylim([-72 72])
set(gca,'XTickLabel',{})
%legend(H,' ',' ',' ','Location','NorthWest')
%legend boxoff
set(gca,'box','on')
%}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.1 0.05 0.80 0.45])
plot([2.82 2.82],[0 3],'--k','LineWidth',1)
hold on
plot([4.86 4.86],[0 3],'--k','LineWidth',1)
plot([V2g(1) V2g(end)],[1 1],'--k','LineWidth',1)

plot(V2g,abs(pG1(:,1)./pD1(:,1)),'-r')
plot(V2g(20:60:end),abs(pG1(20:60:end,1)./pD1(20:60:end,1)),'+r')
G(1)=plot(1,1,'-+r','MarkerSize',6,'Visible','off','DisplayName','dT=5K');

plot(V2g,abs(pG2(:,1)./pD2(:,1)),'-','Color',[0 0.5 0])
plot(V2g(40:60:end),abs(pG2(40:60:end,1)./pD2(40:60:end,1)),'v','Color',[0 0.5 0])
G(2)=plot(1,1,'-v','Color',[0 0.5 0],'MarkerSize',6,'Visible','off','DisplayName','dT=10K');

plot(V2g,abs(pG4(:,1)./pD3(:,1)),'-k')
plot(V2g(20:60:end),abs(pG3(20:60:end,1)./pD3(20:60:end,1)),'^k')
G(3)=plot(1,1,'-^k','MarkerSize',6,'Visible','off','DisplayName','dT=15K');

plot(V2g,abs(pG3(:,1)./pD4(:,1)),'-','Color',[0.5 0 0.5])
plot(V2g(20:60:end),abs(pG4(20:60:end,1)./pD4(20:60:end,1)),'s','Color',[0.5 0 0.5])
G(4)=plot(1,1,'-s','Color',[0.5 0 0.5],'MarkerSize',6,'Visible','off','DisplayName','dT=20K');

plot(V2g,abs(pG5(:,1)./pD5(:,1)),'-b')
plot(V2g(20:60:end),abs(pG5(20:60:end,1)./pD5(20:60:end,1)),'db')
G(5)=plot(1,1,'-db','MarkerSize',6,'Visible','off','DisplayName','dT=15K');


L=legend(G,'Location',[0.63 0.3 0.1 0.1])
legend boxoff
xlim([2 5])
ylim([0 3])
xlabel('gate voltage (V)')
ylabel('ratio |\beta_{S}/\beta_{D}|')

% H='discret';
% load([dossier,dispo,'/',H,'/IMA_T',num2str(T),'dT',num2str(deltaT),'.mat']);
% R=zeros(1,length(V2g));
% for ivg=1:length(V2g)
%     R(ivg)=V2(./IMA(:,ivg);
% end
% figure
% plot(V2g,R)
% xlabel('gate voltage (V)')
% ylabel('V/I (\Omega)')
