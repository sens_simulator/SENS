% comparaison beta/Ge et Puiss/(I*Vds)

clear
close all
clc
qe=1.602176487e-19;

%--------------------------------------------------------------------------
% À MODIFIER
dispos='44-12-15-50-pp';
H='discret';
deltaT=5;
T=100;
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
VGS=3;
%--------------------------------------------------------------------------


dossierm=[dossier,dispos,'/',H,'/'];

load([dossierm,'IMA_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossierm,'ZT_T',num2str(T),'.mat']);

[Z,iT]=min(abs(dT-deltaT));

IQD=(IQFDd-IQFdD)*qe;
IQG=(IQFdG-IQFGd)*qe;
for iv=1:length(V2)
    for ivg=1:length(V2g)
        Puiss(iv,ivg)=V2(iv)*IMA(iv,ivg)*1e15; %fW
        Puiss_Joule(iv,ivg)=IMA(iv,ivg)*IMA(iv,ivg)/(Ge(ivg))*1e15; %fW
        Puiss_Seebeck(iv,ivg)=-Voc(ivg)*IMA(iv,ivg)*1e15; %fW
        dIQ(iv,ivg)=(IQG(iv,ivg)-IQD(iv,ivg))*1e15; %pW
    end
end

erreur=abs((Puiss-(IQG-IQD))./Puiss);

figure
surf(V2,V2g,erreur')
xlabel('drain voltage (V)')
ylabel('gate voltage (V)')
%title('(Puiss-(IQD-IQS))./Puiss A=0.05 T=100K dT=5K')

[Z,ivg]=min(abs(V2g-VGS));

figure
plot(V2(1:5:end)*1e3,Puiss(1:5:end,ivg),'-rx','LineWidth',2)
hold on
plot(V2(1:4:end)*1e3,dIQ(1:4:end,ivg),'--bo','LineWidth',2,'MarkerSize',4)
xlabel('drain voltage (mV)','FontName','Latin Modern Roman','FontSize',16)
ylabel('(fW)','FontName','Latin Modern Roman','FontSize',16)
legend('Vds \times I','I_Q^S - I_Q^D','FontName',...
    'Latin Modern Roman','FontSize',12,'Location','north')
legend boxoff
%title(['Vgs=',num2str(VGS),'V'])


load([dossierm,'ZT_T',num2str(T),'.mat'])
load([dossierm,'CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat'])
betaS=-pIQG(:,1)*1e9; %Ohms
betaD=pIQD(:,1)*1e9;

figure
semilogy(V2g,betaD+betaS,'r','LineWidth',2)
hold on
semilogy(V2g,1./Ge,'b--','LineWidth',2)
semilogy(V2g(1:7:end),betaD(1:7:end)+betaS(1:7:end),'rx','LineWidth',2,'MarkerSize',5);
semilogy(V2g(1:8:end),1./Ge(1:8:end),'ob','LineWidth',2,'MarkerSize',5);
%G(1)=plot(V2g(1:5:end),betaD(1:5:end)+betaS(1:5:end),'--rx','LineWidth',2,'MarkerSize',5,'DisplayName','\beta_{D}+\beta_{S}');
%G(2)=plot(V2g(1:4:end),1./Ge(1:4:end),'--ob','LineWidth',2,'MarkerSize',5,'DisplayName','1/G_e');
xlabel('gate voltage (V)','FontName','Latin Modern Roman','FontSize',16)
ylabel('\Omega','FontName','Latin Modern Roman','FontSize',16)
xlim([2 5])
%legend(G)
%legend('FontName','Latin Modern Roman','FontSize',16)
%legend boxoff