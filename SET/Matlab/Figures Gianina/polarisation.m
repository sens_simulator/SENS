% Vpol pour un Vgs et un Vds donnés
clear
qe=1.602176487e-19; %en C

%--------------------------------------------------------------------------
% À MODIFIER
dispos='44-12-15-50-pp';
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
VGS=2.67; % Vgs pour lequel on extrait la polarisation
VDS=-0.003; % Vds pour lequel on extrait la polarisation
%--------------------------------------------------------------------------

dossierd=([dossier,'/',dispos]);
load([dossierd,'/',dispos,'.mat'])
load([dossierd,'/Axe.mat']) % en m
load([dossierd,'/Vpol.mat'])

[Z,ivg]=min(abs(Vg-VGS));
[Z,iv]=min(abs(V-VDS));
Vpol_=Vpol2{iv,ivg};
Vpol_z=Vpol_(:,:,Nptz/2);

figure
surf(axex*1e9,axe2y*1e9,Vpol_z')
xlabel('x (nm)')
ylabel('y (nm)')
zlabel('Vpol (V)')
title(['potentiel à Vgs=',num2str(VGS),'V et Vds=',num2str(VDS*1e3),'mV'])

[Z,ixG]=min(abs(axex-(-a-hG)*1e-10)); [Z,ixGd]=min(abs(axex-(-a)*1e-10));
[Z,ixD]=min(abs(axex-(a+hD)*1e-10)); [Z,ixDd]=min(abs(axex-a*1e-10));

Vgauche=Vpol_z(ixGd,Npty/2)-Vpol_z(ixG,Npty/2);
Vdroite=Vpol_z(ixD,Npty/2)-Vpol_z(ixDd,Npty/2);

