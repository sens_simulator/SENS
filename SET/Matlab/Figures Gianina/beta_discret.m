% beta et betaS/betaD en fonction de Vgs dans le cas discret
clear
close all 
clc
qe=1.602176487e-19;


%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
deltaT=5;
T=100;
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

H='discret';
load([dossier,dispo,'/',H,'/IMA_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);

subplot('Position',[0.1 0.5 0.80 0.4])
semilogy([2.82 2.82],[1e8 1e14],'--k','LineWidth',1)
xlim([2 5])
hold on
semilogy([4.86 4.86],[1e8 1e14],'--k','LineWidth',1)
semilogy(V2g,pIQD(:,1)*1e9,'-r','LineWidth',2);
semilogy(V2g(1:20:end),pIQD(1:20:end,1)*1e9,'+r','MarkerSize',4,'LineWidth',1);
semilogy(V2g,abs(pIQG(:,1))*1e9,'-b','LineWidth',2);
semilogy(V2g(30:30:end),abs(pIQG(30:30:end,1))*1e9,'ob','MarkerSize',4,'LineWidth',1);
% G(1)=semilogy(V2g,pIQD(:,1)*1e9,'-+r','LineWidth',2,'DisplayName','\beta_D');
% G(2)=semilogy(V2g,abs(pIQG(:,1))*1e9,'-ob','LineWidth',2,'DisplayName','\beta_S');
% legend(G)
% legend boxoff
% legend('Location','south')
ylabel('(\Omega)','FontName','Latin Modern Roman','FontSize',14)

subplot('Position',[0.1 0.1 0.80 0.4])

plot([2.82 2.82],[0 3],'--k','LineWidth',1)
hold on
plot([4.86 4.86],[0 3],'--k','LineWidth',1)
plot([V2g(1) V2g(end)],[1 1],'--k','LineWidth',1)

plot(V2g,abs(pIQG(:,1)./pIQD(:,1)),'-k','LineWidth',2)
plot(V2g(20:15:end),abs(pIQG(20:15:end,1)./pIQD(20:15:end,1)),'+k','MarkerSize',5)

xlim([2 5])
ylim([0 2])
xlabel('gate voltage (V)','FontName','Latin Modern Roman','FontSize',14)
ylabel('ratio |\beta_{S}/\beta_{D}|','FontName','Latin Modern Roman','FontSize',14)
