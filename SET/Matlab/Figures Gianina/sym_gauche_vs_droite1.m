% alpha*T*I et beta*I^2 pour dT=+/-5K

clear
close all 
clc
qe=1.602176487e-19;

%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-12-50-pp';
H='discret';
deltaT=5;
Temp=100;
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

load([dossier,dispo,'/discret/IMA_T100dT5.mat']);
load([dossier,dispo,'/discret/CoeffFit_T100dT5.mat']);
IQG5=(IQFGd-IQFdG)*qe;
IQD5=(IQFdD-IQFDd)*qe;
Im5=(IQG5+IQD5)./2;
I5=IMA;
pG5=pIQG; pD5=pIQD;

load([dossier,dispo,'/discret/IMA_T100dT-5.mat']);
load([dossier,dispo,'/discret/CoeffFit_T100dT-5.mat']);
IQG_5=(IQFGd-IQFdG)*qe;
IQD_5=(IQFdD-IQFDd)*qe;
Im_5=(IQG_5+IQD_5)./2;
I_5=IMA;
pG_5=pIQG; pD_5=pIQD;

[Z,ivg1]=min(abs(V2g-2.67));
[Z,ivg2]=min(abs(V2g-3.80));

fig=figure;

G(1)=plot(I5(:,ivg1)*1e12,pG5(ivg1,2)*I5(:,ivg1)*1e12,'r','LineWidth',2,'DisplayName','(\alphaT)_{gauche}I_{gauche} \DeltaT=5K');
hold on
G(2)=plot(I5(:,ivg1)*1e12,pG5(ivg1,1)*I5(:,ivg1).*I5(:,ivg1)*1e24,'--r','LineWidth',2,'DisplayName','\beta_{gauche}I_{gauche}^2 \DeltaT=5K');
hold on
G(3)=plot(-I_5(:,ivg1)*1e12,-pD_5(ivg1,2)*I_5(:,ivg1)*1e12,'b','LineWidth',2,'DisplayName','-(\alphaT)_{droite}I_{droite} \DeltaT=-5K');
hold on
G(4)=plot(-I_5(:,ivg1)*1e12,pD_5(ivg1,1)*I_5(:,ivg1).*I_5(:,ivg1)*1e24,'--b','LineWidth',2,'DisplayName','\beta_{droite}I_{droite}^2 \DeltaT=-5K');
xlim([-1 1])
%xlim([-.005 .005])
ymax=get(gca,'ylim');
xmax=get(gca,'xlim');
L=legend(G);

han=axes(fig,'visible','off'); 
han.Title.Visible='on';
han.XLabel.Visible='on';
han.YLabel.Visible='on';
ylabel(han,'heat current (fW)');
xlabel(han,'electronic current (pA)');


fig=figure;
R(1)=plot(I5(:,ivg1)*1e12,pD5(ivg1,2)*I5(:,ivg1)*1e12,'r','LineWidth',2,'DisplayName','(\alphaT)_{droite}I_{droite} \DeltaT=5K');
hold on
R(2)=plot(I5(:,ivg1)*1e12,pD5(ivg1,1)*I5(:,ivg1).*I5(:,ivg1)*1e24,'--r','LineWidth',2,'DisplayName','\beta_{droite}I_{droite}^2 \DeltaT=5K');
hold on
R(3)=plot(-I_5(:,ivg1)*1e12,-pG_5(ivg1,2)*I_5(:,ivg1)*1e12,'b','LineWidth',2,'DisplayName','-(\alphaT)_{gauche}I_{gauche} \DeltaT=-5K');
hold on
R(4)=plot(-I_5(:,ivg1)*1e12,pG_5(ivg1,1)*I_5(:,ivg1).*I_5(:,ivg1)*1e24,'--b','LineWidth',2,'DisplayName','\beta_{gauche}I_{gauche}^2 \DeltaT=-5K');
xlim([-1 1])
%xlim([-.005 .005])
ymax=get(gca,'ylim');
xmax=get(gca,'xlim');
L=legend(R);

han=axes(fig,'visible','off'); 
han.Title.Visible='on';
han.XLabel.Visible='on';
han.YLabel.Visible='on';
ylabel(han,'heat current (fW)');
xlabel(han,'electronic current (pA)');

