% Dans un dispositif symétrique, IQG et IQD en fonction de Vds, pour +/-∆T
clear
close all
qe=1.602176487e-19; %en C

%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
H='discret';
dT=[0 5 10];
T=100;
VGS=2.67;
% Dossier où sont stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------


dossier=[dossier_destination,dispo,'/'];

%% Côté source

load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(dT(1)),'.mat'])
IQ1=(IQFGd-IQFdG)*qe; 
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(dT(2)),'.mat'])
IQ2=(IQFGd-IQFdG)*qe; 
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(dT(3)),'.mat'])
IQ3=(IQFGd-IQFdG)*qe; 
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(-dT(1)),'.mat'])
IQ1_=(IQFDd-IQFdD)*qe; 
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(-dT(2)),'.mat'])
IQ2_=(IQFDd-IQFdD)*qe; 
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(-dT(3)),'.mat'])
IQ3_=(IQFDd-IQFdD)*qe; 


[Z,ivg]=min(abs(V2g-VGS));

figure
plot(-V2(1:7:end)*1e3,IQ1_(1:7:end,ivg)*1e15,'bx','LineWidth',2,'MarkerSize',4);
hold on
G(1)=plot(V2(1:7:end)*1e3,IQ1(1:7:end,ivg)*1e15,'-b','LineWidth',2,'MarkerSize',4,'DisplayName',['dT=',num2str(dT(1)),'K']);

hold on
plot(-V2(1:7:end)*1e3,IQ2_(1:7:end,ivg)*1e15,'rx','LineWidth',2,'MarkerSize',6);
hold on
G(2)=plot(V2(1:7:end)*1e3,IQ2(1:7:end,ivg)*1e15,'-r','LineWidth',2,'MarkerSize',6,'DisplayName',['dT=',num2str(dT(2)),'K']);

hold on
plot(-V2(1:7:end)*1e3,IQ3_(1:7:end,ivg)*1e15,'x','Color',[0 0.5 0],'LineWidth',2,'MarkerSize',4,'DisplayName',['dT=',num2str(dT(3)),'K']);
hold on
G(3)=plot(V2(1:7:end)*1e3,IQ3(1:7:end,ivg)*1e15,'Color',[0 0.5 0],'LineWidth',2,'MarkerSize',4,'DisplayName',['dT=',num2str(dT(3)),'K']);

plot([0,0],[-2,2],'k--','LineWidth',2)
plot([-6,6],[0,0],'k--','LineWidth',2)


x1=[-6 0 0 -6];y1=[0 0 20 20];
x2=[0 6 6 0];y2=[0 0 20 20];
x3=[-6 0 0 -6];y3=[-20 -20 0 0];
x4=[0 6 6 0];y4=[-20 -20 0 0];

patch(x1,y1,'g','FaceAlpha',0.1)
patch(x2,y2,'r','FaceAlpha',0.1)
patch(x3,y3,'b','FaceAlpha',0.1)
patch(x4,y4,'g','FaceAlpha',0.1)

legend(G,'Location','NorthWest')
legend box off

xlabel('drain voltage (mV)')
ylabel('heat current (fW)')

title(['IQS Vgs=',num2str(VGS),'V'])
%% Côté drain

load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(dT(1)),'.mat'])
IQ1=(IQFdD-IQFDd)*qe; 
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(dT(2)),'.mat'])
IQ2=(IQFdD-IQFDd)*qe; 
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(dT(3)),'.mat'])
IQ3=(IQFdD-IQFDd)*qe; 
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(-dT(1)),'.mat'])
IQ1_=(IQFdG-IQFGd)*qe; 
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(-dT(2)),'.mat'])
IQ2_=(IQFdG-IQFGd)*qe; 
load([dossier,'/',H,'/IMA_T',num2str(T),'dT',num2str(-dT(3)),'.mat'])
IQ3_=(IQFdG-IQFGd)*qe; 

[Z,ivg]=min(abs(V2g-VGS));

figure
plot(-V2(1:7:end)*1e3,IQ1_(1:7:end,ivg)*1e15,'bx','LineWidth',2,'MarkerSize',4);
hold on
G(1)=plot(V2(1:7:end)*1e3,IQ1(1:7:end,ivg)*1e15,'-b','LineWidth',2,'MarkerSize',4,'DisplayName',['dT=',num2str(dT(1)),'K']);

hold on
plot(-V2(1:7:end)*1e3,IQ2_(1:7:end,ivg)*1e15,'rx','LineWidth',2,'MarkerSize',6);
hold on
G(2)=plot(V2(1:7:end)*1e3,IQ2(1:7:end,ivg)*1e15,'-r','LineWidth',2,'MarkerSize',6,'DisplayName',['dT=',num2str(dT(2)),'K']);

hold on
plot(-V2(1:7:end)*1e3,IQ3_(1:7:end,ivg)*1e15,'x','Color',[0 0.5 0],'LineWidth',2,'MarkerSize',4,'DisplayName',['dT=',num2str(dT(3)),'K']);
hold on
G(3)=plot(V2(1:7:end)*1e3,IQ3(1:7:end,ivg)*1e15,'Color',[0 0.5 0],'LineWidth',2,'MarkerSize',4,'DisplayName',['dT=',num2str(dT(3)),'K']);

plot([0,0],[-2,2],'k--','LineWidth',2)
plot([-6,6],[0,0],'k--','LineWidth',2)


x1=[-6 0 0 -6];y1=[0 0 25 25];
x2=[0 6 6 0];y2=[0 0 25 25];
x3=[-6 0 0 -6];y3=[-20 -20 0 0];
x4=[0 6 6 0];y4=[-20 -20 0 0];

patch(x1,y1,'g','FaceAlpha',0.1)
patch(x2,y2,'r','FaceAlpha',0.1)
patch(x3,y3,'b','FaceAlpha',0.1)
patch(x4,y4,'g','FaceAlpha',0.1)

legend(G,'Location','NorthWest')
legend box off

xlabel('drain voltage (mV)')
ylabel('heat current (fW)')

title(['IQD Vgs=',num2str(VGS),'V'])




