% alpha, Ge et facteur de puissance pour différents H
clear 
close all

%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
T=100; %K
% Dossier où sont stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------


dossier=[dossier_destination,dispo,'/'];

load([dossier,'/discret/ZT_T',num2str(T),'.mat'])
Ge1=Ge;alpha1=alpha;f1=f;
load([dossier,'/A_0.05/ZT_T',num2str(T),'.mat'])
Ge2=Ge;alpha2=alpha;f2=f;
load([dossier,'/A_1/ZT_T',num2str(T),'.mat'])
Ge3=Ge;alpha3=alpha;f3=f;

%CONDUCTANCE ELECTRONIQUE
subplot('Position',[0.15 0.68 0.80 0.29])
plot([2.92 2.92],[0 8],'--k','LineWidth',1);
hold on
plot([4.92 4.92],[0 8],'--k','LineWidth',1);

plot(V2g,Ge1(:,1)*1e9,'-r','LineWidth',2); hold on
plot(V2g(1:45:end),Ge1(1:45:end,1)*1e9,'+r','MarkerSize',10,'LineWidth',2)
G(1)=plot(V2g,Ge1(:,1)*1e9,'-+r','MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','H=0');

plot(V2g,Ge2(:,1)*1e9,'--','Color',[0 0.5 0],'LineWidth',2); hold on
plot(V2g(30:45:end),Ge2(30:45:end,1)*1e9,'v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2)
G(2)=plot(V2g(1:20:end),Ge2(1:20:end,1)*1e9,'--v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','H=0.05');

plot(V2g,Ge3(:,1)*1e9,'-.b','LineWidth',2); hold on
plot(V2g(30:45:end),Ge3(30:45:end,1)*1e9,'db','MarkerSize',8,'LineWidth',2)
G(3)=plot(V2g,Ge3(:,1)*1e9,'-.db','MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName','H=1');

ylabel('electronic conductance (nS)');
legend(G,'Location','North')
legend boxoff

set(gca,'Xticklabel',{});
set(gca,'YLim',[0 1.3]);
xlim([2 5])
%legend(H,'Location','North')
%grid on

%ALPHA
subplot('Position',[0.15 0.39 0.8 0.29])
plot([2.92 2.92],[-2 2],'--k','LineWidth',1);
hold on
plot([4.92 4.92],[-2 2],'--k','LineWidth',1);
plot([V2g(1) V2g(end)],[0 0],'--k','LineWidth',1);

plot(V2g,alpha1(:,2)*1e3,'-r','LineWidth',2); hold on
plot(V2g(1:40:end),alpha1(1:40:end,2)*1e3,'+r','MarkerSize',10,'LineWidth',2)

plot(V2g,alpha2(:,2)*1e3,'--','Color',[0 0.5 0],'LineWidth',2); hold on
plot(V2g(21:40:end),alpha2(21:40:end,2)*1e3,'v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2)

plot(V2g,alpha3(:,2)*1e3,'-.b','LineWidth',2); hold on
plot(V2g(25:40:end),alpha3(25:40:end,2)*1e3,'db','MarkerSize',8,'LineWidth',2)

set(gca,'Xticklabel',{});
set(gca,'YLim',[-1.2 1.2]);
xlim([2 5])
ylabel('Seebeck coefficient (mV.K^{-1})');

%FACTEUR DE PUISSANCE
subplot('Position',[0.15 0.1 0.8 0.29])
plot([2.92 2.92],[1 40],'--k','LineWidth',1);
hold on
plot([4.92 4.92],[1 40],'--k','LineWidth',1);

plot(V2g,f1(:,2)*1e18,'-r','LineWidth',2); hold on
plot(V2g(1:40:end),f1(1:40:end,2)*1e18,'+r','MarkerSize',10,'LineWidth',2)

plot(V2g,f2(:,2)*1e18,'--','Color',[0 0.5 0],'LineWidth',2); hold on
plot(V2g(21:40:end),f2(21:40:end,2)*1e18,'v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2)

plot(V2g,f3(:,2)*1e18,'-.b','LineWidth',2); hold on
plot(V2g(25:40:end),f3(25:40:end,2)*1e18,'db','MarkerSize',8,'LineWidth',2)
%semilogy(V2g,f4(:,2)*1e18,'b');

ylabel('power factor (aW.K^{-2})');
xlim([2 5])

set(gca,'YLim',[0 38]);
%grid on
xlabel('gate voltage (V)');