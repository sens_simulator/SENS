% Voc en fonction de T à dT constant

clear
close all 
clc
qe=1.602176487e-19;

%--------------------------------------------------------------------------
% A MODIFIER
dispo='44-12-15-50-pp';
deltaT=5;
T=[50:5:100];
H='discret';
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

load([dossier,dispo,'/',H,'/IMA_T',num2str(T(1)),'dT',num2str(deltaT),'.mat']);
[Z,ivg]=min(abs(V2g-2.67));

for iT=1:length(T)
load([dossier,dispo,'/',H,'/ZT_T',num2str(T(iT)),'.mat']);
idT=find(dT==deltaT);
Vt(iT)=Voc(ivg,idT);
end

plot(T,Vt*1e3,'x-')
xlabel('average temperature (K)')
ylabel('open circuit tension (mV)')