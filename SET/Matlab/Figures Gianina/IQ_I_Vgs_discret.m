% IQ-I pour différents élargissements

clear
close all 
clc
qe=1.602176487e-19;


%--------------------------------------------------------------------------
%A modifier
dispo='44-12-15-50-pp';
deltaT=5;
T=100;
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------


load([dossier,dispo,'/discret/IMA_T100dT5.mat']);
load([dossier,dispo,'/discret/CoeffFit_T100dT5.mat']);
load([dossier,dispo,'/discret/ZT_T100.mat']);
IQG5=(IQFGd-IQFdG)*qe;
IQD5=(IQFdD-IQFDd)*qe;
Im5=(IQG5+IQD5)./2;
I5=IMA;
alpha5=alpha;
pG5=pIQG;
pD5=pIQD;
K05=KG;

[Z,ivg1]=min(abs(V2g-2.67));
[Z,ivg2]=min(abs(V2g-3.80));

fig=figure;

subplot(1,2,1) %Vgs=2.67V

plot(I5(:,ivg1)*1e12,IQG5(:,ivg1)*1e15,'b','LineWidth',2);
hold on
plot(I5(:,ivg1)*1e12,IQD5(:,ivg1)*1e15,'--r','LineWidth',2);
hold on
%plot(I5(1:20:end,ivg1)*1e12,IQG5(1:20:end,ivg1)*1e15,'+b','LineWidth',2);
hold on
%plot(I5(1:20:end,ivg1)*1e12,IQD5(1:20:end,ivg1)*1e15,'+r','LineWidth',2);
hold on
plot(I5(:,ivg1)*1e12,Im5(:,ivg1)*1e15,'--k','LineWidth',1);
G(1)=plot(I5(:,ivg1)*1e12,IQG5(:,ivg1)*1e15,'-b','LineWidth',2,'Visible','on','DisplayName','I_Q^S');
G(2)=plot(I5(:,ivg1)*1e12,IQD5(:,ivg1)*1e15,'--r','LineWidth',2,'Visible','on','DisplayName','I_Q^D');
G(3)=plot(I5(:,ivg1)*1e12,Im5(:,ivg1)*1e15,'--k','LineWidth',1,'Visible','on','DisplayName','(I_Q^S+I_Q^D)/2');

legend(G,'FontName','Latin Modern Roman','FontSize',12)
legend boxoff
% xlim([min(I5(:,ivg1))*1e12 max(I5(:,iv1))*1e12]);
% ylim([min(IQG5(:,ivg1))*1e15 max(IQD5(:,iv1))*1e15]);
xlim([-2 2])
ymax=get(gca,'ylim');
xmax=get(gca,'xlim');
ax=gca;
ax.FontName='Latin Modern Roman';
ax.FontSize=12;

text(0.9*xmax(2),0.9*ymax(1),{'V_{GS} = 2.67 V'},'HorizontalAlignment','right','VerticalAlignment','bottom','FontName','Latin Modern Roman','FontSize',18)

subplot(1,2,2) %Vgs=3.80V

plot(I5(:,ivg2)*1e12,IQG5(:,ivg2)*1e15,'b','LineWidth',2);
hold on
plot(I5(:,ivg2)*1e12,IQD5(:,ivg2)*1e15,'--r','LineWidth',2);
hold on
%plot(I5(1:20:end,ivg2)*1e12,IQG5(1:20:end,ivg2)*1e15,'+b','LineWidth',2);
hold on
%plot(I5(1:20:end,ivg2)*1e12,IQD5(1:20:end,ivg2)*1e15,'+r','LineWidth',2);
hold on
plot(I5(:,ivg2)*1e12,Im5(:,ivg2)*1e15,'--k','LineWidth',1);

% xlim([min(I5(:,ivg2))*1e12 max(I5(:,iv2))*1e12]);
% ylim([min(IQG5(:,ivg2))*1e15 max(IQD5(:,iv2))*1e15]);
xlim([-7e-5 7e-5])
ymax=get(gca,'ylim');
xmax=get(gca,'xlim');
ax=gca;
ax.FontName='Latin Modern Roman';
ax.FontSize=12;
text(0.9*xmax(2),1.005*ymax(1),{'V_{GS} = 3.80 V'},'HorizontalAlignment','right','VerticalAlignment','bottom','FontName','Latin Modern Roman','FontSize',18)


han=axes(fig,'visible','off'); 
han.Title.Visible='on';
han.XLabel.Visible='on';
han.YLabel.Visible='on';
ylabel(han,'heat current (fW)','FontName','Latin Modern Roman','FontSize',18);
xlabel(han,'electronic current (pA)','FontName','Latin Modern Roman','FontSize',18);
