% heat current and tunnel transfer rates in open circuit
clear 
close all

%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
deltaT=5;
Temp=100;
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

qe=1.602176487e-19;

load([dossier,dispo,'/discret/IMA_T',num2str(Temp),'dT',num2str(deltaT),'.mat'])
load([dossier,dispo,'/discret/ZT_T',num2str(Temp),'.mat'])
[Z,iT]=min(abs(dT-deltaT));
Voc_=Voc(:,iT);

[Z,ivgmax]=min(abs(V2g-5));
[Z,iv1]=min(abs(V2g-3.25));
[Z,iv2]=min(abs(V2g-4.25));

IQGoc=zeros(ivgmax,1);
GGdoc=zeros(ivgmax,2);
GdGoc=zeros(ivgmax,2);
GDdoc=zeros(ivgmax,2);
GdDoc=zeros(ivgmax,2);

for ivg=1:ivgmax
    IQGoc(ivg)=interp1(V2,IQFGd(:,ivg)-IQFdG(:,ivg),-Voc_(ivg),'cubic')*qe; %W
    GGdoc(ivg,:)=interp1(V2,GGd(:,ivg,1:2),-Voc_(ivg),'pchip'); %Hz
    GdGoc(ivg,:)=interp1(V2,GdG(:,ivg,2:3),-Voc_(ivg),'pchip');
    GDdoc(ivg,:)=interp1(V2,GDd(:,ivg,1:2),-Voc_(ivg),'pchip');
    GdDoc(ivg,:)=interp1(V2,GdD(:,ivg,2:3),-Voc_(ivg),'pchip');
end


subplot('Position',[0.1 0.5 0.80 0.4])
yyaxis left
L(1)=plot(V2g(1:ivgmax),IQGoc*1e18,'k','LineWidth',2,'DisplayName','I_Q^S (I = 0)');
ylim([-0 10])
ylabel('heat current (aW)','FontName','Latin Modern Roman','FontSize',16)

yyaxis right
L(2)=plot(V2g,KG(:,2)*1e18,'Color',[0 0.4470 0.7410],'LineWidth',2,'DisplayName','K_S');
ylim([0 1.5])
ylabel({'thermal conductance';'(aW/K)'},'FontName','Latin Modern Roman','FontSize',15)

xlim([2 5])

ax = gca;
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';
set(gca,'XTickLabel',{})

legend(L)
legend('Location','west','FontName','Latin Modern Roman','FontSize',12)
legend boxoff



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.1 0.1 0.80 0.4])
semilogy(V2g(1:ivgmax),GGdoc(1:ivgmax,1),'Color',[0 0.4470 0.7410],'LineWidth',2)
hold on
semilogy(V2g(1:ivgmax),GGdoc(1:ivgmax,2),'--','Color',[0 0.4470 0.7410],'LineWidth',2)
semilogy(V2g(1:ivgmax),GdGoc(1:ivgmax,1),'r','LineWidth',2)
semilogy(V2g(1:ivgmax),GdGoc(1:ivgmax,2),'r--','LineWidth',2)
semilogy(V2g(1:ivgmax),GDdoc(1:ivgmax,1),'k','LineWidth',2)
semilogy(V2g(1:ivgmax),GDdoc(1:ivgmax,2),'k--','LineWidth',2)
semilogy(V2g(1:ivgmax),GdDoc(1:ivgmax,1),'Color',[0 0.5 0],'LineWidth',2)
semilogy(V2g(1:ivgmax),GdDoc(1:ivgmax,2),'--','Color',[0 0.5 0],'LineWidth',2)
xlim([2 5])

% G(1)=semilogy(V2g(1:ivgmax),GGdoc(1:ivgmax,2),'-','Color',[0 0.4470 0.7410],'LineWidth',2,'DisplayName','\Gamma_{Sd}');
% G(2)=semilogy(V2g(1:ivgmax),GdGoc(1:ivgmax,1),'-r','LineWidth',2,'DisplayName','\Gamma_{dS}');
% G(3)=semilogy(V2g(1:ivgmax),GDdoc(1:ivgmax,1),'-k','LineWidth',2,'DisplayName','\Gamma_{Dd}');
% G(4)=semilogy(V2g(1:ivgmax),GdDoc(1:ivgmax,2),'-','Color',[0 0.5 0],'LineWidth',2,'DisplayName','\Gamma_{dD}');

% G(1)=semilogy(V2g(1:ivgmax),GGdoc(1:ivgmax,2),'-','Color',[.5 0 .5],'LineWidth',2,'DisplayName','\Gamma (0 \leftrightarrow 1)');
% G(2)=semilogy(V2g(1:ivgmax),GdGoc(1:ivgmax,2),'--','Color',[.5 0 .5],'LineWidth',2,'DisplayName','\Gamma (1 \leftrightarrow 2)');
xlabel('gate voltage (V)','FontName','Latin Modern Roman','FontSize',16)
ylabel('transfer rates (Hz)','FontName','Latin Modern Roman','FontSize',16)
% legend(G)
% legend boxoff
% legend('FontSize',12,'FontName','Latin Modern Roman','Location','southwest')
