% beta et betaS/betaD en fonction de H
clear
close all 
clc
qe=1.602176487e-19;


%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-15-50-pp';
deltaT=5; %K
T=100; %K
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

H='discret';
load([dossier,dispo,'/',H,'/IMA_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pGdis=pIQG;pDdis=pIQD;
H='A_0.05';
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pG1=pIQG;pD1=pIQD;
H='A_0.1';
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pG2=pIQG;pD2=pIQD;
H='A_0.5';
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pG3=pIQG;pD3=pIQD;
H='A_1';
load([dossier,dispo,'/',H,'/CoeffFit_T',num2str(T),'dT',num2str(deltaT),'.mat']);pG4=pIQG;pD4=pIQD;


% FigureFormat(6,6,16,2);
subplot('Position',[0.1 0.5 0.80 0.45])
semilogy([2.82 2.82],[1e8 1e14],'--k','LineWidth',1)
hold on
semilogy([4.86 4.86],[1e8 1e14],'--k','LineWidth',1)

semilogy(V2g,pDdis(:,1)*1e9,'-r','LineWidth',2);
semilogy(V2g(30:30:end),pDdis(30:30:end,1)*1e9,'+r','MarkerSize',5,'LineWidth',2);

semilogy(V2g,pD1(:,1)*1e9,'-','Color',[0 0.5 0],'LineWidth',2);
semilogy(V2g(10:30:end),pD1(10:30:end,1)*1e9,'v','Color',[0 0.5 0],'MarkerSize',5,'LineWidth',2);


semilogy(V2g,pD2(:,1)*1e9,'-k','LineWidth',2);
semilogy(V2g(20:30:end),pD2(20:30:end,1)*1e9,'^k','MarkerSize',5,'LineWidth',2);

semilogy(V2g,pD3(:,1)*1e9,'-','Color',[0.5 0 0.5],'LineWidth',2);
semilogy(V2g(1:30:end),pD3(1:30:end,1)*1e9,'s','Color',[0.5 0 0.5],'MarkerSize',5,'LineWidth',2);

semilogy(V2g,pD4(:,1)*1e9,'-b','LineWidth',2);
semilogy(V2g(1:30:end),pD4(1:30:end,1)*1e9,'db','MarkerSize',5,'LineWidth',2);

set(gca,'XTickLabel',{})
xlim([2 5])
ylabel('non-linearity coefficient \beta_{D} (\Omega)')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.1 0.05 0.80 0.45])
plot([2.82 2.82],[0 3],'--k','LineWidth',1)
hold on
plot([4.86 4.86],[0 3],'--k','LineWidth',1)
plot([V2g(1) V2g(end)],[1 1],'--k','LineWidth',1)

plot(V2g,abs(pGdis(:,1)./pDdis(:,1)),'-r','LineWidth',2)
plot(V2g(60:60:end),abs(pGdis(60:60:end,1)./pDdis(60:60:end,1)),'+r','LineWidth',2)
G(1)=plot(1,1,'-+r','MarkerSize',5,'Visible','off','DisplayName','H = 0','LineWidth',2);

plot(V2g,abs(pG1(:,1)./pD1(:,1)),'-','Color',[0 0.5 0],'LineWidth',2)
plot(V2g(20:60:end),abs(pG1(20:60:end,1)./pD1(20:60:end,1)),'v','Color',[0 0.5 0],'LineWidth',2)
G(2)=plot(1,1,'-v','Color',[0 0.5 0],'MarkerSize',5,'Visible','off','DisplayName','H = 5\times10^{-2}k_B T','LineWidth',2);

plot(V2g,abs(pG2(:,1)./pD2(:,1)),'-k','LineWidth',2)
plot(V2g(40:60:end),abs(pG2(40:60:end,1)./pD2(40:60:end,1)),'^k','LineWidth',2)
G(3)=plot(1,1,'-^k','MarkerSize',5,'Visible','off','DisplayName',' ','LineWidth',2);

plot(V2g,abs(pG3(:,1)./pD3(:,1)),'-','Color',[0.5 0 0.5],'LineWidth',2)
plot(V2g(20:60:end),abs(pG3(20:60:end,1)./pD3(20:60:end,1)),'s','Color',[0.5 0 0.5],'LineWidth',2)
G(4)=plot(1,1,'-s','Color',[0.5 0 0.5],'MarkerSize',5,'Visible','off','DisplayName','H = 5\times10^{-2}k_B T','LineWidth',2);


plot(V2g,abs(pG4(:,1)./pD4(:,1)),'-b','LineWidth',2)
plot(V2g(30:60:end),abs(pG4(30:60:end,1)./pD4(30:60:end,1)),'db','LineWidth',2)
G(3)=plot(1,1,'-db','MarkerSize',5,'Visible','off','DisplayName','H = k_B T','LineWidth',2);

L=legend(G,'Location',[0.63 0.3 0.1 0.1]);
legend boxoff
xlim([2 5])
ylim([0 3])
xlabel('gate voltage (V)')
ylabel('ratio |\beta_{S}/\beta_{D}|')

