% Dans un dispositif symétrique, IQ-I pour dT=+/-5K, à droite et à gauche

clear
close all 
clc
qe=1.602176487e-19;

%--------------------------------------------------------------------------
% À MODIFIER
dispo='44-12-12-50-pp';
H='discret';
deltaT=5;
Temp=100;
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------

load([dossier,dispo,'/discret/IMA_T100dT5.mat']);
load([dossier,dispo,'/discret/CoeffFit_T100dT5.mat']);
IQG5=(IQFGd-IQFdG)*qe;
IQD5=(IQFdD-IQFDd)*qe;
Im5=(IQG5+IQD5)./2;
I5=IMA;

load([dossier,dispo,'/discret/IMA_T100dT-5.mat']);
load([dossier,dispo,'/discret/CoeffFit_T100dT-5.mat']);
IQG_5=(IQFGd-IQFdG)*qe;
IQD_5=(IQFdD-IQFDd)*qe;
Im_5=(IQG5+IQD5)./2;
I_5=IMA;

[Z,ivg1]=min(abs(V2g-2.67));
[Z,ivg2]=min(abs(V2g-3.80));

fig=figure;

G(1)=plot(I5(:,ivg1)*1e12,IQG5(:,ivg1)*1e15,'b','LineWidth',2,'DisplayName','I_Q^{gauche} dT=5K');
hold on
G(2)=plot(I5(:,ivg1)*1e12,IQD5(:,ivg1)*1e15,'r','LineWidth',2,'DisplayName','I_Q^{droite} dT=5K');
hold on
G(3)=plot(-I_5(:,ivg1)*1e12,-IQD_5(:,ivg1)*1e15,'--b','LineWidth',2,'DisplayName','I_Q^{droite} dT=-5K');
hold on
G(4)=plot(-I_5(:,ivg1)*1e12,-IQG_5(:,ivg1)*1e15,'--r','LineWidth',2,'DisplayName','I_Q^{gauche} dT=-5K');

xlim([-1 1])
ymax=get(gca,'ylim');
xmax=get(gca,'xlim');
L=legend(G);

han=axes(fig,'visible','off'); 
han.Title.Visible='on';
han.XLabel.Visible='on';
han.YLabel.Visible='on';
ylabel(han,'heat current (fW)');
xlabel(han,'electronic current (pA)');