clear
close all 
clc
qe=1.602176487e-19;
load(['44-12-15-50-pp/discret/IMA_T100dT5.mat']);
load(['44-12-15-50-pp/discret/CoeffFit_T100dT5.mat']);
load(['44-12-15-50-pp/discret/ZT_T100.mat']);
IQGdis=(IQFGd-IQFdG)*qe;
IQDdis=(IQFdD-IQFDd)*qe;
Idis=IMA;
alphadis=alpha;
pGdis=pIQG2;
pDdis=pIQD2;
K0dis=KG;

load(['44-12-15-50-pp/A_0.05/IMA_T100dT5.mat']);
load(['44-12-15-50-pp/A_0.05/CoeffFit_T100dT5.mat']);
load(['44-12-15-50-pp/A_0.05/ZT_T100.mat']);

IQG5=(IQFGd-IQFdG)*qe;
IQD5=(IQFdD-IQFDd)*qe;
I5=IMA;
alpha5=alpha;
pG5=pIQG2;
pD5=pIQD2;
K05=KG;
KD05=KD;


load(['44-12-15-50-pp/A_1/IMA_T100dT5.mat']);
load(['44-12-15-50-pp/A_1/CoeffFit_T100dT5.mat']);
load(['44-12-15-50-pp/A_1/ZT_T100.mat']);
IQG1=(IQFGd-IQFdG)*qe;
IQD1=(IQFdD-IQFDd)*qe;
I1=IMA;
alpha1=alpha;
pG1=pIQG2;
pD1=pIQD2;
K01=KG;

FigureFormat(6.5,8,16,2);
subplot('Position',[0.15 0.65 0.7 0.3]);
IQ=pG5(362,1).*I5(:,362).*I5(:,362)+pG5(362,2).*Idis(:,362)+K05(362,2)*5;
IQDD=pD5(362,1).*I5(:,362).*I5(:,362)+pD5(362,2).*Idis(:,362)+KD05(362,2)*5;
plot(I5(:,362)*1e18,IQG5(:,362)*1e18,'LineWidth',2);
hold on
plot(I5(:,362)*1e18,IQD5(:,362)*1e18,'--r','LineWidth',2);
plot(I5(1:10:end,362)*1e18,IQ(1:10:end)*1e18,'+b','LineWidth',2);
plot(I5(1:10:end,362)*1e18,IQDD(1:10:end)*1e18,'+r','LineWidth',2);

%plot(I1(:,362)*1e15,IQG1(:,362)*1e16,'--b');
%plot(I1(:,362)*1e15,IQD1(:,362)*1e16,'--r');
xlim([min(I5(:,362))*1e18 max(I5(:,362))*1e18]);
ylim([min(IQG5(:,362))*1e18 max(IQD5(:,362))*1e18]);

%xlabel('Courant(aA)')
%ylabel('Courant de Chaleur I^{Q} (aW)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.15 0.30 0.7 0.25]);
%plot(V2g,pGdis(:,2)*10,'-r');
hold on
%plot(V2g(1:60:end),alphadis(1:60:end,2)*1e3,'or');
plot(V2g,pG5(:,2)*10,'-','Color',[0 0.5 0]);
plot(V2g(1:60:end),alpha5(1:60:end,2)*1e3,'o','Color',[0 0.5 0]);
plot(V2g,pG1(:,2)*10,'--b');
plot(V2g(1:60:end),alpha1(1:60:end,2)*1e3,'ob');
set(gca,'XTickLabel',{})
%legend(H,' ',' ',' ','Location','NorthWest')
%legend boxoff
set(gca,'box','on')
ylim([-1.1 1.3])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.15 0.05 0.7 0.25]);
%plot(V2g,pGdis(:,1)*1e-12,'-r');
hold on
plot(V2g,pG5(:,1)*1e-12,'-','Color',[0 0.5 0]);
plot(V2g,pD5(:,1)*1e-12,'-.','Color',[0 0.5 0]);
ylim([-72 72])

axes('Position',get(gca,'Position'),...
       'XAxisLocation','bottom',...
       'YAxisLocation','right',...
       'Color','none','Box','on');
hold on
plot(V2g,pG1(:,1)*1e-9,'--b');
plot(V2g,pD1(:,1)*1e-9,'-.b');

ylim([-72 72])
set(gca,'XTickLabel',{})
%legend(H,' ',' ',' ','Location','NorthWest')
%legend boxoff
set(gca,'box','on')

