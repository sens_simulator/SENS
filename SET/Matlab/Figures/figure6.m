%Figure6
clear 
close all
dispo='44-12-15-50-pp';
qe=1.602176487e-19;

load([dispo,'/discret/ZT_T100.mat'])
K0=KG;K0dis=K0*qe;Zdis=ZT;VocDis=Voc;

load([dispo,'/A_0.001/ZT_T100.mat'])
K0=KG;K01=K0*qe;Z1=ZT;Voc1=Voc;
load([dispo,'/A_0.05/ZT_T100.mat'])
K0=KG;K2=K0*qe;Z2=ZT;
load([dispo,'/A_1/ZT_T100.mat'])
K0=KG;K3=K0*qe;Z3=ZT;

FigureFormat(7,7,16,2);
subplot('Position',[0.15 0.5 0.7 0.45]);

hold on
plot(V2g,-K2(:,2)*1e18,'-','Color',[0 0.5 0])
ylim([0 50])
set(gca,'XTickLabel',{})
axes('Position',get(gca,'Position'),...
       'XAxisLocation','bottom',...
       'YAxisLocation','right',...
       'Color','none','Box','on');
hold on
plot(V2g,-K3(:,2)*1e18,'--b')
set(gca,'YTick',[100 200 300 400 500])
set(gca,'XTickLabel',{})

subplot('Position',[0.15 0.05 0.7 0.45]);
hold on

plot(V2g,-Z2(:,2),'-','Color',[0 0.5 0])
axes('Position',get(gca,'Position'),...
       'XAxisLocation','bottom',...
       'YAxisLocation','right',...
       'Color','none','Box','on');
   hold on
plot(V2g,-Z3(:,2),'--b')

%saveas2('figure6.pdf')