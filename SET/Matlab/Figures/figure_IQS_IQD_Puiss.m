%Figure IQS IQD Puissance

clear;
close all
qe=1.602176487e-19;

load(['44-12-15-50-pp/discret/IMA_T100dT5.mat']);
load(['44-12-15-50-pp/discret/ZT_T100.mat']);

IQGdis=(IQFGd-IQFdG)*qe;
IQDdis=(IQFdD-IQFDd)*qe;
Diffdis=IQGdis-IQDdis;
V=repmat(V2,[601 1]);
Pdis=V'.*IMA;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load(['44-12-15-50-pp/A_0.05/IMA_T100dT5.mat']);
IQG5=(IQFGd-IQFdG)*qe;
IQD5=(IQFdD-IQFDd)*qe;
Diff5=IQG5-IQD5;
V=repmat(V2,[601 1]);
P5=V'.*IMA;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load(['44-12-15-50-pp/A_1/IMA_T100dT5.mat']);
IQG1=(IQFGd-IQFdG)*qe;
IQD1=(IQFdD-IQFDd)*qe;
Diff1=IQG1-IQD1;
V=repmat(V2,[601 1]);
P1=V'.*IMA;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FigureFormat(6.5,7.5,16,2);
subplot('Position',[0.15 0.65 0.7 0.3]);

plot([Voc(543,2)*1e3 Voc(543,2)*1e3],[-110 110],':k','LineWidth',1);
hold on
plot([0 0],[-110 110],':k','LineWidth',1);
H(1)=plot(V2*1e3,IQGdis(:,543)*1e15,'-k','LineWidth',2);
H(2)=plot(V2(5:10:end)*1e3,IQG5(5:10:end,543)*1e15,'xk','LineWidth',2);
H(3)=plot(V2(5:10:end)*1e3,IQG1(5:10:end,543)*1e15,'ok','LineWidth',2);
ylim([-80 110])
set(gca,'XTickLabel',{})
%legend(H,' ',' ',' ','Location','NorthWest')
%legend boxoff
set(gca,'box','on')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subplot('Position',[0.15 0.35 0.7 0.3]);

plot([Voc(543,2)*1e3 Voc(543,2)*1e3],[-110 110],':k','LineWidth',1);
hold on
plot([0 0],[-110 110],':k','LineWidth',1);
H(1)=plot(V2*1e3,IQDdis(:,543)*1e15,'-k','LineWidth',2);
H(2)=plot(V2(5:10:end)*1e3,IQD5(5:10:end,543)*1e15,'xk','LineWidth',2);
H(3)=plot(V2(5:10:end)*1e3,IQD1(5:10:end,543)*1e15,'ok','LineWidth',2);
ylim([-80 110])
set(gca,'XTickLabel',{})
set(gca,'box','on')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subplot('Position',[0.15 0.05 0.7 0.3]);
plot([Voc(543,2)*1e3 Voc(543,2)*1e3],[-110 110],':k','LineWidth',1);
hold on
plot([0 0],[-110 110],':k','LineWidth',1);
H(1)=plot(V2*1e3,-Pdis(:,543)*1e15,'--k');
hold on
H(2)=plot(V2(1:10:end)*1e3,Diffdis(1:10:end,543)*1e15,'db');
legend(H,' ',' ','Location','SouthWest')
legend boxoff
set(gca,'box','on')
ylim([-25 2])
%xlim([Voc(543,2)*1e3 0])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
