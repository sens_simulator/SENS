%Figure pour IEEE NANO 2018
clear; close all;

load('44-12-15-50-pp/discret/ZT_T100.mat')

[~,ivgmax]=max(Voc(:,22));

alphatest=alpha(ivgmax,2:22)';
pAlpha=polyfit(dT(2:22)',alphatest,1);


plot(dT,Voc(ivgmax,:)*1e3)
 
FigureFormat(7,6,20,2);

plot(dT(2:22),alpha(ivgmax,2:22)*1e3)
hold on
plot(dT(2:22),pAlpha(1)*dT(2:22)*1e3+pAlpha(2)*1e3,'--r')
xlabel('Temperature difference between leads (K)')
ylabel('Seebeck coefficient as Voc/\Delta T (mV.K^{-1})')
legend('simulation','quadratic fit')


