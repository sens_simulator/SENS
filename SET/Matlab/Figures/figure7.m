%Figure7
clear
close all 
clc
qe=1.602176487e-19;
load(['44-12-15-50-pp/discret/CoeffFit_T100dT5.mat']);pGdis=pIQG;pDdis=pIQD;
load(['44-12-15-50-pp/A_0.05/CoeffFit_T100dT5.mat']);pG1=pIQG;pD1=pIQD;
load(['44-12-15-50-pp/A_0.1/CoeffFit_T100dT5.mat']);pG2=pIQG;pD2=pIQD;
load(['44-12-15-50-pp/A_0.5/CoeffFit_T100dT5.mat']);pG3=pIQG;pD3=pIQD;
load(['44-12-15-50-pp/A_1/CoeffFit_T100dT5.mat']);pG4=pIQG;pD4=pIQD;
load(['44-12-15-50-pp/discret/IMA_T100dT5.mat']);


FigureFormat(6,6,16,2);
subplot('Position',[0.1 0.5 0.80 0.45])
semilogy([2.82 2.82],[1e8 1e14],'--k','LineWidth',1)
hold on
semilogy([4.86 4.86],[1e8 1e14],'--k','LineWidth',1)

semilogy(V2g,pDdis(:,1),'-r');
semilogy(V2g(30:30:end),pDdis(30:30:end,1),'+r','MarkerSize',6);

semilogy(V2g,pD1(:,1),'-','Color',[0 0.5 0]);
semilogy(V2g(10:30:end),pD1(10:30:end,1),'v','Color',[0 0.5 0],'MarkerSize',6);

semilogy(V2g,pD2(:,1),'-k');
semilogy(V2g(20:30:end),pD2(20:30:end,1),'^k','MarkerSize',6);

semilogy(V2g,pD3(:,1),'-','Color',[0.5 0 0.5]);
semilogy(V2g(1:30:end),pD3(1:30:end,1),'s','Color',[0.5 0 0.5],'MarkerSize',6);

semilogy(V2g,pD4(:,1),'-b');
semilogy(V2g(1:30:end),pD4(1:30:end,1),'db','MarkerSize',6);

set(gca,'XTickLabel',{})

%{
%plot(V2g,pGdis(:,1)*1e-12,'-r');
hold on
semilogy([2.82 2.82],[-72 72],'--k','LineWidth',1)
hold on
semilogy([4.86 4.86],[-72 72],'--k','LineWidth',1)
plot(V2g,pG1(:,1)*1e-12,'-','Color',[0 0.5 0]);
plot(V2g,pD1(:,1)*1e-12,'--','Color',[0 0.5 0]);
plot(V2g(1:60:end),pD1(1:60:end,1)*1e-12,'d','Color',[0 0.5 0]);
plot(V2g(1:60:end),pG1(1:60:end,1)*1e-12,'d','Color',[0 0.5 0]);

ylim([-72 72])
set(gca,'XTickLabel',{})

axes('Position',get(gca,'Position'),...
       'XAxisLocation','bottom',...
       'YAxisLocation','right',...
       'Color','none','Box','on');
hold on
plot(V2g,pG4(:,1)*1e-9,'-b');
plot(V2g,pD4(:,1)*1e-9,'-.b');
plot(V2g(60:60:end),pG4(60:60:end,1)*1e-9,'ob');
plot(V2g(60:60:end),pD4(60:60:end,1)*1e-9,'ob');

ylim([-72 72])
set(gca,'XTickLabel',{})
%legend(H,' ',' ',' ','Location','NorthWest')
%legend boxoff
set(gca,'box','on')
%}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.1 0.05 0.80 0.45])
plot([2.82 2.82],[0 3],'--k','LineWidth',1)
hold on
plot([4.86 4.86],[0 3],'--k','LineWidth',1)
plot([V2g(1) V2g(end)],[1 1],'--k','LineWidth',1)

plot(V2g,abs(pGdis(:,1)./pDdis(:,1)),'-r')
plot(V2g(60:60:end),abs(pGdis(60:60:end,1)./pDdis(60:60:end,1)),'+r')
G(1)=plot(1,1,'-+r','MarkerSize',6,'Visible','off','DisplayName',' ');

plot(V2g,abs(pG1(:,1)./pD1(:,1)),'-','Color',[0 0.5 0])
plot(V2g(20:60:end),abs(pG1(20:60:end,1)./pD1(20:60:end,1)),'v','Color',[0 0.5 0])
G(2)=plot(1,1,'-v','Color',[0 0.5 0],'MarkerSize',6,'Visible','off','DisplayName',' ');

plot(V2g,abs(pG2(:,1)./pD2(:,1)),'-k')
plot(V2g(40:60:end),abs(pG2(40:60:end,1)./pD2(40:60:end,1)),'^k')
G(3)=plot(1,1,'-^k','MarkerSize',6,'Visible','off','DisplayName',' ');

plot(V2g,abs(pG3(:,1)./pD3(:,1)),'-','Color',[0.5 0 0.5])
plot(V2g(20:60:end),abs(pG3(20:60:end,1)./pD3(20:60:end,1)),'s','Color',[0.5 0 0.5])
G(4)=plot(1,1,'-s','Color',[0.5 0 0.5],'MarkerSize',6,'Visible','off','DisplayName',' ');


plot(V2g,abs(pG4(:,1)./pD4(:,1)),'-b')
plot(V2g(30:60:end),abs(pG4(30:60:end,1)./pD4(30:60:end,1)),'db')
G(5)=plot(1,1,'-db','MarkerSize',6,'Visible','off','DisplayName',' ');

L=legend(G,'Location',[0.63 0.3 0.1 0.1])
legend boxoff

ylim([0 3])
