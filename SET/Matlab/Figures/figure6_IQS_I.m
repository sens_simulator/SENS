clear
close all 
clc
qe=1.602176487e-19;
load(['44-12-15-50-pp/discret/IMA_T100dT5.mat']);
load(['44-12-15-50-pp/discret/CoeffFit_T100dT5.mat']);
load(['44-12-15-50-pp/discret/ZT_T100.mat']);
IQGdis=(IQFGd-IQFdG)*qe;
IQDdis=(IQFdD-IQFDd)*qe;
Imdis=(IQGdis+IQDdis)./2;
Idis=IMA;
alphadis=alpha;
pGdis=pIQG3;
pDdis=pIQD3;
K0dis=KG;

load(['44-12-15-50-pp/A_0.05/IMA_T100dT5.mat']);
load(['44-12-15-50-pp/A_0.05/CoeffFit_T100dT5.mat']);
load(['44-12-15-50-pp/A_0.05/ZT_T100.mat']);

IQG5=(IQFGd-IQFdG)*qe;
IQD5=(IQFdD-IQFDd)*qe;
I5=IMA;
alpha5=alpha;
pG5=pIQG;
pD5=pIQD;
K05=KG;
KD05=KD;
Im5=(IQG5+IQD5)./2;


load(['44-12-15-50-pp/A_1/IMA_T100dT5.mat']);
load(['44-12-15-50-pp/A_1/CoeffFit_T100dT5.mat']);
load(['44-12-15-50-pp/A_1/ZT_T100.mat']);
IQG1=(IQFGd-IQFdG)*qe;
IQD1=(IQFdD-IQFDd)*qe;
I1=IMA;
alpha1=alpha;
pG1=pIQG;
pD1=pIQD;
K01=KG;
KD01=KD;
Im1=(IQG1+IQD1)./2;



FigureFormat(8,8,16,2);
%%
subplot(222);

IQ=pG5(362,1).*I5(:,362).*I5(:,362)+pG5(362,2).*I5(:,362)+K05(362,2)*5;
IQDD=pD5(362,1).*I5(:,362).*I5(:,362)+pD5(362,2).*I5(:,362)+KD05(362,2)*5;
plot(I5(:,362)*1e12,IQG5(:,362)*1e15,'LineWidth',2);
hold on
plot(I5(:,362)*1e12,IQD5(:,362)*1e15,'--r','LineWidth',2);
plot(I5(1:30:end,362)*1e12,IQ(1:30:end)*1e15,'+b','LineWidth',2);
plot(I5(1:30:end,362)*1e12,IQDD(1:30:end)*1e15,'+r','LineWidth',2);

plot(I5(:,362)*1e12,Im5(:,362)*1e15,'--k','LineWidth',1);

xlim([min(I5(:,362))*1e12 max(I5(:,362))*1e12]);
ylim([min(IQG5(:,362))*1e15 max(IQD5(:,362))*1e15]);

%xlabel('Courant(aA)')
%ylabel('Courant de Chaleur I^{Q} (aW)')

%%
subplot(221);

IQ=pG5(135,1).*I5(:,135).*I5(:,135)+pG5(135,2).*I5(:,135)+K05(135,2)*5;
IQDD=pD5(135,1).*I5(:,135).*I5(:,135)+pD5(135,2).*I5(:,135)+KD05(135,2)*5;
plot(I5(:,135)*1e12,IQG5(:,135)*1e15,'LineWidth',2);
hold on
plot(I5(:,135)*1e12,IQD5(:,135)*1e15,'--r','LineWidth',2);
plot(I5(1:30:end,135)*1e12,IQ(1:30:end)*1e15,'+b','LineWidth',2);
plot(I5(1:30:end,135)*1e12,IQDD(1:30:end)*1e15,'+r','LineWidth',2);
plot(I5(:,135)*1e12,Im5(:,135)*1e15,'--k','LineWidth',1);


xlim([-2 2]);
ylim([-50 63]);

%xlabel('Courant(aA)')
%ylabel('Courant de Chaleur I^{Q} (aW)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
subplot(224);

IQ=pG1(362,1).*I1(:,362).*I1(:,362)+pG1(362,2).*I1(:,362)+K01(362,2)*5;
IQDD=pD1(362,1).*I1(:,362).*I1(:,362)+pD1(362,2).*I1(:,362)+KD01(362,2)*5;
plot(I1(:,362)*1e12,IQG1(:,362)*1e15,'LineWidth',2);
hold on
plot(I1(:,362)*1e12,IQD1(:,362)*1e15,'--r','LineWidth',2);
plot(I1(1:30:end,362)*1e12,IQ(1:30:end)*1e15,'+b','LineWidth',2);
plot(I1(1:30:end,362)*1e12,IQDD(1:30:end)*1e15,'+r','LineWidth',2);
plot(I1(:,362)*1e12,Im1(:,362)*1e15,'--k','LineWidth',1);


xlim([min(I1(:,362))*1e12 max(I1(:,362))*1e12]);
ylim([min(IQG1(:,362))*1e15 max(IQD1(:,362))*1e15]);

%xlabel('Courant(aA)')
%ylabel('Courant de Chaleur I^{Q} (aW)')

%%
subplot(223);

IQ=pG1(135,1).*I1(:,135).*I1(:,135)+pG1(135,2).*I1(:,135)+K01(135,2)*5;
IQDD=pD1(135,1).*I1(:,135).*I1(:,135)+pD1(135,2).*I1(:,135)+KD01(135,2)*5;
plot(I1(:,135)*1e12,IQG1(:,135)*1e15,'LineWidth',2);
hold on
plot(I1(:,135)*1e12,IQD1(:,135)*1e15,'--r','LineWidth',2);
plot(I1(1:30:end,135)*1e12,IQ(1:30:end)*1e15,'+b','LineWidth',2);
plot(I1(1:30:end,135)*1e12,IQDD(1:30:end)*1e15,'+r','LineWidth',2);
plot(I1(:,135)*1e12,Im1(:,135)*1e15,'--k','LineWidth',1);

xlim([-2 2]);
ylim([-50 63]);

%{

subplot('Position',[0.15 0.30 0.7 0.25]);
%plot(V2g,pGdis(:,2)*10,'-r');
hold on
%plot(V2g(1:60:end),alphadis(1:60:end,2)*1e3,'or');
plot(V2g,pG5(:,2)*10,'-','Color',[0 0.5 0]);
plot(V2g(1:60:end),alpha5(1:60:end,2)*1e3,'o','Color',[0 0.5 0]);
plot(V2g,pG1(:,2)*10,'--b');
plot(V2g(1:60:end),alpha1(1:60:end,2)*1e3,'ob');
set(gca,'XTickLabel',{})
%legend(H,' ',' ',' ','Location','NorthWest')
%legend boxoff
set(gca,'box','on')
ylim([-1.1 1.3])
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%{
FigureFormat(6,5,16,2);
load(['44-12-15-50-pp/A_0.001/CoeffFit_T100dT5.mat']);pG(1)=min(pIQG(:,1));pD(1)=max(pIQD(:,1));
load(['44-12-15-50-pp/A_0.005/CoeffFit_T100dT5.mat']);pG(2)=min(pIQG(:,1));pD(2)=max(pIQD(:,1));
load(['44-12-15-50-pp/A_0.01/CoeffFit_T100dT5.mat']);pG(3)=min(pIQG(:,1));pD(3)=max(pIQD(:,1));
load(['44-12-15-50-pp/A_0.05/CoeffFit_T100dT5.mat']);pG(4)=min(pIQG(:,1));pD(4)=max(pIQD(:,1));
load(['44-12-15-50-pp/A_0.1/CoeffFit_T100dT5.mat']);pG(5)=min(pIQG(:,1));pD(5)=max(pIQD(:,1));
load(['44-12-15-50-pp/A_0.5/CoeffFit_T100dT5.mat']);pG(6)=min(pIQG(:,1));pD(6)=max(pIQD(:,1));
load(['44-12-15-50-pp/A_1/CoeffFit_T100dT5.mat']);pG(7)=min(pIQG(:,1));pD(7)=max(pIQD(:,1));

AA=[0.001 0.005 0.01 0.05 0.1 0.5 1];

loglog(AA,pD)
%}