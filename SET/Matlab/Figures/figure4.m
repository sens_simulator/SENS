%figure4
clear 
close all
dispo='44-12-15-50-pp';

load([dispo,'/Energies.mat']);
qe=1.602176487e-19;
T=100;
U=(E2-EFD)./(T);
%On se met pour V2 = 0 V
iv=121;

H='discret';
dossierm=[dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);

%FigureFormat(8,6,24,2) 
%set(gca,'Position',[0.15 0.18 0.8 0.8])

plot(U(iv,:,1)*1e3,U(iv,:,1)*1e3,'--r');
hold on

H='discret';
dossierm=[dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
plot(U(iv,:,1)*1e3,alpha(:,2)*1e3,'-r'); hold on
plot(U(iv,1:20:end,1)*1e3,alpha(1:20:end,2)*1e3,'+r','MarkerSize',8,'LineWidth',2)
G(1)=plot(U(iv,1:20:end,1)*1e3,alpha(1:20:end,2)*1e3,'-+r','MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName',' ');

H='A_0.05';
dossierm=[dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
plot(U(iv,:,1)*1e3,alpha(:,2)*1e3,'-','Color',[0 0.5 0]); hold on
plot(U(iv,14:20:end,1)*1e3,alpha(14:20:end,2)*1e3,'v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2)
G(2)=plot(U(iv,15:20:end,1)*1e3,alpha(15:20:end,2)*1e3,'-v','Color',[0 0.5 0],'MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName',' ');

H='A_0.1';
dossierm=[dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
plot(U(iv,:,1)*1e3,alpha(:,2)*1e3,'-k'); hold on
plot(U(iv,7:20:end,1)*1e3,alpha(7:20:end,2)*1e3,'^k','MarkerSize',8,'LineWidth',2)
G(3)=plot(U(iv,5:20:end,1)*1e3,alpha(5:20:end,2)*1e3,'-^k','MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName',' ');


H='A_0.5';
dossierm=[dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
plot(U(iv,:,1)*1e3,alpha(:,2)*1e3,'-','Color',[0.5 0 0.5]); hold on
plot(U(iv,7:20:end,1)*1e3,alpha(7:20:end,2)*1e3,'s','Color',[0.5 0 0.5],'MarkerSize',8,'LineWidth',2)
G(4)=plot(U(iv,5:20:end,1)*1e3,alpha(5:20:end,2)*1e3,'-s','Color',[0.5 0 0.5],'MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName',' ');



H='A_1';
dossierm=[dispo,'/',H,'/'];
load([dossierm,'ZT_T',num2str(T),'.mat']);
plot(U(iv,:,1)*1e3,alpha(:,2)*1e3,'-b'); hold on
plot(U(iv,5:20:end,1)*1e3,alpha(5:20:end,2)*1e3,'db','MarkerSize',8,'LineWidth',2)
G(5)=plot(U(iv,5:20:end,1)*1e3,alpha(5:20:end,2)*1e3,'-db','MarkerSize',8,'LineWidth',2,'Visible','off','DisplayName',' ');


legend(G,'Location','NorthWest')
legend boxoff
xlim([-1 1])
ylim([-1 1])
xlabel('\alpha_{ id�al}(mV.K^{ -1})')
ylabel('\alpha (mV.K^{ -1})')
grid on
