% Figure 2 Article - Diagramme Diamants DT = 5K 
clear
close all

load('44-12-15-50-pp-old/44-12-15-50-pp-old.mat')

load('44-12-15-50-pp-old/discret/IMA_T100dT5.mat')

[V,VG]=ndgrid(V2,V2g);

pcolor(V*1e3,VG,IMA*1e12)
shading interp
colorbar
caxis([-6 6])

load('44-12-15-50-pp-old/discret/ZT_T100.mat')
hold on
plot(Voc(:,2)*1e3,V2g,'-k','LineWidth',1);
plot([V2(1)*1e3 V2(end)*1e3],[2.67 2.67],'--k','LineWidth',1);
