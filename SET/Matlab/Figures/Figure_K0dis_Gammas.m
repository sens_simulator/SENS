%Figure6
clear 
close all
dispo='44-12-15-50-pp';
qe=1.602176487e-19;

load([dispo,'/discret/ZT_T100.mat'])
K0dis=K0*qe;Zdis=ZT;VocDis=Voc;
load('44-12-15-50-pp/discret/Gammaoc_T100.mat')



FigureFormat(7,7,16,2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.15 0.7 0.7 0.25]);

hold on
plot(V2g,-K0dis(:,2)*5*1e18,'-k')
ylim([0 10])
set(gca,'XTickLabel',{})
set(gca,'box','on')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.15 0.45 0.7 0.25]);
load('44-12-15-50-pp/discret/IQGoc_T100dT5.mat')

plot(V2g,(IGd_oc(:,1)-IdG_oc(:,1))*1e18,'--b')
hold on
plot(V2g,(IGd_oc(:,2)-IdG_oc(:,2))*1e18,'-k')

legend(' ',' ','Location','NorthWest')
legend boxoff
set(gca,'XTickLabel',{})
ylim([-40 40])
set(gca,'Ytick',[-30 0 30]);
set(gca,'box','on')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.15 0.05 0.7 0.4]);
hold on

semilogy(V2g,GGdoc(:,1),'-b');hold on
H(1)=semilogy(V2g(1:60:end),GGdoc(1:60:end,1),'db')

semilogy(V2g,GGdoc(:,2),'--b')
semilogy(V2g(1:60:end),GGdoc(1:60:end,2),'db')

semilogy(V2g,GdGoc(:,2),'-r')
H(2)=semilogy(V2g(1:60:end),GdGoc(1:60:end,2),'+r')

semilogy(V2g,GdGoc(:,3),'--r')
semilogy(V2g(1:60:end),GdGoc(1:60:end,3),'+r')

semilogy(V2g,GDdoc(:,1),'-k')
H(3)=semilogy(V2g(30:60:end),GDdoc(30:60:end,1),'sk')

semilogy(V2g,GDdoc(:,2),'--k')
semilogy(V2g(30:60:end),GDdoc(30:60:end,2),'sk')

semilogy(V2g,GdDoc(:,2),'-','Color',[0 0.5 0])
H(4)=semilogy(V2g(15:60:end),GdDoc(15:60:end,2),'v','Color',[0 0.5 0])

semilogy(V2g,GdDoc(:,3),'--','Color',[0 0.5 0])
semilogy(V2g(15:60:end),GdDoc(15:60:end,3),'v','Color',[0 0.5 0])

set(gca,'YScale','log')
ylim([1e-8 30000])
set(gca,'box','on')
legend(' ',' ',' ',' ',' ',' ',' ',' ','Location','EastOutside')
legend boxoff

saveas2('figure6.pdf')