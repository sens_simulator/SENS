clear;close all;clc
tic

%--------------------------------------------------------------------------
% À MODIFIER
dispos='44-12-15-50-pp'; % diamètre-épaisseur oxyde source-oxyde drain-oxyde grille
dT=[0]; %K différence de température
T=100; %K température moyenne
alpha=[5e-2]; %elargissement des niveaux en kB*T
% Dossier où sont stockés les fichiers de données Matlab
dossier_destination='/Users/gianina.frimu/Documents/data_SENS/SET/Matlab/';
%--------------------------------------------------------------------------
%Constantes
eps0=8.8e-12; %eV
qe=1.6e-19; %c
hb=1.05459e-34/qe; %eV.s
kb=8.617e-5; %eV
me=9.1e-31; %kg

% Constantes physiques du matériau (en eV) modifiables
mox=0.5*me;
epsox=3.8;
epsdot=12;
Phimetal=4.1;Phidot=4;Phiox=0.9; %eV
Efmetal=11.6; %eV
Velec=1; %volume des electrodes

Vbarmetalox=Phimetal-Phiox;Vbardotox=Phidot-Phiox;

% Constantes d'échange (UA => SI)
abohr=0.529177e-10;
Ebohr=27.211; %eV
Psibohr=(0.529177e-10)^(-3/2);

%--------------------------------------------------------------------------
load([dossier_destination,dispos,'/',dispos,'.mat'])
%--------------------------------------------------------------------------
Nmin=0;
Nmax=Nemax;
%--------------------------------------------------------------------------
tmax=10;

%Chargement des données necessaires au calcul des GAMMAs
load([dossier_destination,dispos,'/Energies.mat']);E1=E;
clear E ECD ECG Vbarr V0DSee V0GSee V0DMAX V0GMAX
load([dossier_destination,dispos,'/Axe.mat']);

%potentiels
EFG = Phidot-Phimetal;
ECG = EFG-Efmetal;
Vbarr = Phidot-Phiox;

% Mise en forme des données et extraction des tailles de tableaux
xp=axex; yp=axey; zp=axez;
xp2=xp(1):abs(xp(2)-xp(1))/2:xp(end);xp2=xp2';
[X,Y,Z]=meshgrid(xp,yp,zp);[X2,Y2,Z2]=meshgrid(xp2,yp,zp);
[Xn,Yn,Zn]=ndgrid(xp,yp,zp);[Xn2,Yn2,Zn2]=ndgrid(xp2,yp,zp);


%Extraction des Psi à mi-barrière
disp('Extraction Psi mi-barrière')
if (exist([dispos,'/PSI_MIBAR.mat'],'file')==0),
    intpsiG=zeros(length(V),length(Vg),Nemax);
    intpsiD=zeros(length(V),length(Vg),Nemax);
    intdpsixG=zeros(length(V),length(Vg),Nemax);
    intdpsixD=zeros(length(V),length(Vg),Nemax);
    disp('Chargement de Psi.mat');
    load([dossier_destination,dispos,'/Psi.mat']);
    disp('Chargement termine');
    for iv = 1:length(V),
        for ivg = 1:length(Vg),
            % TROUVER LES V2 CORRESPONDANTS AU V (IDEM VG)
            % INTERPOLATION SE FAIT APRES
            iv0=1+(iv-1)*10;
            ivg0=1+(ivg-1)*10;
            %disp(['iv0=',num2str(iv0),' ivg0=',num2str(ivg0)])

            for ne=1:Nemax,
                psi=(permute(abs(Psi{iv,ivg,ne}),[2,1,3]));
                psiG(:,:)=interp3(xp,yp,zp,psi,-(a+hG/2)*1e-10,yp,zp,'cubic');
                psiD(:,:)=interp3(xp,yp,zp,psi,(a+hD/2)*1e-10,yp,zp,'cubic');

                [~,dpsidx,~]=gradient(abs(Psi{iv,ivg,ne}),yp,xp,zp);
                dpsidx=permute(dpsidx,[2 1 3]);
                dpsixG(:,:)=interp3(X,Y,Z,dpsidx, -(a+hG/2)*1e-10,yp,zp,'cubic');
                dpsixD(:,:)=interp3(X,Y,Z,dpsidx,(a+hD/2)*1e-10,yp,zp,'cubic');

                intpsiG(iv,ivg,ne)=trapz(zp,trapz(yp,psiG,1));
                intpsiD(iv,ivg,ne)=trapz(zp,trapz(yp,psiD,1));
                intdpsixG(iv,ivg,ne)=trapz(zp,trapz(yp,dpsixG,1));
                intdpsixD(iv,ivg,ne)=trapz(zp,trapz(yp,dpsixD,1));
            end
        end
    end

    clear dpsix dpsidxG dpsixD psiG psiD psi

    % Interpolation des intégrales
    disp('Interpolation des int�grales');
    %utile pour l'interpolation des énergies, ndgrid à la place de meshgrid.
    [Vi1,Vi1g]=ndgrid(V,Vg);
    [Vi2,Vi2g]=ndgrid(V2,V2g);

    PSIG=zeros(length(V2),length(V2g),Nemax);
    PSID=zeros(length(V2),length(V2g),Nemax);
    DPSIG=zeros(length(V2),length(V2g),Nemax);
    DPSID=zeros(length(V2),length(V2g),Nemax);
    for in=1:Nemax,
        PSIG(:,:,in)=interpn(Vi1,Vi1g,intpsiG(:,:,in),Vi2,Vi2g,'cubic');
        PSID(:,:,in)=interpn(Vi1,Vi1g,intpsiD(:,:,in),Vi2,Vi2g,'cubic');
        DPSIG(:,:,in)=interpn(Vi1,Vi1g,intdpsixG(:,:,in),Vi2,Vi2g,'cubic');
        DPSID(:,:,in)=interpn(Vi1,Vi1g,intdpsixD(:,:,in),Vi2,Vi2g,'cubic');
    end

    save([dossier_destination,dispos,'/PSI_MIBAR.mat'],'PSIG','PSID','DPSIG','DPSID');
else
    load([dossier_destination,dispos,'/PSI_MIBAR.mat'])
    disp('Psi_mibar exists');
end


for ia=1:length(alpha),
    %tic
    disp(['mih=',num2str(alpha(ia)),'kb.T']);

    %Fonctions spectrales

    mih=alpha(ia)*kb*T;%largeur à mi-hauteur de Lorentzienne. Lorentzienne mise � une int�grale de 2pi.
    %E=-50*mih:mih/10:50*mih;

    x2=mih*tan(0.995*pi/2)/2; %point pour lequel intégrale Lorentzienne = 0.99*2*pi
    %x2=0.05;
    disp(['x2=',num2str(x2)]);
    E=linspace(-x2,x2,1001); % Energie
    disp(['dE=',num2str(E(2)-E(1))]);
    A=mih./((mih/2)^2+E.^2)/2/pi; %Lorentzienne d'intégrale 2*PI
    disp(['int�grale =',num2str(trapz(E,A))]);

    A=A./trapz(E,A);
    disp(['length E=',num2str(length(E))]);


    %----------------------------------------------------------------------------
    % Calcul des fréquences de transferts puis du courant
    %----------------------------------------------------------------------------
    for it=1:length(dT),
        T1=T+dT(it)/2;
        T2=T-dT(it)/2;
        disp('Calcul des fonctions de transferts');
	disp(['dT=',num2str(dT(it))]);

        IMA=zeros(length(V2),length(V2g));
        IQFGd=zeros(length(V2),length(V2g));
        IQFdG=zeros(length(V2),length(V2g));
        IQFDd=zeros(length(V2),length(V2g));
        IQFdD=zeros(length(V2),length(V2g));

        Puiss=zeros(length(V2),length(V2g));

        for iv = 1:length(V2),

            EFD=Phidot-Phimetal-V2(iv);
            ECD=EFD-Efmetal;


            tic
            for ivg=1:length(V2g),
                %disp(['V=',num2str(V2(iv)),' Vg=',num2str(V2g(ivg))]);
                dGdG=zeros(Nemax+1,length(E));
                dGdD=zeros(Nemax+1,length(E));
                dGGd=zeros(Nemax+1,length(E));
                dGDd=zeros(Nemax+1,length(E));

                for ie=1:Nemax,
                    Ee=E+E2(iv,ivg,ie);
                    VOG=V0G0(iv,ivg,ie);
                    VOD=V0D0(iv,ivg,ie);
                    kG=sqrt(2*me/hb^2*(Ee-ECG))/sqrt(qe);
                    alG=sqrt(2*mox/(hb^2)*(VOG+Vbarr-Ee))/sqrt(qe);
                    rhoG=2*Velec/(2*pi)^2*(2*me/hb^2)^(3/2).*sqrt(Ee-ECG); % En eV^-1.(qe)^3/2.m^-3 (seulement si on considere que V=1m est une approximation costaude...)
                    intG=abs(alG.*PSIG(iv,ivg,ie)+DPSIG(iv,ivg,ie)).^2;


                    kD=sqrt(2*me/hb^2*(Ee-ECD))/sqrt(qe);
                    alD=sqrt(2*mox/hb^2*(VOD+Vbarr-Ee))/sqrt(qe);
                    rhoD=2*Velec/(2*pi)^2*(2*me/hb^2)^(3/2).*sqrt(Ee-ECD);
                    intD=abs(alD.*PSID(iv,ivg,ie)-DPSID(iv,ivg,ie)).^2;

                    corr1G=abs(1-(VOG./(VOG+Vbarr-Ee)));
                    corr2G=2*(VOG+Vbarr-Ee)./(3.*VOG)...
                        .*(1-(1-(VOG./(VOG+Vbarr-Ee))).^(3/2));
                    corr1D=abs(1-((VOD+V2(iv))./(VOD+Vbarr-Ee)));
                    corr2D=2*(VOD+Vbarr-Ee)./(3.*(VOD+V2(iv)))...
                        .*(1-(1-((VOD+V2(iv))./(VOD+Vbarr-Ee))).^(3/2));
                    corr2D=(corr2D==0)+(isnan(corr2D))+corr2D;
                    corr2G=(corr2G==0)+(isnan(corr2G))+corr2G;
                    MG=(hb^2/(2*mox))^2*2/Velec*kG.^2./(kG.^2+alG.^2*(me/mox)^2.*corr1G)...
                        .*exp(-alG.*hG*1e-10.*corr2G).*intG;
                    MD=(hb^2/(2*mox))^2*2/Velec*kD.^2./(kD.^2+alD.^2*(me/mox)^2.*corr1D)...
                        .*exp(-alD.*hD*1e-10.*corr2D).*intD;
                    fermiG=1./(1+exp((Ee-EFG)./(kb*T1)));
                    fermiD=1./(1+exp((Ee-EFD)./(kb*T2)));

                    dGdG(ie+1,:)=ie*MG.*rhoG.*A.*(1-fermiG);
                    dGdD(ie+1,:)=ie*MD.*rhoD.*A.*(1-fermiD);
                    dGGd(ie,:)=(13-ie)*MG.*rhoG.*A.*fermiG;
                    dGDd(ie,:)=(13-ie)*MD.*rhoD.*A.*fermiD;
                end
                %save([dispos,'/A',num2str(alpha(ia)),'/dGAMMA_T',num2str(T),'dT',num2str(dT(it))...
                %    ,'_V',num2str(V2(iv)),'_Vg',num2str(V2g(ivg)),'.mat'],...
                %     'dGdG','dGdD','dGGd','dGDd');

                dIQFGd=zeros(1,length(E));
                dIQFDd=zeros(1,length(E));
                dIQFdG=zeros(1,length(E));
                dIQFdD=zeros(1,length(E));
                dIMA=zeros(1,length(E));

                Gtot=dGGd+dGDd+dGdG+dGdD;
                GAMMA=zeros(Nemax+1,Nemax+1,length(E));
                for ii=1:Nemax+1,
                    GAMMA(ii,ii,:)=-Gtot(ii,:);
                end
                for ii=1:Nmax,
                    GAMMA(ii,ii+1,:)=dGdD(ii+1,:)+dGdG(ii+1,:);
                    GAMMA(ii+1,ii,:)=dGDd(ii,:)+dGGd(ii,:);
                end

                %Equation maîtresse
                P0=zeros(Nemax+1,1);P0(1,:)=1;
                for ie=1:length(E)
                    GAMMAt=GAMMA(:,:,ie).*tmax;
                    PI=expm(GAMMAt)*P0;
                    % Calcul de courant electrique avec l'équation maitresse
                    dIMA(ie)=sum(PI.*(dGGd(:,ie)-dGdG(:,ie)));
                    % Calcul des courants de chaleur
                    for ne=1:Nemax,
                        dIQFGd(ie)=dIQFGd(ie)+PI(ne)*(E2(iv,ivg,ne)-EFG+E(ie))*dGGd(ne,ie);
                        dIQFDd(ie)=dIQFDd(ie)+PI(ne)*(E2(iv,ivg,ne)-EFD+E(ie))*dGDd(ne,ie);
                        dIQFdG(ie)=dIQFdG(ie)+PI(ne+1)*(E2(iv,ivg,ne)-EFG+E(ie))*dGdG(ne+1,ie);
                        dIQFdD(ie)=dIQFdD(ie)+PI(ne+1)*(E2(iv,ivg,ne)-EFD+E(ie))*dGdD(ne+1,ie);
                    end
                end
                IMA(iv,ivg)=qe*sqrt(qe)*2*pi/hb*trapz(E,dIMA); %A
                IQFGd(iv,ivg)=sqrt(qe)*2*pi/hb*trapz(E,dIQFGd); %W/qe
                IQFDd(iv,ivg)=sqrt(qe)*2*pi/hb*trapz(E,dIQFDd);
                IQFdG(iv,ivg)=sqrt(qe)*2*pi/hb*trapz(E,dIQFdG);
                IQFdD(iv,ivg)=sqrt(qe)*2*pi/hb*trapz(E,dIQFdD);
                
                Puiss(iv,ivg)=IMA(iv,ivg)*V2(iv);
                
                
            end
            toc
        end

    disp('SAUVEGARDE IMA');
    mkdir([dossier_destination,dispos,'/A_',num2str(alpha(ia))]);
    save([dossier_destination,dispos,'/A_',num2str(alpha(ia)),'/IMA_T',num2str(T),'dT',num2str(dT(it)),'.mat'],...
        'IMA','V2','V2g','A','E','Puiss','IQFGd','IQFDd','IQFdD','IQFdG');
    end

end
disp('end')
