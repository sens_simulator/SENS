void create_dens(reel *ndens, reel *psi_tot);
void solve_Poi(reel *ndens, reel *Vint,reel tol);
void calc_VexchLDA(reel *Vxc,reel *ndens);
void create_polarx(reel *Vpol2,reel *Vpol,reel Vxgauche,reel Vxdroite,reel Vg,reel tol);

/*---------------------------------------------------------------------------*/
void create_dens(reel *ndens, reel *psi_tot)
{
 int i,j;

 for(i=0;i<Nptmax;i++) ndens[i]=0.0;
 for(j=0;j<Nborbit;j++) 
 {
  for(i=0;i<Nptmax;i++) ndens[i]=ndens[i]+Orbitale[j]*psi_tot[j*Nptmax+i]*psi_tot[j*Nptmax+i];
 }
}
/*---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

void calc_VexchLDA(reel *Vxc,reel *ndens) // Uniquement pour la DFT !!!
{
 int i;
 reel Const1,rs,Vc,Vx,be,tier;
 tier=1/3.0;
 for(i=0;i<Nptmax;i++)
 {

  rs=pow(3.0/(4.0*Pi*ndens[i]),tier);
  be=1.0+0.0368*rs*log(1.0+21.0/rs);
  Vxc[i]=-2.0*be*pow(3.0/(8.0*Pi)*ndens[i],tier)/epsilon[i];
 }
}

/*---------------------------------------------------------------------------*/
void solve_Poi(reel *ndens, reel *Vint,reel tol)
{
 #define NDENS(i,j,k) (ndens[(k)*Nptaxe2+(j)*Nptaxe+(i)])
 #define VINT(i,j,k) (Vint[(k)*Nptaxe2+(j)*Nptaxe+(i)])
 #define EPSI(i,j,k) (epsilon[(k)*Nptaxe2+(j)*Nptaxe+(i)])
 #define UTEM(i,j,k) (Utemp[(k)*Nptaxe2+(j)*Nptaxe+(i)])


 int i,j,k,iter;
 reel dx2;
 int imax,jmax,kmax;
 reel vip,vim,vjp,vjm,vkp,vkm;
 reel eip,eim,ejp,ejm,ekp,ekm,ep;
 reel temp,errcrit,errtemp,deltaV;
 reel *Utemp,lim,r,erreur,erreurp;

 dx2=dx*dx;
 Utemp=malloc(Nptmax*sizeof(reel));
 if (Utemp==NULL) printf("probl�me allocation\n");
 for (i=0;i<Nptmax;i++) Vint[i]=0;
 for (i=0;i<Nptmax;i++) Utemp[i]=0;

 for(iter=0;iter<Nbitermaxpoi;iter++)
 {
  erreurp=0;

  /*premi�re passe*/
  for(k=0;k<Nptaxe;k++)
  {
   for(j=0;j<Nptaxe;j++)
   {
    for(i=0;i<Nptaxe;i++)
    {
     ep=EPSI(i,j,k);

     if (i==Nptaxe-1) {r=sqrt((x[i]+dx)*(x[i]+dx)+y[j]*y[j]+z[k]*z[k]); lim=(Nemax-1)/(ep*r);vip=lim; eip=EPSI(Nptaxe-1,j,k); }
     else {vip=VINT(i+1,j,k); eip=EPSI(i+1,j,k);}
     if (i==0)        {r=sqrt((x[i]-dx)*(x[i]-dx)+y[j]*y[j]+z[k]*z[k]); lim=(Nemax-1)/(ep*r);vim=lim; eim=EPSI(0,j,k); }
     else {vim=VINT(i-1,j,k); eim=EPSI(i-1,j,k);}
     if (j==Nptaxe-1) {r=sqrt(x[i]*x[i]+(y[j]+dx)*(y[j]+dx)+z[k]*z[k]); lim=(Nemax-1)/(ep*r);vjp=lim; ejp=EPSI(i,Nptaxe-1,k); }
     else {vjp=VINT(i,j+1,k); ejp=EPSI(i,j+1,k);}
     if (j==0)        {r=sqrt(x[i]*x[i]+(y[j]-dx)*(y[j]-dx)+z[k]*z[k]); lim=(Nemax-1)/(ep*r);vjm=lim; ejm=EPSI(i,0,k); }
     else {vjm=VINT(i,j-1,k); ejm=EPSI(i,j-1,k);}
     if (k==Nptaxe-1) { r=sqrt(x[i]*x[i]+y[j]*y[j]+(z[k]+dx)*(z[k]+dx)); lim=(Nemax-1)/(ep*r);vkp=lim; ekp=EPSI(i,j,Nptaxe-1);}
     else {vkp=VINT(i,j,k+1); ekp=EPSI(i,j,k+1);}
     if (k==0)        { r=sqrt(x[i]*x[i]+y[j]*y[j]+(z[k]-dx)*(z[k]-dx)); lim=(Nemax-1)/(ep*r);vkm=lim; ekm=EPSI(i,j,0);}
     else {vkm=VINT(i,j,k-1); ekm=EPSI(i,j,k-1);}

     UTEM(i,j,k)=(vip+vim+vjp+vjm+vkp+vkm)/6.0
     +1.0/(24.0*ep)*((eip-eim)*(vip-vim)+(ejp-ejm)*(vjp-vjm)+(ekp-ekm)*(vkp-vkm))
     +NDENS(i,j,k)*4.0*Pi*dx2/(6.0*ep);
     //deltaV=deltaV+fabs(UTEM(i,j,k)-VINT(i,j,k));
     erreur=fabs(UTEM(i,j,k)-VINT(i,j,k));
     if (erreur>erreurp) {deltaV=erreur/fabs(UTEM(i,j,k)); erreurp=erreur;}

     VINT(i,j,k)=UTEM(i,j,k);

    }
   }
  }
  /*deuxieme passe*/
  /*
  for(k=Nptaxe-1;k>=0;k--)
  {
  for(j=Nptaxe-1;j>=0;j--)
  {
  for(i=Nptaxe-1;i>=0;i--)
  {
  ep=EPSI(i,j,k);

  if (i==Nptaxe-1) {r=sqrt((x[i]+dx)*(x[i]+dx)+y[j]*y[j]+z[k]*z[k]); lim=(Nemax-1)/(ep*r);vip=lim;eip=EPSI(Nptaxe-1,j,k);}
  else {vip=UTEM(i+1,j,k); eip=EPSI(i+1,j,k);}
  if (i==0)        {r=sqrt((x[i]-dx)*(x[i]-dx)+y[j]*y[j]+z[k]*z[k]); lim=(Nemax-1)/(ep*r);vim=lim; eim=EPSI(0,j,k);}
  else {vim=UTEM(i-1,j,k); eim=EPSI(i-1,j,k);}
  if (j==Nptaxe-1) {r=sqrt(x[i]*x[i]+(y[j]+dx)*(y[j]+dx)+z[k]*z[k]); lim=(Nemax-1)/(ep*r);vjp=lim; ejp=EPSI(i,Nptaxe-1,k);}
  else {vjp=UTEM(i,j+1,k); ejp=EPSI(i,j+1,k);}
  if (j==0)        {r=sqrt(x[i]*x[i]+(y[j]-dx)*(y[j]-dx)+z[k]*z[k]); lim=(Nemax-1)/(ep*r);vjm=lim; ejm=EPSI(i,0,k);}
  else {vjm=UTEM(i,j-1,k); ejm=EPSI(i,j-1,k);}
  if (k==Nptaxe-1) {r=sqrt(x[i]*x[i]+y[j]*y[j]+(z[k]+dx)*(z[k]+dx)); lim=(Nemax-1)/(ep*r);vkp=lim; ekp=EPSI(i,j,Nptaxe-1);}
  else {vkp=UTEM(i,j,k+1); ekp=EPSI(i,j,k+1);}
  if (k==0)        {r=sqrt(x[i]*x[i]+y[j]*y[j]+(z[k]-dx)*(z[k]-dx)); lim=(Nemax-1)/(ep*r);vkm=lim; ekm=EPSI(i,j,0);}
  else {vkm=UTEM(i,j,k-1); ekm=EPSI(i,j,k-1);}

  VINT(i,j,k)=(vip+vim+vjp+vjm+vkp+vkm)/6.0
  +1.0/(24.0*ep)*((eip-eim)*(vip-vim)+(ejp-ejm)*(vjp-vjm)+(ekp-ekm)*(vkp-vkm))
  +4.0*Pi*dx2/(6.0*ep)*NDENS(i,j,k);

  deltaV=deltaV+fabs(UTEM(i,j,k)-VINT(i,j,k));
  }
  }
  }

  */
  if (deltaV<tol) {break;}
 }
 //printf("\b\t\t iter=%d                ",iter);   
 if (deltaV>tol) {printf("Attention Poisson n'a pas encore converge\n");
 fprintf(fichlog,"Attention Poisson n'a pas encore converge\n");}
 free(Utemp);
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void create_polarx(reel *Vpol2,reel *Vpol,reel Vxgauche,reel Vxdroite,reel Vg,reel tol)
{ 
 #define VPOL(i,j,k) (Vpol[(k)*Nptaxe2+(j)*Nptaxe+(i)])
 #define VPOL2(i,j,k) (Vpol2[(k)*Nptxy2+(j)*Nptaxe+(i)])
 #define EPSIP2(i,j,k) (epsilon2[(k)*Nptxy2+(j)*Nptaxe+(i)])
 #define UTEMPOL(i,j,k) (Utemp[(k)*Nptxy2+(j)*Nptaxe+(i)])

 int i,j,k,iter;
 int imax,jmax,kmax;
 reel vip,vim,vjp,vjm,vkp,vkm;
 reel eip,eim,ejp,ejm,ekp,ekm,ep;
 reel errcrit,errtemp,deltaV;
 reel *Utemp,lim,r,erreur,erreurp;
 char fichpoi[30];
 
 
 int ixmin,ixmax,ia,ima,iymin,iymax,jymin,jymax,jyg;
 reel xmin,xmax,ymin,ymax,yg;
 reel esi=11.7;
 reel esio2=3.8;
 
 //limites des oxydes 
 xmin=-a-hmin;
 xmax=a+hmax;
 ymax=a;
 ymin=-a;
 yg=a+hg;
 //C'est ici que se met en place la g�om�trie du dispo.

 for(i=1;i<Nptaxe-1;i++)
 { 
  if (((x[i-1]+x[i])/2.0<=xmin)&&((x[i]+x[i+1])/2.0>xmin)) ixmin=i; // � partir de quel point nous ne sommes plus dans l'oxyde
  if (((x[i-1]+x[i])/2.0<xmax)&&((x[i]+x[i+1])/2.0>=xmax)) ixmax=i;
 }
 

 
 for(j=1;j<Npty2-1;j++)
 {
  if (((y2[j-1]+y2[j])/2.0<ymax)&&((y2[j]+y2[j+1])/2.0>=ymax)) jymax=j;
  if (((y2[j-1]+y2[j])/2.0<=ymin)&&((y2[j]+y2[j+1])/2.0>ymin)) jymin=j;
 }  

 for(j=1;j<Npty2-1;j++)
 { 
  if (((y2[j-1]+y2[j])/2.0<yg)&&((y2[j]+y2[j+1])/2.0>=yg)) jyg=j;
 }
 
 Utemp=malloc(Nptmax2*sizeof(reel));
 if (Utemp==NULL) printf("probl�me allocation\n");



 //cr�ation de Vpol avec conditions limites donn�es par les �l�ctrodes
 for(j=0;j<Npty2;j++)
 {
  for(i=0;i<Nptaxe;i++)
  {
   if (i<=ixmin && j<=jymax && j>=jymin) {for(k=0;k<Nptaxe;k++) VPOL2(i,j,k)=Vxgauche;}
   else if (i>=ixmax && j<=jymax && j>=jymin) {for(k=0;k<Nptaxe;k++) VPOL2(i,j,k)=Vxdroite;}
   else if (j>=jyg) {for(k=0;k<Nptaxe;k++) VPOL2(i,j,k)=Vg;}
   else {for(k=0;k<Nptaxe;k++) VPOL2(i,j,k)=0;}
  }
 }

 for (i=0;i<Nptmax2;i++) Utemp[i]=Vpol2[i];


 for(iter=0;iter<Nbitermaxpoi;iter++)
 {
  erreurp=0.0;

  /*premi�re passe*/
  for(k=0;k<Nptaxe;k++)
  {
   for(j=0;j<Npty2;j++)
   {
    for(i=0;i<Nptaxe;i++)
    {
    ep=EPSIP2(i,j,k);

    //V(i+1) 
    if(i==Nptaxe-1) {vip=Vxdroite; eip=EPSIP2(Nptaxe-1,j,k);}
    //else if (i>=ixmax-1 & j<=jymax & j>=jymin) {vip=Vxdroite; eip=EPSIP(Nptx-1,j,k);}    // limite sur x � droite impos� par potentiel Vd (electrode de droite/drain)
    else {vip=VPOL2(i+1,j,k); eip=EPSIP2(i+1,j,k);}
	
    //V(i-1)
    if(i==0) {vim=Vxgauche; eim=EPSIP2(0,j,k);}
    //else if (i<=ixmin+1 & j<jymax & j>jymin) {vim=Vxgauche; eim=EPSIP(0,j,k);}       // limite sur x impos� par potentiel Vg (electrode de gauche/source) 
    else {vim=VPOL2(i-1,j,k); eim=EPSIP2(i-1,j,k);}

    //V(j+1)
    if(j==Npty2-1) {vjp=Vg; ejp=EPSIP2(i,Nptaxe2-1,k);}
    //else if (j>=jyg-1) {vjp=Vg; ejp=EPSIP(i,Npty-1,k); }        // limite sur y  impos� par potentiel de grille
    else {vjp=VPOL2(i,j+1,k); ejp=EPSIP2(i,j+1,k);}

    //V(j-1)
    if (j==0)        {vjm=VPOL2(i,0,k); ejm=EPSIP2(i,0,k);}                // limite sur y Eperpen=0
    else {vjm=VPOL2(i,j-1,k); ejm=EPSIP2(i,j-1,k);}

    //V(k+1)  
    if (k==Nptaxe-1) {vkp=VPOL2(i,j,Nptaxe-1); ekp=EPSIP2(i,j,Nptaxe-1);}         // limite sur z Eperpen=0
    else {vkp=VPOL2(i,j,k+1); ekp=EPSIP2(i,j,k+1);}

    //V(k-1)
    if (k==0)        {vkm=VPOL2(i,j,0); ekm=EPSIP2(i,j,0);}                // limite sur z Eperpen=0
    else {vkm=VPOL2(i,j,k-1); ekm=EPSIP2(i,j,k-1);}

    //Calcul de Vpol(it+1) � partir de Vpol (it)
    UTEMPOL(i,j,k)=(vip+vim+vjp+vjm+vkp+vkm)/6.0
    +1.0/(24.0*ep)*((eip-eim)*(vip-vim)+(ejp-ejm)*(vjp-vjm)+(ekp-ekm)*(vkp-vkm));
   
    //Conditions aux limites
    if (i<=ixmin && j<=jymax && j>=jymin) {UTEMPOL(i,j,k)=Vxgauche;}
    else if (i>=ixmax && j<=jymax && j>=jymin) {UTEMPOL(i,j,k)=Vxdroite;}
    else if (j>=jyg) {UTEMPOL(i,j,k)=Vg;}
   
    //Calcul de l'erreur
    //deltaV=deltaV+fabs(UTEM(i,j,k)-VPOL(i,j,k));
    erreur=fabs(UTEMPOL(i,j,k)-VPOL2(i,j,k));
    if (erreur>erreurp)  {deltaV=erreur/fabs(UTEMPOL(i,j,k));  erreurp=erreur;}

    VPOL2(i,j,k)=UTEMPOL(i,j,k);
    }
   }
  }
  if (deltaV<tol) {printf("nombre d'iteration: %d",iter); break;}
 }
 if (deltaV>tol) {printf("Attention Poisson n'a pas encore converge\n");
 fprintf(fichlog,"Attention Poisson n'a pas encore converge\n");}
 free(Utemp);
 
 
 
 //changement variable
for(i=0;i<Nptaxe;i++)
{
for(j=0;j<Nptaxe;j++)
{
for(k=0;k<Nptaxe;k++)
{
VPOL(i,j,k)=VPOL2(i,j,k);
}
}
}


/*sprintf(fichpoi,"fichpoi.dat");
 fichdata2=fopen(fichpoi,"w");
 fprintf(fichdata2,"%f\n%f\n%f\n%f\n%f\n%d\n%d\n%d\n%d\n%d",xmin,xmax,ymin,ymax,yg,ixmin,ixmax,jymin,jymax,jyg);
 fclose(fichdata2); */


}

