/*-----------------------------------------------------------------------*/
#define Matfunc(y,x,n) (Hamilton(y,x,n))
void myeigs(int n,int k,reel *eigvec,reel *eigval);
/*-----------------------------------------------------------------------*/
long idum=1294857645;
int basis(void);
static int groesser1(reel x);
reel mach_eps(void);
int comdiv(reel ar,reel ai,reel br,reel bi,reel *cr,reel *ci);
reel comabs ( reel  ar,reel  ai );
static int balance(int n,reel *mat,reel scal[], int *low,int *high,int basis);
static int balback(int n,int low,int high,reel scal[],reel *eivec);
static int elmhes(int n,int low,int high,reel *mat,int perm[]);
static int elmtrans(int n,int low,int high,reel *mat,int perm[],reel *h);
static int orthes(int  n,int  low,int  high,reel *mat,reel d[]);
static int orttrans(int  n,int  low,int  high,reel *mat,reel d[],reel *v );
static int hqrvec(int n,int low,int high,reel *h,reel wr[],reel wi[],reel *eivec);
static int hqr2(int vec,int n,int low,int high,reel *h,reel wr[],reel wi[],reel *eivec,int cnt[]);
static int norm_1(int n,reel *v,reel wi[]);
static int norm_2(int n,reel *v,reel wi[]);
int eigen (int vec,int ortho,int ev_norm,int n,reel *mat,reel *eivec,reel valre[],reel valim[],int cnt[]);
void arnoldi(int k1,int k2,int n,int k,int p,reel *v,reel *H);
reel ran0(long *idum);



#define MAXIT 50                     /*  Maximal number of           */
                                     /*  iterations per eigenvalue   */

#define SWAP(typ, a, b) { typ temp; temp = a; a = b; b = temp; }
#define ABS(X) (((X) >= 0) ? (X) : -(X))
#define SQRT(x)    (reel)sqrt((double)(x))
#define SQR(X) ((X) * (X))  

static int imaxarg1,imaxarg2;
#define IMAX(a,b) (imaxarg1=(a),imaxarg2=(b),(imaxarg1) > (imaxarg2) ?\
        (imaxarg1) : (imaxarg2))
static int iminarg1,iminarg2;
#define IMIN(a,b) (iminarg1=(a),iminarg2=(b),(iminarg1) < (iminarg2) ?\
        (iminarg1) : (iminarg2))
static double rmaxarg1,rmaxarg2;
/*#define RMAX(a,b) (rmaxarg1=(a),rmaxarg2=(b),(rmaxarg1) > (rmaxarg2) ?\
        (rmaxarg1) : (rmaxarg2))
static double rminarg1,rminarg2;*/
#define RMIN(a,b) (rminarg1=(a),rminarg2=(b),(rminarg1) < (rminarg2) ?\
        (rminarg1) : (rminarg2))

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))


/*Quelques types...*
/*---------------- */
#undef boolean
#undef FALSE
#undef TRUE
typedef enum {FALSE, TRUE} boolean;

#define MACH_EPS  mach_eps() 
#define BASIS     basis()  

#ifdef FLT_EPSILON               /* ANSI C compiler ? ................*/
#define MACH_EPS  (REAL)FLT_EPSILON
#else                            /* not an ANSI C compiler ? .........*/
#define MACH_EPS  mach_eps()     /* machine constant .................*/
#endif

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void myeigs(int n,int k,reel *eigvec,reel *eigval)
{
 int i,j,l,m,k1,k2,ii,ip2,jj;
 int p,ksave,maxit,iter,*count;
 reel tol,beta,alpha,norm,stopcrit,norm1,norm2,c,s,t,temp1,temp2;
 reel *v,*H,*H2;
 reel *v_point,*v_point2;
 reel *Vecppe,*wreal,*wimag,*vectmp,*qvec;
 char tto;
 p=IMIN(IMAX(2*k,20),n);
 p=p-k;

 maxit=300;

 v=malloc((k+p+1)*n*sizeof(reel));
 if(v==NULL) printf("probl�me allocation\n");
 H=malloc((k+p)*(k+p)*sizeof(reel));
 if(H==NULL) printf("probl�me allocation\n");
 H2=malloc((k+p)*(k+p)*sizeof(reel));
 if(H2==NULL) printf("probl�me allocation\n");
 Vecppe=malloc((k+p)*(k+p)*sizeof(reel));
 if(Vecppe==NULL) printf("probl�me allocation\n");
 wreal=malloc((k+p)*sizeof(reel));
 if(wreal==NULL) printf("probl�me allocation\n");
 wimag=malloc((k+p)*sizeof(reel));
 if(wimag==NULL) printf("probl�me allocation\n");
 count=malloc((k+p+2)*sizeof(int));
 if(count==NULL) printf("probl�me allocation\n");

 vectmp=malloc(n*sizeof(reel));
 if (vectmp==NULL) printf("probl�me allocation\n");
 qvec=malloc((k+p)*sizeof(reel));
 if (qvec==NULL) printf("probl�me allocation\n");
 
 for(i=0;i<(k+p)*(k+p);i++) H[i]=0;

 v_point=v;
 beta=0;
 for(i=0;i<=n-1;i++)
    {v_point[i]=(ran0(&idum)-0.5);
     beta=beta+v_point[i]*v_point[i];
    }
 for(i=0;i<=n-1;i++) v_point[i]=v_point[i]/sqrt(beta);

 v_point=v+1*n;
 Matfunc(v_point,v,n);

 v_point=v+0*n;
 v_point2=v+1*n;
 alpha=0;
 for(i=0;i<n;i++) alpha=alpha+v_point[i]*v_point2[i];
 for(i=0;i<n;i++) v_point2[i]=v_point2[i]-alpha*v_point[i];
 H[0]=alpha;

 /* Raffinage de l'orthonormalisation */
 /*-----------------------------------*/
 v_point=v+0*n;
 v_point2=v+1*n;
 alpha=0;
 for(i=0;i<n;i++) alpha=alpha+v_point[i]*v_point2[i];
 for(i=0;i<n;i++) v_point2[i]=v_point2[i]-alpha*v_point[i];
 H[0]=H[0]+alpha;

 arnoldi(1,k,n,k,p,v,H);
 tol=1e-5;
 iter=0;
 stopcrit=1;
 while(((stopcrit>tol)&&(iter<maxit))||(iter<2))
     {
      iter=iter+1;
      arnoldi(k,k+p,n,k,p,v,H);

      for(i=0;i<(k+p)*(k+p);i++) H2[i]=H[i];
      for(i=0;i<(k+p);i++) count[i]=0;
      for(i=0;i<(k+p)*(k+p);i++) Vecppe[i]=0;
      eigen(1,0,1,k+p,H2,Vecppe,wreal,wimag,count);

        for(i=0;i<k+p;i++) for(j=k+p-1;j>i;j--) if(wreal[j-1]<wreal[j])
          {
           SWAP(reel,wreal[j],wreal[j-1]);
           SWAP(reel,wimag[j],wimag[j-1]);
           for(m=0;m<k+p;m++) SWAP(reel,Vecppe[m*(k+p)+j],Vecppe[m*(k+p)+j-1]);
          }

      /*printf("\n\nEigen:\n");
      for(i=0;i<k+p;i++)  printf("%f\n",wreal[k+p-1-i]);
      fflush(stdin);getc(stdin);*/
      for(i=0;i<k;i++) eigval[i]=wreal[k+p-1-i];

      for(j=0;j<k;j++)
        {
         for(i=0;i<n;i++)
           {
            eigvec[j*n+i]=0;
            for(m=0;m<k+p;m++)
              {
               eigvec[j*n+i]=eigvec[j*n+i]+Vecppe[m*(k+p)+k+p-1-j]*v[m*n+i];
              }
           }
        }

      for(j=0;j<k;j++)
        {
         norm=0;
         for(i=0;i<n;i++) norm=norm+eigvec[j*n+i]*eigvec[j*n+i];
         norm=sqrt(norm);
         if (norm!=0) for(i=0;i<n;i++) eigvec[j*n+i]=eigvec[j*n+i]/norm;
        }

      stopcrit=0;
      norm=0;
      for(j=0;j<k;j++)
        {
         Matfunc(vectmp,eigvec+j*n,n);
         norm1=0; norm2=0;
         for(i=0;i<n;i++)
           {
            norm1=norm1+fabs(vectmp[i]);
            vectmp[i]=vectmp[i]-eigval[j]*eigvec[j*n+i];
            norm2=norm2+fabs(vectmp[i]);
           }
         if (stopcrit<norm2) stopcrit=norm2;
         if (norm<norm1) norm=norm1;
        }
        stopcrit=stopcrit/norm;
      /*printf("stopcrit=%e iter=%d\n",stopcrit,iter);*/
      /*apshift2*/
      for(i=0;i<k+p;i++) qvec[i]=0;  qvec[k+p-1]=1;

      m=0; count[m]=-1;
      for(i=0;i<k+p-2;i++) {if (H[(i+1)*(k+p)+i]==0) {m=m+1; count[m]=i;}}
      m=m+1; count[m]=k+p-2;

      for(j=0;j<p;j++)
        {
         if (fabs(wimag[j])==0)
          {
           for(i=0;i<m;i++)
             {
              k1=count[i]+1;
              k2=count[i+1];
              c=H[k1*(k+p)+k1]-wreal[j];
              s=H[(k1+1)*(k+p)+k1];
              t=sqrt(c*c+s*s);
              if (t==0.0) {c=1.0; s=0.0;} else {c=c/t; s=s/t;}

              for(l=k1;l<=k2;l++)
                {
                 if(l>k1)
                  {
                   temp1=H[l*(k+p)+l-1];
                   temp2=H[(l+1)*(k+p)+l-1];
                   t=sqrt(temp1*temp1+temp2*temp2);
                   if (t==0.0)
                    {
                     c=1.0;
                     s=0.0;
                    }
                   else
                    {
                     c=temp1/t;
                     s=temp2/t;
                     H[l*(k+p)+l-1]=t;
                     H[(l+1)*(k+p)+l-1]=0.0;
                    }
                   }
                   for(ii=l;ii<k+p;ii++)
                     {
                      temp1=H[l*(k+p)+ii];
                      temp2=H[(l+1)*(k+p)+ii];
                      H[l*(k+p)+ii]=c*temp1+s*temp2;
                      H[(l+1)*(k+p)+ii]=-s*temp1+c*temp2;
                     }

                   ip2=l+2; if(ip2>k+p-1) ip2=k+p-1;

                   for(ii=0;ii<=ip2;ii++)
                     {
                      temp1=H[ii*(k+p)+l];
                      temp2=H[ii*(k+p)+l+1];
                      H[ii*(k+p)+l]=c*temp1+s*temp2;
                      H[ii*(k+p)+l+1]=-s*temp1+c*temp2;
                     }
                    for(ii=0;ii<n;ii++)
                     {
                      temp1=v[l*n+ii];
                      temp2=v[(l+1)*n+ii];
                      v[l*n+ii]=c*temp1+s*temp2;
                      v[(l+1)*n+ii]=-s*temp1+c*temp2;
                     }

                    temp1=qvec[l];
                    temp2=qvec[l+1];
                    qvec[l]=c*temp1+s*temp2;
                    qvec[l+1]=-s*temp1+c*temp2;
                }
             }
          }
         else
          {
           printf("Erreur: complexe\n");
           return;
          }
        /*fin apshift2*/
   }

 for(ii=0;ii<n;ii++) v[(k+p)*n+ii]=v[(k+p)*n+ii]*qvec[k-1];
 for(ii=0;ii<n;ii++) v[k*n+ii]=v[(k+p)*n+ii]+v[k*n+ii]*H[k*(k+p)+k-1];

}
  free(H);free(H2);free(v);free(Vecppe);free(wreal);free(wimag);free(count);
  free(vectmp);free(qvec);
  /*printf("eigenvalues:\n");
  for (i=0;i<k;i++) printf("%15f\n",eigval[i]);
  printf("\n");
  printf("\n stopcrit=%e\n",stopcrit);*/
}
/*---------------------------------------------------------------------------*/
void arnoldi(int k1,int k2,int n,int k,int p,reel *v,reel *H)
{
 int l,m,i;
 reel beta,alpha,norm;
 reel *v_point,*v_point2;
 reel *alpha2;

 alpha2=malloc(sizeof(reel)*(k+p));
 if (alpha2==NULL) printf("Erreur alloc");

for(l=k1;l<k2;l++)
   {
    v_point=v+l*n;

    beta=0;
    for(i=0;i<=n-1;i++) beta=beta+v_point[i]*v_point[i];
    beta=sqrt(beta);
    H[l*(k+p)+l-1]=beta;

    norm=0;
    for(i=0;i<=l-1;i++) norm=norm+H[i*(k+p)+l-1]*H[i*(k+p)+l-1];
    norm=sqrt(norm);
    if(beta<=10.0*MACH_EPS*norm)
      {
       for(i=0;i<=n-1;i++) v_point[i]=ran0(&idum);

       for(m=0;m<=l-1;m++)
         {
          v_point=v+l*n;
          v_point2=v+m*n;
          alpha=0;
          for(i=0;i<n;i++) alpha=alpha+v_point[i]*v_point2[i];
          for(i=0;i<n;i++) v_point[i]=v_point[i]-alpha*v_point2[i];
         }

       for(m=0;m<=l-1;m++)
         {
          v_point=v+l*n;
          v_point2=v+m*n;
          alpha=0;
          for(i=0;i<n;i++) alpha=alpha+v_point[i]*v_point2[i];
          for(i=0;i<n;i++) v_point[i]=v_point[i]-alpha*v_point2[i];
         }
       beta=0;
       for(i=0;i<=n-1;i++) beta=beta+v_point[i]*v_point[i];
       beta=1/sqrt(beta);
       H[l*(k+p)+l-1]=0.0;
      }
      else
      {
       beta=1/beta;
      }
    for(i=0;i<=n-1;i++) v_point[i]=beta*v_point[i];

    v_point2=v+(l+1)*n;

    Matfunc(v_point2,v_point,n);

    for(m=0;m<=l;m++)
      {
       v_point=v+m*n;
       v_point2=v+(l+1)*n;
       alpha=0;
       for(i=0;i<=n-1;i++) alpha=alpha+v_point2[i]*v_point[i];
       H[m*(k+p)+l]=alpha;
      }

    for(m=0;m<=l;m++)
      {
       v_point=v+m*n;
       v_point2=v+(l+1)*n;
       for(i=0;i<=n-1;i++) v_point2[i]=v_point2[i]-v_point[i]*H[m*(k+p)+l];
      }
     /*raffinement*/

     for(m=0;m<=l;m++)
      {
       v_point=v+m*n;
       v_point2=v+(l+1)*n;
       alpha2[m]=0;
       for(i=0;i<=n-1;i++) alpha2[m]=alpha2[m]+v_point2[i]*v_point[i];
       H[m*(k+p)+l]=H[m*(k+p)+l]+alpha2[m];
      }

    for(m=0;m<=l;m++)
      {
       v_point=v+m*n;
       v_point2=v+(l+1)*n;
       for(i=0;i<=n-1;i++) v_point2[i]=v_point2[i]-v_point[i]*alpha2[m];
      }


   }
free(alpha2);
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/







/*--------------------------------------------------------------------*
 * Aux functions for  eigen                                           *
 *--------------------------------------------------------------------*/

/*--------------------------------------------------------------------*/

int basis(void)
{
  reel x,
       eins,
       b;

  x = eins = b = 1.0;

  while ((x + eins) - x == eins)
    x *= 2.0;
  while ((x + b) == x)
    b *= 2.0;


  return (int)((x + b) - x);
}
/*-----------------------------------------------------------------*/
/*--------------------------------------------------------------------*/

static int groesser1(reel x)
{
  return x > 1.0;
}



/*--------------------------------------------------------------------*/

reel mach_eps(void)
{
  static reel    epsilon;
  static boolean schon_berechnet = FALSE;

  if (! schon_berechnet)
  {
    for (epsilon = 1.0; groesser1(1.0 + epsilon); )
      epsilon *= 0.5;
    epsilon         *= 2.0;
    schon_berechnet  = TRUE;
  }
  return epsilon;
}
/*---------------------------------------------------------------------*/
int comdiv
           (
            reel   ar,            /* Real part of numerator ..........*/
            reel   ai,            /* Imaginary part of numerator .....*/
            reel   br,            /* Real part of denominator ........*/
            reel   bi,            /* Imaginary part of denominator ...*/
            reel * cr,            /* Real part of quotient ...........*/
            reel * ci             /* Imaginary part of quotient ......*/
           )
{
  reel tmp;

  if (br == 0.0 && bi == 0.0) return (1);

  if (ABS (br) > ABS (bi))
  {
    tmp  = bi / br;
    br   = tmp * bi + br;
    *cr  = (ar + tmp * ai) / br;
    *ci  = (ai - tmp * ar) / br;
  }
  else
  {
    tmp  = br / bi;
    bi   = tmp * br + bi;
    *cr  = (tmp * ar + ai) / bi;
    *ci  = (tmp * ai - ar) / bi;
 }

 return (0);
}
/*---------------------------------------------------------------------*/
reel comabs
             (
               reel  ar,          /* Real part .......................*/
               reel  ai           /* Imaginary part ..................*/
              )
{
  if (ar == 0.0 && ai == 0.0) return (0.0);

  ar = ABS (ar);
  ai = ABS (ai);

  if (ai > ar)                                  /* Switch  ai and ar */
    SWAP (reel, ai, ar)

  return ((ai == 0.0) ? (ar) : (ar * SQRT (1.0 + ai / ar * ai / ar)));
}

/*---------------------------------------------------------------------*/
static int balance
                   (int       n,      /* size of matrix ..............*/
                    reel *    mat,  /* matrix ......................*/
                    reel      scal[], /* Scaling data ................*/
                    int *     low,    /* first relevant row index ....*/
                    int *     high,   /* last relevant row index .....*/
                    int       basis   /* base of computer numbers ....*/
                   )
{
  register int i, j;
  int      iter, k, m;
  reel     b2, r, c, f, g, s;

  b2 = (reel) (basis * basis);
  m = 0;
  k = n - 1;

  do
  {
    iter = FALSE;
    for (j = k; j >= 0; j--)
    {
      for (r = 0, i = 0; i <= k; i++)
        if (i != j)  r += ABS (mat[j*n+i]);

      if (r == 0)
      {
        scal[k] = (reel) j;
        if (j != k)
        {
          for (i = 0; i <= k; i++) SWAP (reel, mat[i*n+j], mat[i*n+k])
          for (i = m; i < n; i++)  SWAP (reel, mat[j*n+i], mat[k*n+i])
        }
        k--;
        iter = TRUE;
      }
    }   /* end of j */
  }   /* end of do  */
  while (iter);

  do
  {
    iter = FALSE;
    for (j = m; j <= k; j++)
    {
      for (c = 0, i = m; i <= k; i++)
        if (i != j) c += ABS (mat[i*n+j]);
      if (c == 0)
      {
        scal[m] = (reel) j;
        if ( j != m )
        {
          for (i = 0; i <= k; i++) SWAP (reel, mat[i*n+j], mat[i*n+m])
          for (i = m; i < n; i++)  SWAP (reel, mat[j*n+i], mat[m*n+i])
        }
        m++;
        iter = TRUE;
      }
    }   /* end of j */
  }   /* end of do  */
  while (iter);

  *low = m;
  *high = k;
  for (i = m; i <= k; i++) scal[i] = 1.0;

  do
  {
    iter = FALSE;
    for (i = m; i <= k; i++)
    {
      for (c = r = 0, j = m; j <= k; j++)
      if (j !=i)
      {
        c += ABS (mat[j*n+i]);
        r += ABS (mat[i*n+j]);
      }
      g = r / basis;
      f = 1.0;
      s = c + r;

      while (c < g)
      {
        f *= basis;
        c *= b2;
      }

      g = r * basis;
      while (c >= g)
      {
        f /= basis;
        c /= b2;
      }

      if ((c + r) / f < (reel)0.95 * s)
      {
        g = 1.0 / f;
        scal[i] *= f;
        iter = TRUE;
        for (j = m; j < n; j++ ) mat[i*n+j] *= g;
        for (j = 0; j <= k; j++ ) mat[j*n+i] *= f;
      }
    }
  }
  while (iter);

  return (0);
}
/*---------------------------------------------------------------------*/
static int balback
                   (int     n,        /* Dimension of matrix .........*/
                    int     low,      /* first nonzero row ...........*/
                    int     high,     /* last nonzero row ............*/
                    reel    scal[],   /* Scaling data ................*/
                    reel *  eivec   /* Eigenvectors ................*/
                   )
{
  register int i, j, k;
  reel s;

  for (i = low; i <= high; i++)
  {
    s = scal[i];
    for (j = 0; j < n; j++) eivec[i*n+j] *= s;
  }

  for (i = low - 1; i >= 0; i--)
  {
    k = (int) scal[i];
    if (k != i)
      for (j = 0; j < n; j++) SWAP (reel, eivec[i*n+j], eivec[k*n+j])
  }

  for (i = high + 1; i < n; i++)
  {
    k = (int) scal[i];
    if (k != i)
      for (j = 0; j < n; j++) SWAP (reel, eivec[i*n+j], eivec[k*n+j])
  }
  return (0);
}
/*---------------------------------------------------------------------*/
static int elmhes
                  (int       n,       /* Dimension of matrix .........*/
                   int       low,     /* first nonzero row ...........*/
                   int       high,    /* last nonzero row ............*/
                   reel *    mat,   /* input/output matrix .........*/
                   int       perm[]   /* Permutation vector ..........*/
                  )
{
  register int i, j, m;
  reel   x, y;

  for (m = low + 1; m < high; m++)
  {
    i = m;
    x = 0;
    for (j = m; j <= high; j++)
      if (ABS (mat[j*n+m-1]) > ABS (x))
      {
        x = mat[j*n+m-1];
        i = j;
      }

    perm[m] = i;
    if (i != m)
    {
      for (j = m - 1; j < n; j++) SWAP (reel, mat[i*n+j], mat[m*n+j])
      for (j = 0; j <= high; j++) SWAP (reel, mat[j*n+i], mat[j*n+m])
    }

    if (x != 0)
    {
      for (i = m + 1; i <= high; i++)
      {
        y = mat[i*n+m-1];
        if (y != 0)
        {
          y = mat[i*n+m-1] = y / x;
          for (j = m; j < n; j++) mat[i*n+j] -= y * mat[m*n+j];
          for (j = 0; j <= high; j++) mat[j*n+m] += y * mat[j*n+i];
        }
      } /* end i */
    }
  } /* end m */

  return (0);
}
/*---------------------------------------------------------------------*/
static int elmtrans
                    (int     n,       /* Dimension of matrix .........*/
                     int     low,     /* first nonzero row ...........*/
                     int     high,    /* last nonzero row ............*/
                     reel *  mat,   /* input matrix ................*/
                     int     perm[],  /* row permutations ............*/
                     reel *  h      /* Hessenberg matrix ...........*/
                    )
{
  register int k, i, j;

  for (i = 0; i < n; i++)
  {
    for (k = 0; k < n; k++) h[i*n+k] = 0;
    h[i*n+i] = 1.0;
  }

  for (i = high - 1; i > low; i--)
  {
    j = perm[i];
    for (k = i + 1; k <= high; k++) h[k*n+i] = mat[k*n+i-1];
    if ( i != j )
    {
      for (k = i; k <= high; k++)
      {
        h[i*n+k] = h[j*n+k];
        h[j*n+k] = 0;
      }
      h[j*n+i] = 1.0;
    }
  }

  return (0);
}
/* ------------------------------------------------------------------ */
static int orthes
                 (
                  int  n,                  /* Dimension of matrix     */
                  int  low,                /* [low,low]..[high,high]: */
                  int  high,               /* submatrix to be reduced */
                  reel *mat,             /* input/output matrix     */
                  reel d[]                 /* reduction information   */
                 )                         /* error code              */

{
  int  i, j, m;    /* loop variables                                  */
  reel s,          /* Euclidian norm sigma of the subdiagonal column  */
                   /* vector v of mat, that shall be reflected into a */
                   /* multiple of the unit vector e1 = (1,0,...,0)    */
                   /* (v = (v1,..,v(high-m+1))                        */
       x = 0,   /* first element of v in the beginning, then       */
                   /* summation variable in the actual Householder    */
                   /* transformation                                  */
       y,          /* sigma^2 in the beginning, then ||u||^2, with    */
                   /* u := v +- sigma * e1                            */
       eps;        /* tolerance for checking if the transformation is */
                   /* valid                                           */

  eps = (reel)128.0 * MACH_EPS;

  for (m = low + 1; m < high; m++)
  {
    for (y = 0, i = high; i >= m; i--)
      x    = mat[i*n+m - 1],
      d[i] = x,
      y    = y + x * x;
    if (y <= eps)
      s = 0;
    else
    {
      s = (x >= 0) ? -SQRT(y) : SQRT(y);
      y    -= x * s;
      d[m] =  x - s;

      for (j = m; j < n; j++)               /* multiply mat from the  */
      {                                     /* left by  (E-(u*uT)/y)  */
        for (x = 0, i = high; i >= m; i--)
          x += d[i] * mat[i*n+j];
        for (x /= y, i = m; i <= high; i++)
          mat[i*n+j] -= x * d[i];
      }

      for (i = 0; i <= high; i++)           /* multiply mat from the  */
      {                                     /* right by  (E-(u*uT)/y) */
        for (x = 0, j = high; j >= m; j--)
          x += d[j] * mat[i*n+j];
        for (x /= y, j = m; j <= high; j++)
          mat[i*n+j] -= x * d[j];
      }
    }

    mat[m*n+m - 1] = s;
  }

  return 0;

}    /* --------------------------- orthes -------------------------- */


/* ------------------------------------------------------------------ */

static int orttrans
                   (
                    int  n,      /* Dimension of matrix               */
                    int  low,    /* [low,low]..[high,high]: submatrix */
                    int  high,   /* affected by the reduction         */
                    reel *mat, /* Hessenberg matrix, reduction inf. */
                    reel d[],    /* remaining reduction information   */
                    reel *v    /* transformation matrix             */
                   )             /* error code                        */

{
  int  i, j, m;                        /* loop variables              */
  reel x,                              /* summation variable in the   */
                                       /* Householder transformation  */
       y;                              /* sigma  respectively         */
                                       /* sigma * (v1 +- sigma)       */

  for (i = 0; i < n; i++)              /* form the unit matrix in v   */
  {
    for (j = 0; j < n; j++)
      v[i*n+j] = 0;
    v[i*n+i] = 1.0;
  }

  for (m = high - 1; m > low; m--)     /* apply the transformations   */
  {                                    /* that reduced mat to upper   */
    y = mat[m*n+m - 1];                 /* Hessenberg form also to the */
                                       /* unit matrix in v. This      */
    if (y != 0)                     /* produces the desired        */
    {                                  /* transformation matrix in v. */
      y *= d[m];
      for (i = m + 1; i <= high; i++)
        d[i] = mat[i*n+m - 1];
      for (j = m; j <= high; j++)
      {
        for (x = 0, i = m; i <= high; i++)
          x += d[i] * v[i*n+j];
        for (x /= y, i = m; i <= high; i++)
          v[i*n+j] += x * d[i];
      }
    }
  }

  return 0;

}
/* -------------------------- orttrans ------------------------- */
static int hqrvec
                  (int     n,           /* Dimension of matrix .......*/
                   int     low,         /* first nonzero row .........*/
                   int     high,        /* last nonzero row ..........*/
                   reel *  h,         /* upper Hessenberg matrix ...*/
                   reel    wr[],        /* reel parts of evalues .....*/
                   reel    wi[],        /* Imaginary parts of evalues */
                   reel *  eivec      /* Eigenvectors ..............*/
                  )
{
  int   i, j, k;
  int   l, m, en, na;
  reel  p, q, r = 0, s = 0, t, w, x, y, z = 0,
        ra, sa, vr, vi, norm;

  for (norm = 0, i = 0; i < n; i++)        /* find norm of h       */
  {
    for (j = i; j < n; j++) norm += ABS(h[i*n+j]);
  }

  if (norm == 0) return (1);               /* zero matrix          */

  for (en = n - 1; en >= 0; en--)             /* transform back       */
  {
    p = wr[en];
    q = wi[en];
    na = en - 1;
    if (q == 0)
    {
      m = en;
      h[en*n+en] = 1.0;
      for (i = na; i >= 0; i--)
      {
        w = h[i*n+i] - p;
        r = h[i*n+en];
        for (j = m; j <= na; j++) r += h[i*n+j] * h[j*n+en];
        if (wi[i] < 0)
        {
          z = w;
          s = r;
        }
        else
        {
          m = i;
          if (wi[i] == 0)
            h[i*n+en] = -r / ((w != 0) ? (w) : (MACH_EPS * norm));
          else
          {  /* Solve the linear system:            */
             /* | w   x |  | h[i][en]   |   | -r |  */
             /* |       |  |            | = |    |  */
             /* | y   z |  | h[i+1][en] |   | -s |  */

             x = h[i*n+i+1];
             y = h[(i+1)*n+i];
             q = SQR (wr[i] - p) + SQR (wi[i]);
             h[i*n+en] = t = (x * s - z * r) / q;
             h[(i+1)*n+en] = ( (ABS(x) > ABS(z) ) ?
                                (-r -w * t) / x : (-s -y * t) / z);
          }
        }  /* wi[i] >= 0  */
      }  /*  end i     */
    }  /* end q = 0  */

    else if (q < 0)
    {
      m = na;
      if (ABS(h[en*n+na]) > ABS(h[na*n+en]))
      {
        h[na*n+na] = - (h[en*n+en] - p) / h[en*n+na];
        h[na*n+en] = - q / h[en*n+na];
      }
      else
       comdiv(-h[na*n+en], 0, h[na*n+na]-p, q, &h[na*n+na], &h[na*n+en]);

      h[en*n+na] = 1.0;
      h[en*n+en] = 0;
      for (i = na - 1; i >= 0; i--)
      {
        w = h[i*n+i] - p;
        ra = h[i*n+en];
        sa = 0;
        for (j = m; j <= na; j++)
        {
          ra += h[i*n+j] * h[j*n+na];
          sa += h[i*n+j] * h[j*n+en];
        }

        if (wi[i] < 0)
        {
          z = w;
          r = ra;
          s = sa;
        }
        else
        {
          m = i;
          if (wi[i] == 0)
            comdiv (-ra, -sa, w, q, &h[i*n+na], &h[i*n+en]);
          else
          {

         /* solve complex linear system:                              */
         /* | w+i*q     x | | h[i][na] + i*h[i][en]  |   | -ra+i*sa | */
         /* |             | |                        | = |          | */
         /* |   y    z+i*q| | h[i+1][na]+i*h[i+1][en]|   | -r+i*s   | */

            x = h[i*n+i+1];
            y = h[(i+1)*n+i];
            vr = SQR (wr[i] - p) + SQR (wi[i]) - SQR (q);
            vi = 2.0 * q * (wr[i] - p);
            if (vr == 0 && vi == 0)
              vr = MACH_EPS * norm *
                  (ABS (w) + ABS (q) + ABS (x) + ABS (y) + ABS (z));

            comdiv (x * r - z * ra + q * sa, x * s - z * sa -q * ra,
                    vr, vi, &h[i*n+na], &h[i*n+en]);
            if (ABS (x) > ABS (z) + ABS (q))
            {
              h[(i+1)*n+na] = (-ra - w * h[i*n+na] + q * h[i*n+en]) / x;
              h[(i+1)*n+en] = (-sa - w * h[i*n+en] - q * h[i*n+na]) / x;
            }
            else
              comdiv (-r - y * h[i*n+na], -s - y * h[i*n+en], z, q,
                                              &h[(i+1)*n+na], &h[(i+1)*n+en]);

          }   /* end wi[i] > 0  */
        }   /* end wi[i] >= 0  */
      }   /* end i            */
    }    /*  if q < 0        */
  }    /* end  en           */

  for (i = 0; i < n; i++)         /* Eigenvectors for the evalues for */
    if (i < low || i > high)      /* rows < low  and rows  > high     */
      for (k = i + 1; k < n; k++) eivec[i*n+k] = h[i*n+k];

  for (j = n - 1; j >= low; j--)
  {
    m = (j <= high) ? j : high;
    if (wi[j] < 0)
    {
      for (l = j - 1, i = low; i <= high; i++)
      {
        for (y = z = 0, k = low; k <= m; k++)
        {
          y += eivec[i*n+k] * h[k*n+l];
          z += eivec[i*n+k] * h[k*n+j];
        }

        eivec[i*n+l] = y;
        eivec[i*n+j] = z;
      }
    }
    else
      if (wi[j] == 0)
      {
        for (i = low; i <= high; i++)
        {
          for (z = 0, k = low; k <= m; k++)
            z += eivec[i*n+k] * h[k*n+j];
          eivec[i*n+j] = z;
        }
      }

  }  /*  end j  */

  return (0);
}
/*---------------------------------------------------------------------*/
static int hqr2         /* compute eigenvalues .......................*/
                (int     vec,         /* switch for computing evectors*/
                 int     n,           /* Dimension of matrix .........*/
                 int     low,         /* first nonzero row ...........*/
                 int     high,        /* last nonzero row ............*/
                 reel *  h,         /* Hessenberg matrix ...........*/
                 reel    wr[],        /* reel parts of eigenvalues ...*/
                 reel    wi[],        /* Imaginary parts of evalues ..*/
                 reel *  eivec,     /* Matrix of eigenvectors ......*/
                 int     cnt[]        /* Iteration counter ...........*/
                )
{
  int  i, j;
  int  na, en, iter, k, l, m;
  reel p = 0, q = 0, r = 0, s, t, w, x, y, z;

  for (i = 0; i < n; i++)
    if (i < low || i > high)
    {
      wr[i] = h[i*n+i];
      wi[i] = 0;
      cnt[i] = 0;
    }

  en = high;
  t = 0;

  while (en >= low)
  {
    iter = 0;
    na = en - 1;

    for ( ; ; )
    {
      for (l = en; l > low; l--)             /* search for small      */
        if ( ABS(h[l*n+l-1]) <=               /* subdiagonal element   */
              MACH_EPS * (ABS(h[(l-1)*n+l-1]) + ABS(h[l*n+l])) )  break;

      x = h[en*n+en];
      if (l == en)                            /* found one evalue     */
      {
        wr[en] = h[en*n+en] = x + t;
        wi[en] = 0;
        cnt[en] = iter;
        en--;
        break;
      }

      y = h[na*n+na];
      w = h[en*n+na] * h[na*n+en];

      if (l == na)                            /* found two evalues    */
      {
        p = (y - x) * 0.5;
        q = p * p + w;
        z = SQRT (ABS (q));
        x = h[en*n+en] = x + t;
        h[na*n+na] = y + t;
        cnt[en] = -iter;
        cnt[na] = iter;
        if (q >= 0)
        {                                     /* reel eigenvalues     */
          z = (p < 0) ? (p - z) : (p + z);
          wr[na] = x + z;
          wr[en] = s = x - w / z;
          wi[na] = wi[en] = 0;
          x = h[en*n+na];
          r = SQRT (x * x + z * z);

          if (vec)
          {
            p = x / r;
            q = z / r;
            for (j = na; j < n; j++)
            {
              z = h[na*n+j];
              h[na*n+j] = q * z + p * h[en*n+j];
              h[en*n+j] = q * h[en*n+j] - p * z;
            }

            for (i = 0; i <= en; i++)
            {
              z = h[i*n+na];
              h[i*n+na] = q * z + p * h[i*n+en];
              h[i*n+en] = q * h[i*n+en] - p * z;
            }

            for (i = low; i <= high; i++)
            {
              z = eivec[i*n+na];
              eivec[i*n+na] = q * z + p * eivec[i*n+en];
              eivec[i*n+en] = q * eivec[i*n+en] - p * z;
            }
          }  /* end if (vec) */
        }  /* end if (q >= 0) */
        else                                  /* pair of complex      */
        {                                     /* conjugate evalues    */
          wr[na] = wr[en] = x + p;
          wi[na] =   z;
          wi[en] = - z;
        }

        en -= 2;
        break;
      }  /* end if (l == na) */

      if (iter >= MAXIT)
      {
        cnt[en] = MAXIT + 1;
        return (en);                         /* MAXIT Iterations     */
      }

      if ( (iter != 0) && (iter % 10 == 0) )
      {
        t += x;
        for (i = low; i <= en; i++) h[i*n+i] -= x;
        s = ABS (h[en*n+na]) + ABS (h[na*n+en-2]);
        x = y = (reel)0.75 * s;
        w = - (reel)0.4375 * s * s;
      }

      iter ++;

      for (m = en - 2; m >= l; m--)
      {
        z = h[m*n+m];
        r = x - z;
        s = y - z;
        p = ( r * s - w ) / h[(m+1)*n+m] + h[m*n+m+1];
        q = h[(m + 1)*n+m + 1] - z - r - s;
        r = h[(m + 2)*n+m + 1];
        s = ABS (p) + ABS (q) + ABS (r);
        p /= s;
        q /= s;
        r /= s;
        if (m == l) break;
        if ( ABS (h[m*n+m-1]) * (ABS (q) + ABS (r)) <=
                 MACH_EPS * ABS (p)
                 * ( ABS (h[(m-1)*n+m-1]) + ABS (z) + ABS (h[(m+1)*n+m+1])) )
          break;
      }

      for (i = m + 2; i <= en; i++) h[i*n+i-2] = 0;
      for (i = m + 3; i <= en; i++) h[i*n+i-3] = 0;

      for (k = m; k <= na; k++)
      {
        if (k != m)             /* double  QR step, for rows l to en  */
        {                       /* and columns m to en                */
          p = h[k*n+k-1];
          q = h[(k+1)*n+k-1];
          r = (k != na) ? h[(k+2)*n+k-1] : 0;
          x = ABS (p) + ABS (q) + ABS (r);
          if (x == 0) continue;                  /*  next k        */
          p /= x;
          q /= x;
          r /= x;
        }
        s = SQRT (p * p + q * q + r * r);
        if (p < 0) s = -s;

        if (k != m) h[k*n+k-1] = -s * x;
          else if (l != m)
                 h[k*n+k-1] = -h[k*n+k-1];
        p += s;
        x = p / s;
        y = q / s;
        z = r / s;
        q /= p;
        r /= p;

        for (j = k; j < n; j++)               /* modify rows          */
        {
          p = h[k*n+j] + q * h[(k+1)*n+j];
          if (k != na)
          {
            p += r * h[(k+2)*n+j];
            h[(k+2)*n+j] -= p * z;
          }
          h[(k+1)*n+j] -= p * y;
          h[k*n+j]   -= p * x;
        }

        j = (k + 3 < en) ? (k + 3) : en;
        for (i = 0; i <= j; i++)              /* modify columns       */
        {
          p = x * h[i*n+k] + y * h[i*n+k+1];
          if (k != na)
          {
            p += z * h[i*n+k+2];
            h[i*n+k+2] -= p * r;
          }
          h[i*n+k+1] -= p * q;
          h[i*n+k]   -= p;
        }

        if (vec)      /* if eigenvectors are needed ..................*/
        {
          for (i = low; i <= high; i++)
          {
            p = x * eivec[i*n+k] + y * eivec[i*n+k+1];
            if (k != na)
            {
              p += z * eivec[i*n+k+2];
              eivec[i*n+k+2] -= p * r;
            }
            eivec[i*n+k+1] -= p * q;
            eivec[i*n+k]   -= p;
          }
        }
      }    /* end k          */

    }    /* end for ( ; ;) */

  }    /* while (en >= low)                      All evalues found    */

  if (vec)                                /* transform evectors back  */
    if (hqrvec (n, low, high, h, wr, wi, eivec)) return (99);
  return (0);
}

/*--------------------------------------------------------------------*/
static int norm_1       /* normalize eigenvectors to have one norm 1 .*/
                  (int     n,       /* Dimension of matrix ...........*/
                   reel *  v,     /* Matrix with eigenvektors ......*/
                   reel    wi[]     /* Imaginary parts of evalues ....*/
                  )
{
  int  i, j;
  reel maxi, tr, ti;

  if (n < 1) return (1);

  for (j = 0; j < n; j++)
  {
    if (wi[j] == 0)
    {
      maxi = v[0*n+j];
      for (i = 1; i < n; i++)
        if (ABS (v[i*n+j]) > ABS (maxi))  maxi = v[i*n+j];

      if (maxi != 0)
      {
        maxi = 1.0 / maxi;
        for (i = 0; i < n; i++) v[i*n+j] *= maxi;
      }
    }
    else
    {
      tr = v[0*n+j];
      ti = v[0*n+j+1];
      for (i = 1; i < n; i++)
        if ( comabs (v[i*n+j], v[i*n+j+1]) > comabs (tr, ti) )
        {
          tr = v[i*n+j];
          ti = v[i*n+j+1];
        }

      if (tr != 0 || ti != 0)
        for (i = 0; i < n; i++)
          comdiv (v[i*n+j], v[i*n+j+1], tr, ti, &v[i*n+j], &v[i*n+j+1]);

      j++;                                          /* raise j by two */
    }
  }
  return (0);
}


/*--------------------------------------------------------------------*/
static int norm_2       /* normalize eigenvectors to have one norm 2 .*/
                  (int     n,       /* Dimension of matrix ...........*/
                   reel *  v,     /* Matrix with eigenvektors ......*/
                   reel    wi[]     /* Imaginary parts of evalues ....*/
                  )
{
  int  i, j;
  reel maxi, tr, ti;

  if (n < 1) return (1);

  for (j = 0; j < n; j++)
  {
    if (wi[j] == 0)
    {
      maxi = 0;
      for (i = 0; i < n; i++)
        maxi=maxi+v[i*n+j]*v[i*n+j];

      if (maxi != 0)
      {
        maxi = 1.0 / sqrt(maxi);
        for (i = 0; i < n; i++) v[i*n+j] *= maxi;
      }
    }
    else
    {
      maxi =0;
      for (i = 0; i < n; i++)
        {tr=comabs (v[i*n+j], v[i*n+j+1]);
         maxi=maxi+tr*tr;
        }

      if (maxi != 0)
      {
        maxi = 1.0 / sqrt(maxi);
        for (i = 0; i < n; i++) {v[i*n+j] *= maxi; v[i*n+j+1] *= maxi;}
      }

      j++;                                          /* raise j by two */
    }
  }
  return (0);
}

/*---------------------------------------------------------------------*/
int eigen               /* Compute all evalues/evectors of a matrix ..*/
          (
           int     vec,           /* switch for computing evectors ...*/
           int     ortho,         /* orthogonal Hessenberg reduction? */
           int     ev_norm,       /* normalize Eigenvectors? .........*/
           int     n,             /* size of matrix ..................*/
           reel *  mat,         /* input matrix ....................*/
           reel *  eivec,       /* Eigenvectors ....................*/
           reel    valre[],       /* reel parts of eigenvalues .......*/
           reel    valim[],       /* imaginary parts of eigenvalues ..*/
           int     cnt[]          /* Iteration counter ...............*/
          )
{
  register i;
  int      low, high, rc;
  reel     *scale,
           *d = NULL;
  void     *vmblock;

  if (n < 1) return (1);                       /*  n >= 1 ............*/

  if (valre == NULL || valim == NULL || mat == NULL || cnt == NULL)
    return (1);

  for (i = 0; i < n; i++) cnt[i] = 0;

  if (n == 1)                                  /*  n = 1 .............*/
  {
    eivec[0*n+0] = 1.0;
    valre[0]    = mat[0*n+0];
    valim[0]    = 0;
    return (0);
  }

  if (vec)
  {
    if (eivec == NULL) return (1);
  }

  scale = (reel *)calloc(n,sizeof(reel));
  if (scale==NULL) {printf("Erreur alloc)"); return(2);}

  if (vec && ortho)                          /* with Eigenvectors     */
  {                                          /* and orthogonal        */
                                             /* Hessenberg reduction? */
    d = (reel *) calloc(n,sizeof(reel));
    if (d==NULL) {printf("Erreur alloc)"); return(2);}
  }

                                            /* balance mat for nearly */
  rc = balance (n, mat, scale,              /* equal row and column   */
                  &low, &high, BASIS);      /* one norms              */
  if (rc)
  {
    free(scale); free(d);
    return (100 + rc);
  }

  if (ortho)
    rc = orthes(n, low, high, mat, d);
  else
    rc = elmhes (n, low, high, mat, cnt);   /*  reduce mat to upper   */
  if (rc)                                   /*  Hessenberg form       */
  {
    free(scale);free(d);
    return (200 + rc);
  }

  if (vec)                                  /*  initialize eivec      */
  {
    if (ortho)
      rc = orttrans(n, low, high, mat, d, eivec);
    else
      rc = elmtrans (n, low, high, mat, cnt, eivec);
    if (rc)
    {
      free(scale); free(d);
      return (300 + rc);
    }
  }

  rc = hqr2 (vec, n, low, high, mat,        /*  execute Francis QR    */
             valre, valim, eivec, cnt);     /*  algorithm to obtain   */
  if (rc)                                   /*  eigenvalues           */
  {
    free(scale); free(d);
    return (400 + rc);
  }

  if (vec)
  {
    rc = balback (n, low, high,             /*  reverse balancing if  */
                      scale, eivec);        /*  eigenvaectors are to  */
    if (rc)                                 /*  be determined         */
    {
      free(scale);free(d);
      return (500 + rc);
    }
    if (ev_norm)
      rc = norm_2 (n, eivec, valim);        /* normalize eigenvectors */
    if (rc)
    {
      free(scale); free(d);
      return (600 + rc);
    }
  }

  free(scale);                          /* free buffers           */
  free(d);


  return (0);
}
/*---------------------------------------------------------------------------*/
#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define MASK 123459876

reel ran0(long *idum)
{
 long k;
 reel ans;

 *idum ^=MASK;
 k=(*idum)/IQ;
 *idum=IA*(*idum-k*IQ)-IR*k;
 if (*idum<0) *idum+=IM;
 ans=AM*(*idum);
 *idum^=MASK;

return ans;
}



