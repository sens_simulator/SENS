void normalise_tot(reel *psi);

/*--------------------------------------------------------------------------*/
void Hamilton(reel *result,reel *psi,int n)
{
#define PSI(i,j,k) (psi[(k)*Nptaxe2+(j)*Nptaxe+(i)])
#define RESULT(i,j,k) (result[(k)*Nptaxe2+(j)*Nptaxe+(i)])
#define MASSE(i,j,k) (masse[(k)*Nptaxe2+(j)*Nptaxe+(i)])

 int i,j,k;
 reel psiip,psiim,psijp,psijm,psikp,psikm;
 reel mip,mim,mjp,mjm,mkp,mkm;


 for(k=0;k<Nptaxe;k++)
   {
    for(j=0;j<Nptaxe;j++)
      {
       for(i=0;i<Nptaxe;i++)
         {

 /*          if (i==Nptaxe-1) psiip=0;  else psiip=PSI(i+1,j,k);
           if (i==0)        psiim=0;  else psiim=PSI(i-1,j,k);
           if (j==Nptaxe-1) psijp=0;  else psijp=PSI(i,j+1,k);
           if (j==0)        psijm=0;  else psijm=PSI(i,j-1,k);
           if (k==0)        psikm=0;  else psikm=PSI(i,j,k-1);
           if (k==Nptaxe-1) psikp=0;  else psikp=PSI(i,j,k+1);
 

           RESULT(i,j,k)=
           -0.5/(dx2)*(psiim+psiip+psijp+psijm+psikp+psikm-6*PSI(i,j,k))
           +(Vconf[k*Nptaxe2+j*Nptaxe+i]+Vinter[k*Nptaxe2+j*Nptaxe+i]+Vxc[k*Nptaxe2+j*Nptaxe+i])*PSI(i,j,k);
*/

if (i==Nptaxe-1) {psiip=0; mip=MASSE(Nptaxe-1,j,k);} else {psiip=PSI(i+1,j,k); mip=MASSE(i+1,j,k);}
if (i==0)        {psiim=0; mim=MASSE(0,j,k);}        else {psiim=PSI(i-1,j,k); mim=MASSE(i-1,j,k);}
if (j==Nptaxe-1) {psijp=0; mjp=MASSE(i,Nptaxe-1,k);} else {psijp=PSI(i,j+1,k); mjp=MASSE(i,j+1,k);}
if (j==0)        {psijm=0; mjm=MASSE(i,0,k);}        else {psijm=PSI(i,j-1,k); mjm=MASSE(i,j-1,k);}
if (k==Nptaxe-1) {psikp=0; mkp=MASSE(i,j,Nptaxe-1);} else {psikp=PSI(i,j,k+1); mkp=MASSE(i,j,k+1);}
if (k==0)        {psikm=0; mkm=MASSE(i,j,0);}        else {psikm=PSI(i,j,k-1); mkm=MASSE(i,j,k-1);}
   

RESULT(i,j,k)=
-0.5/(masse[k*Nptaxe2+j*Nptaxe+i]*dx*dx)*(psiim+psiip+psijp+psijm+psikp+psikm-6*PSI(i,j,k))
+(Vconf[k*Nptaxe2+j*Nptaxe+i]+Vpolar[k*Nptaxe2+j*Nptaxe+i]+Vinter[k*Nptaxe2+j*Nptaxe+i]+Vxc[k*Nptaxe2+j*Nptaxe+i])*PSI(i,j,k)
-0.125/(dx*dx)*((psiip-psiim)*(1/mip-1/mim)+(psijp-psijm)*(1/mjp-1/mjm)+(psikp-psikm)*(1/mkp-1/mkm)); 

         }
      }
   }




}
/*----------------------------------------------------------------------------*/
void normalise_tot(reel *psi)
{
 int j,i;
 reel norm;
 reel dV;

 dV=dx*dx*dx;

 for(j=0;j<Nborbit;j++)
   {
    norm=0;
    for(i=0;i<Nptmax;i++) norm=norm+psi[j*Nptmax+i]*psi[j*Nptmax+i]*dV;
    for(i=0;i<Nptmax;i++) psi[j*Nptmax+i]=psi[j*Nptmax+i]/sqrt(norm);
   }
}
/*----------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------*-------------------------------
 *Calcul de l energie globale du syteme  *
 *---------------------------------------*/
reel calc_energtot(reel *E,reel dx,reel *Vint,reel *Vxc,reel *ndens)
{
 reel Etot,temp;
 int i;
 reel dV;

 dV=dx*dx*dx;
 Etot=0;
 /*calc_Vint(Vinter,Vxc,Upot,upsi,r,dx);*/
 for(i=0;i<Nborbit;i++) Etot=Etot+Orbitale[i]*E[i];

 temp=0.0;
 for(i=0;i<Nptmax;i++) temp=temp+0.25*ndens[i]*(2.0*Vint[i]+Vxc[i]);
 Etot=Etot-temp*dV;

return Etot;
}
/*----------------------------------------------------------------------------*/

