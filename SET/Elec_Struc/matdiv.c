reel funcVconf(reel x,reel y,reel z);
reel create_maille(reel *x,reel *y,reel *z);
reel funcmasse(reel x,reel y,reel z);
reel funcepsi(reel x,reel y,reel z);
void create_Vconf(reel *Vconf);
void create_epsilon(reel *epsilon);
void create_epsilon2(reel *epsilon2);
void create_masse(reel *masse);
void init_vect(reel *vect,int n);

/*---------------------------------------------------------------------------*/
reel create_maille(reel *x,reel *y,reel *z)
{
 int i;
 reel step;

 step=acube/(Nptaxe-1);
 for(i=0;i<Nptaxe;i++)
   {
    x[i]=-acube/2.0+i*step;
    y[i]=-acube/2.0+i*step;
    z[i]=-acube/2.0+i*step;
   }
return step;
}
/*---------------------------------------------------------------------------*/
reel funcVconf(reel x,reel y,reel z)
{
 reel V0=3.1/(2.0*13.6);
 if (sqrt(x*x+y*y+z*z)>a) return V0; else return 0.0;
}
/*---------------------------------------------------------------------------*/
reel funcmasse(reel x,reel y,reel z)
{
 reel msi=0.27;
 reel msio2=0.5;
 reel r;

 //r=sqrt(x*x+y*y+z*z);
 if (sqrt(x*x+y*y+z*z)>a) return msio2; else return msi;
 //return msi+0.5*(msio2-msi)*(1+tanh(2*(r-a)));
}
/*---------------------------------------------------------------------------*/
reel funcepsi(reel x,reel y,reel z)
{
 reel esi=11.7;
 reel esio2=3.8;
 reel r;


if (sqrt(x*x+y*y+z*z)>a) return esio2; else return esi;
}
/*
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
reel funcVconf(reel x,reel y,reel z)
{
 reel V0=2.0/(2.0*13.6);
 if ((fabs(z)>a)||(fabs(x)>a)||(fabs(y)>a)) return V0; else return 0.0;
}
//---------------------------------------------------------------------------
reel funcmasse(reel x,reel y,reel z)
{
 reel msi=0.27;
 reel msio2=0.45;
 reel r,m;

 if ((fabs(z)>a)||(fabs(x)>a)||(fabs(y)>a)) return msio2; else return msi;
 }
//---------------------------------------------------------------------------
reel funcepsi(reel x,reel y,reel z)
{
 reel esi=11.7;
 reel esio2=9.0;
 reel r,ep;
 if ((fabs(z)>a)||(fabs(x)>a)||(fabs(y)>a)) return esio2; else return esi;
}
*/


/*---------------------------------------------------------------------------*/
void create_Vconf(reel *Vconf)
{
 int i,j,k;

 for(i=0;i<Nptaxe;i++)
 for(j=0;j<Nptaxe;j++)
 for(k=0;k<Nptaxe;k++)
 Vconf[k*Nptaxe2+j*Nptaxe+i]=funcVconf(x[i],y[j],z[k]);
}
/*---------------------------------------------------------------------------*/
void create_masse(reel *masse)
{
 int i,j,k;

 for(i=0;i<Nptaxe;i++)
 for(j=0;j<Nptaxe;j++)
 for(k=0;k<Nptaxe;k++)
 masse[k*Nptaxe2+j*Nptaxe+i]=funcmasse(x[i],y[j],z[k]);
}
/*---------------------------------------------------------------------------*/
void create_epsilon(reel *epsilon)
{
 int i,j,k;

 for(i=0;i<Nptaxe;i++)
 for(j=0;j<Nptaxe;j++)
 for(k=0;k<Nptaxe;k++)
 epsilon[k*Nptaxe2+j*Nptaxe+i]=funcepsi(x[i],y[j],z[k]);
}
/*---------------------------------------------------------------------------*/
void create_epsilon2(reel *epsilon2)
{
 int i,j,k;

 for(i=0;i<Nptaxe;i++)
 for(j=0;j<Npty2;j++)
 for(k=0;k<Nptaxe;k++)
 epsilon2[k*Nptxy2+j*Nptaxe+i]=funcepsi(x[i],y2[j],z[k]);
}

/*---------------------------------------------------------------------------*/
void init_vect(reel *vect,int n)
{
 int i;

 for(i=0;i<n;i++) vect[i]=0;
}
/*---------------------------------------------------------------------------*/
/*reel chgt_variable(reel *Vpol,reel *epsilon,reel *Vpol2,reel *epsilon2)
{
int i,j,k;
for(i=0;i<Nptx;i++)
{
for(j=0;j<Npty;j++)
{
for(k=0;k<Nptz;k++)
{
Vpol[(k)+Nptxy+(j)*Nptx+(i)]=Vpol2[(k)*Nptxy2+(j)*Nptx+(i)];
epsilon[k*Nptxy+j*Nptx+i]=epsilon2[k*Nptxy2+j*Nptx+i];
}
}
}
}*/
