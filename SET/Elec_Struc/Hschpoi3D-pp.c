///*-----------------------------------------------------------------------------
//     R�solution 3D de Poisson Schrodinger pour les points quantiques
//     m�thodes utilis�s: - Functional Density (LDA)
//                        - Recherche des valeurs propres par m�thode de
//                          Arnoldi
//
//                                                    Johann S�e 25/08/01
//----------------------------------------------------------------------------*/
//
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#define Pi 3.14159265359
#define abohr 0.52918 //Rayon de Bohr en Angstrom
#define Nptaxe 40
#define Nptaxe2 Nptaxe*Nptaxe
#define Nptmax Nptaxe*Nptaxe*Nptaxe
#define TOL 1e-5
#define Nbitermaxpoi 500000
#define Nbiterschropoi 8


//Variables du dispo
#define Vmin -0.006
#define Vmax 0.0061
#define Vgmin VGMINE
#define Vgmax VGMAXE
#define pasV 0.001
#define pasVg 0.03


#define hmin 12.0/abohr //hGauche
#define hmax 15.0/abohr //hDroite
#define hg 50.0/abohr //hgrille
#define a 44.4/(2*abohr) //diametre dot
#define Nemin 1 //nombre mini d'electrons dans le dot
#define Nelec 4 //nombre maximum d'electrons dans le dot
#define acube 2*(a+hmin+hmax+5/abohr)

/*-----------------------------------------------------------------------------
                                  Les types
-----------------------------------------------------------------------------*/
typedef double reel;

/*-----------------------------------------------------------------------------
                                 Les Variables
-----------------------------------------------------------------------------*/

/* Calcul du nombre de polarisation */
int Nbpolar=(Vmax-Vmin)/pasV+1;
int Nbpolarg=(Vgmax-Vgmin)/pasVg+1;
/*Les variables*/
reel *x,*y,*y2,*z;
reel dx;

reel *masse;
reel *epsilon,*epsilon2;

reel *psi,*psi2;
reel *ndens,*ndens2;

reel *Vconf;
reel *Vinter,*Vxc,*Vpolar2,*Vpolar;

int Npty2,Nptxy2,Nptmax2;
int  Nborbit,Nbsearch;
reel Orbitale[20];

int Nemax; //variation nombre electrons
int polpos; //variation V_DS
int polpog; //variation V_GS
reel polarV; //V_DS
reel Vg; //Vg (oui, oui)

/* Les fichiers */
FILE *fichdata;
FILE *fichdata2;
FILE *fichdata3;
FILE *fichdispo;
FILE *fichpsi;
FILE *fichVpol;
FILE *fichE;
FILE *fichlog;
char nomfich[60],nomdoss[80],nomdisp[80];


/*-----------------------------------------------------------------------------
                               Prototypes de Fonctions
-----------------------------------------------------------------------------*/
void Hamilton(reel *result,reel *psi,int n);
#include "matdiv.c"
#include "schrod.c"
#include "eigval.c"
#include "Hpoiss3D.c"
///*-----------------------------------------------------------------------------
//                                   Le Main
//-----------------------------------------------------------------------------*/
int main()
{
// time_t time_i,time_tot,time_ipol,time_fpol;
// time_i=time(NULL);
//
 reel E[20],E2[20],Etemp,Etot,Einter;
 reel temp;
 reel psitemp;
 reel result[15][Nbpolar];
 reel polar[Nbpolar],polarg[Nbpolarg];

 int i,j,k,ii,jj,kk;

 /* Creation dossier dispo */
 /*-----------------------*/

sprintf(nomdisp,"%1.0f-%1.0f-%1.0f-%1.0f-pp",2*a*abohr,hmin*abohr,hmax*abohr,hg*abohr);
mkdir(nomdisp, 01777);
/*--------------------*/

 /*Allocation de la m�moire des donn�es non d�pendantes de V/Vg/Ne*/
 /*------------------------*/
 x=malloc(Nptaxe*sizeof(reel));
 if(x==NULL) printf("Erreur allocation de m�moire !\n");
 y=malloc(Nptaxe*sizeof(reel));
 if(y==NULL) printf("Erreur allocation de m�moire !\n");
 z=malloc(Nptaxe*sizeof(reel));
 if(z==NULL) printf("Erreur allocation de m�moire !\n");
 masse=malloc(Nptmax*sizeof(reel));
 if(masse==NULL) printf("Erreur allocation de m�moire !\n");
 epsilon=malloc(Nptmax*sizeof(reel));
 if(epsilon==NULL) printf("Erreur allocation de m�moire !\n");
 Vconf=malloc(Nptmax*sizeof(reel));
 if(Vconf==NULL) printf("Erreur allocation de m�moire !\n");

 //Remplissage de ces donn�es...
 dx=create_maille(x,y,z);
 create_Vconf(Vconf);
 create_masse(masse);
 create_epsilon(epsilon);

 // Agrandissement de la grille suivant les y, pour la grille !
 Npty2=ceil((acube+hg)/dx)+1;
 Nptxy2=Nptaxe*Npty2;
 Nptmax2=Nptaxe*Nptaxe*Npty2;

 y2=malloc(Npty2*sizeof(reel));
 if(y2==NULL) printf("Erreur allocation de m�moire !\n");
 epsilon2=malloc(Nptmax2*sizeof(reel));
 if(epsilon2==NULL) printf("Erreur allocation de m�moire !\n");

 for(j=0;j<Npty2;j++) y2[j]=-acube/2.0+j*dx;
 create_epsilon2(epsilon2);

 //Creation des polarisations
 for(i=0;i<Nbpolar;i++) polar[i]=Vmin+i*pasV;
 for(j=0;j<Nbpolarg;j++) polarg[j]=Vgmin+j*pasVg;
//
 //Sauvegarde des caract�ristiques dispo
sprintf(nomfich,"%s/dispo.txt",nomdisp);
fichdata=fopen(nomfich,"w");
 fprintf(fichdata,"%d\n%d\n%f\n%f\n%f\n%f\n%d\n%d\n%d\n",Nptaxe,Npty2,a*abohr,hmin*abohr,hmax*abohr,hg*abohr,Nelec,Nbpolar,Nbpolarg);
 for(i=0;i<Nbpolar;i++) fprintf(fichdata,"%f\n",polar[i]);
 for(j=0;j<Nbpolarg;j++) fprintf(fichdata,"%f\n",polarg[j]);
 fclose(fichdata);

 for(i=0;i<15;i++) for(j=0;j<Nbpolar;j++) result[i][j]=0;

 //Creation du fichier log g�n�ral
 sprintf(nomfich,"%s/schpoiLDA_a%2.0f.txt",nomdisp,a*abohr);
 fichlog=fopen(nomfich,"w");



/** OUVERTURE DES BOUCLES **/


 for(polpos=0;polpos<Nbpolar;polpos++)  // D�but de la boucle VDS
 {
  polarV=polar[polpos]/27.2;
  for(polpog=0;polpog<Nbpolarg;polpog++) // Debut de la boucle VGS
  {
   Vg=polarg[polpog]/27.2;

   //Allocation m�moire
   Vpolar=malloc(Nptmax*sizeof(reel));
   if(Vpolar==NULL) printf("Erreur allocation de m�moire !\n");
   Vpolar2=malloc(Nptmax2*sizeof(reel));
   if(Vpolar2==NULL) printf("Erreur allocation de m�moire !\n");


//---------------------------------------------------------------------------
//----------------CALCUL DE VPOL, INDEP DE LA CHARGE-------------------------
//---------------------------------------------------------------------------
   printf("- Calcul potentiel de polarisation...\n");
   fprintf(fichlog,"- Calcul potentiel de polarisation...\n");

   create_polarx(Vpolar2,Vpolar,0.0,-polarV,-Vg,1e-8);

   for(Nemax=Nemin;Nemax<=Nelec;Nemax++)  // D�but de la boucle Nelectrons
   {
    /*Calcul du nombre de fnct d'onde*/
    /*-------------------------------*/
    temp=(reel)Nemax;
    temp=temp/12.0;
    Nborbit=ceil(temp);
    for(i=0;i<Nborbit-1;i++) Orbitale[i]=12.0;
    if ((Nemax%12)==0) Orbitale[Nborbit-1]=12.0; else Orbitale[Nborbit-1]=(reel)(Nemax%12);
    Nbsearch=Nborbit+6;

    /*Allocation de la m�moire*/
    /*------------------------*/
    ndens=malloc(Nborbit*Nptmax*sizeof(reel));
    if(ndens==NULL) printf("Erreur allocation de m�moire !\n");
    ndens2=malloc(Nborbit*Nptmax*sizeof(reel));
    if(ndens2==NULL) printf("Erreur allocation de m�moire !\n");
    Vinter=malloc(Nptmax*sizeof(reel));
    if(Vinter==NULL) printf("Erreur allocation de m�moire !\n");
    Vxc=malloc(Nptmax*sizeof(reel));
    if(Vxc==NULL) printf("Erreur allocation de m�moire !\n");
    psi=malloc(Nptmax*Nbsearch*sizeof(reel));
    if(psi==NULL) printf("Erreur allocation de m�moire !\n");
    psi2=malloc(Nptmax*Nbsearch*sizeof(reel));
    if(psi2==NULL) printf("Erreur allocation de m�moire !\n");

    init_vect(Vinter,Nptmax);
    init_vect(Vxc,Nptmax);
	init_vect(ndens,Nptmax);

    /*Initialisation*/
    /*--------------*/

   printf("\nTaille du point quantique: %2.0f A Polarisation: %6.2f V\n",a*abohr,polarV*27.2);
   fprintf(fichlog,"\nTaille du point quantique: %2.0f A Polarisation: %6.2f V\n",a*abohr,polarV*27.2);
   printf("Polarisation de grille: %6.2f V\n",Vg*27.2);
   fprintf(fichlog,"Polarisation de grille: %6.2f V\n",Vg*27.2);
    printf("-----------------------------------------------\n");
    printf("Nb electrons: %d  Nb fonctions d'onde: %d  \n",Nemax,Nborbit);
    printf("Organisation:");
    for(i=0;i<Nborbit;i++) printf("%2.0f   ",Orbitale[i]);
    printf("\n");

    fprintf(fichlog,"---------------------------------------------\n");
    fprintf(fichlog,"Nb electrons: %d  Nb fonctions d'onde: %d \n",Nemax,Nborbit);
    fprintf(fichlog,"Organisation:");
    for(i=0;i<Nborbit;i++) fprintf(fichlog,"%2.0f   ",Orbitale[i]);
    fprintf(fichlog,"\n");


    /*Programme principal    */
    /*-----------------------*/

    printf("- Initialisation...\n");
    fprintf(fichlog,"- Initialisation...\n");

    myeigs(Nptmax,Nbsearch,psi,E);
    normalise_tot(psi);
    for(i=0;i<Nborbit;i++){ printf("          E%d=%f eV\n",i,E[i]*2*13.6);
    fprintf(fichlog,"          E%d=%f eV\n",i,E[i]*2*13.6);}

    printf("- Poisson Schrodinger Self consistent...\n");
    fprintf(fichlog,"- Poisson Schrodinger Self consistent...\n");

    Etot=0;
    for(k=0;k<Nborbit;k++) Etot=Etot+Orbitale[k]*E[k];

    for(k=0;k<Nborbit;k++) {Orbitale[k]=Orbitale[k]-1;
    create_dens(ndens+k*Nptmax,psi);
    Orbitale[k]=Orbitale[k]+1;}
    if (Nemax>0)
    {
     for(j=0;j<Nbiterschropoi;j++)
     {
      Etemp=Etot;
      Etot=0;
      for(k=0;k<Nborbit;k++)
      {
       solve_Poi(ndens+k*Nptmax,Vinter,1e-8);
       printf("\b");fflush(stdout);
       printf("\bS");fflush(stdout);
       myeigs(Nptmax,Nbsearch,psi2,E2);
       normalise_tot(psi2);
       printf("\b");fflush(stdout);

       E[k]=E2[k];
       for(i=0;i<Nptmax;i++) psi[k*Nptmax+i]=psi2[k*Nptmax+i];

       Einter=0;
       for(i=0;i<Nptmax;i++) Einter=Einter+psi[k*Nptmax+i]*psi[k*Nptmax+i]*Vinter[i]*dx*dx*dx;

       Etot=Etot+Orbitale[k]*(E[k]-0.5*Einter);
       printf("          E%d=%f eV\n",k,E[k]*2.0*13.6);
       fprintf(fichlog,"          E%d=%f eV\n",k,E[k]*2.0*13.6);
      }
      for(k=0;k<Nborbit;k++) for(i=0;i<Nptmax;i++) ndens2[k*Nptmax+i]=ndens[k*Nptmax+i];
      for(k=0;k<Nborbit;k++) {Orbitale[k]=Orbitale[k]-1;
      create_dens(ndens+k*Nptmax,psi);
      Orbitale[k]=Orbitale[k]+1;}
      for(k=0;k<Nborbit;k++) for(i=0;i<Nptmax;i++) ndens[k*Nptmax+i]=(ndens[k*Nptmax+i]+ndens2[k*Nptmax+i])/2.0;

      printf("                        Etot=%f",Etot);
      printf("\n");fprintf(fichlog,"\n");

      if (fabs((Etot-Etemp)/Etot)<=1e-2)
      {printf("self consistent termine \n");fprintf(fichlog,"self consistent termine \n"); break;}
     }


     if (fabs((Etot-Etemp)/Etot)>1e-2)
     {
      printf("self consistent n a pas converge \n");
      fprintf(fichlog,"self consistent n a pas converge \n");

     }

     printf("Etot=%f Hartree\n",Etot);
     fprintf(fichlog,"Etot=%f Hartree\n",Etot);
    }

    for(j=0;j<Nborbit;j++) if(E[j]>3.1/27.2) Etot=-1;
    result[Nemax-1][polpos]=Etot;

    /*Sauvegarde des fichiers*/
    /*-----------------------*/

     fprintf(fichlog,"sauvegardes\n");

    //E
  sprintf(nomdoss,"%s/E",nomdisp);
  mkdir(nomdoss, 01777);
    sprintf(nomfich,"%s/E/e_e%d_V%2.5f_Vg%2.5f.txt",nomdisp,Nemax,polarV*27.2,Vg*27.2);
    fichdata=fopen(nomfich,"w");
    for(j=0;j<Nborbit;j++)
    {
     fprintf(fichdata,"%d %f \n",j,E[j]);
    }
    fprintf(fichdata,"%d %f \n",Nemax,Etot);
    fclose(fichdata);
    //Psi
  sprintf(nomdoss,"%s/Psi",nomdisp);
  mkdir(nomdoss, 01777);
      sprintf(nomfich,"%s/Psi/psi_e%d_V%2.5f_Vg%2.5f.txt",nomdisp,Nemax,polarV*27.2,Vg*27.2);
    fichdata=fopen(nomfich,"w");
    for(i=0;i<Nptmax;i++)
    {
     for(j=0;j<Nborbit;j++)
     {
      fprintf(fichdata," %le",psi[j*Nptmax+i]);
     }
     fprintf(fichdata,"\n");
    }
    fclose(fichdata);
    /*
    //Vint
    mkdir("Vin", 01777);
    sprintf(nomfich,"Vin/vin_e%d_V%2.5f_Vg%2.5f.txt",Nemax,polarV*27.2,Vg*27.2);
    fichdata=fopen(nomfich,"w");
    for(i=0;i<Nptmax;i++)
    {
      fprintf(fichdata," %le",Vinter[i]);
     fprintf(fichdata,"\n");
    }
    fclose(fichdata);
    */

    /*   //Densit�

    sprintf(nomfich,"dens/dens_e%d_V%2.5f_Vg%2.5f.txt",Nemax,polarV*27.2,Vg*27.2);
    fichdata=fopen(nomfich,"w");
    for(i=0;i<Nptmax;i++) fprintf(fichdata," %le",ndens[i]);
    fclose(fichdata);*/


    /* //Vxc
    mkdir("Vxc", 01777);
    sprintf(nomfich,"Vxc/vxc_a%2.0f_e%d_V%2.5f_Vg%2.5f.txt",Nemax,polarV*27.2,Vg*27.2);
    fichdata=fopen(nomfich,"w");
    for(i=0;i<Nptmax;i++) fprintf(fichdata," %le",Vxc[i]);
    fclose(fichdata); */
   }

   //Vpol
  sprintf(nomdoss,"%s/Vpol",nomdisp);
  mkdir(nomdoss, 01777);
     sprintf(nomfich,"%s/Vpol/Vpol_V%2.5f_Vg%2.5f.txt",nomdisp,polarV*27.2,Vg*27.2);
   fichdata=fopen(nomfich,"w");
   for(i=0;i<Nptmax;i++) fprintf(fichdata," %le",Vpolar[i]*27.2);
   fclose(fichdata);
   //Vpol2
   sprintf(nomdoss,"%s/Vpol2",nomdisp);
  mkdir(nomdoss, 01777);
   sprintf(nomfich,"%s/Vpol2/Vpol2_V%2.5f_Vg%2.5f.txt",nomdisp,polarV*27.2,Vg*27.2);
   fichdata=fopen(nomfich,"w");
   for(i=0;i<Nptmax2;i++) fprintf(fichdata," %le",Vpolar2[i]*27.2);
   fclose(fichdata);
  }
 }

 // axes

  sprintf(nomdoss,"%s/axe",nomdisp);
  mkdir(nomdoss, 01777);
 sprintf(nomfich,"%s/axe/axex_a%2.0f.txt",nomdisp,a*abohr);
 fichdata=fopen(nomfich,"w");
 for(i=0;i<Nptaxe;i++)
 {
  fprintf(fichdata,"%f\n",x[i]);
 }
 fclose(fichdata);
 sprintf(nomfich,"%s/axe/axey_a%2.0f.txt",nomdisp,a*abohr);
 fichdata=fopen(nomfich,"w");
 for(j=0;j<Nptaxe;j++)
 {
  fprintf(fichdata,"%f\n",y[j]);
 }
 fclose(fichdata);
 sprintf(nomfich,"%s/axe/axey2_a%2.0f.txt",nomdisp,a*abohr);
 fichdata=fopen(nomfich,"w");
 for(j=0;j<Npty2;j++)
 {
  fprintf(fichdata,"%f\n",y2[j]);
 }
 fclose(fichdata);
 sprintf(nomfich,"%s/axe/axez_a%2.0f.txt",nomdisp,a*abohr);
 fichdata=fopen(nomfich,"w");
 for(k=0;k<Nptaxe;k++)
 {
 fprintf(fichdata,"%f\n",z[k]);
 }
 fclose(fichdata);

 /* //masse
 mkdir("masse", 01777);
 sprintf(nomfich,"masse/masse_a%2.0f.txt",a*abohr);
 fichdata=fopen(nomfich,"w");
 for(i=0;i<Nptmax;i++) fprintf(fichdata," %le",masse[i]);
 fclose(fichdata);

 // Epsilon
 mkdir("epsilon", 01777);
 sprintf(nomfich,"epsilon/epsi_a%2.0f.txt",a*abohr);
 fichdata=fopen(nomfich,"w");
 for(i=0;i<Nptmax;i++) fprintf(fichdata," %le",epsilon[i]);
 fclose(fichdata);

 // Epsilon2
 mkdir("epsilon2", 01777);

 sprintf(nomfich,"epsilon2/epsi_a%2.0f.txt",a*abohr);
 fichdata=fopen(nomfich,"w");
 for(i=0;i<Nptmax;i++) fprintf(fichdata," %le",epsilon2[i]);
 fclose(fichdata); */


 /*
 //Vconf
 mkdir("Vconf", 01777);
 sprintf(nomfich,"Vconf/vconf_a%2.0f.txt",a*abohr);
 fichdata=fopen(nomfich,"w");
 for(i=0;i<Nptmax;i++) fprintf(fichdata," %le",Vconf[i]);
 fclose(fichdata);

 */

 fprintf(fichlog,"simu finie\n");
 fclose(fichlog);

 /*Fin du main*/
 /*-----------*/
// time_tot=time(NULL)-time_i;
// printf("\nfin\n%d\n",time_tot);
 scanf("fin");

printf("end\n");
return 0;
}
