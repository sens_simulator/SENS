# **Simulateur SENS** - _Documentation_

## 1. Introduction
## 2. Dispositifs à boîtes quantiques métalliques
### 2. 1. Théorie orthodoxe

Les programmes relatifs aux calculs par la théorie orthodoxe sont rassemblés dans le dossier [TheoOrth](https://gitlab.com/sens_simulator/SENS/-/tree/main/TheoOrth).
Les scripts [CapaOrthodoxe.m](https://gitlab.com/sens_simulator/SENS/-/blob/main/TheoOrth/CapaOrthodoxe.m) et [SETOrthodoxe.m](https://gitlab.com/sens_simulator/SENS/-/blob/main/TheoOrth/SETOrthodoxe.m) permettent d’obtenir les caractéristiques courant-tension, respectivement, d’une capacité à 1 dot, et d’un SET (Figure 1), calculées par la théorie orthodoxe [1-3 - AJOUTER LES REF.]. Il suffit d’indiquer au début du script les résistances et capacités des jonctions tunnel ainsi que la température. Les résultats sont affichés mais pas sauvegardés.
Le calcul pour les dispositifs à 2 dots reste à implémenter. Des expressions utiles sont données dans [4].
<p align="center">
  <img src="Annexe/SET_ortho.png"  width="350" height="350"/>
  <Figure 1. Exemple de diagramme en diamants obtenu pour un SET par la théorie orthodoxe avec R1 = 10 MΩ, R2 = 1 MΩ, C1 = 1 aF, C2 = 0.01 aF, Cg = 1 aF, T = 30 K>
</p>


### 2. 2. Le simulateur interactif

Description à venir

## 3. Dispositifs à boîtes quantiques semi-conductrices

### 3. 1. Capacités quantiques

Travail effectué par J. Sée, puis V. Talbo pour la partie bruit. 

#### 3. 1. 1. Structure électronique
#### 3. 1. 2. Calculs du courant
#### 3. 1. 3. Calculs du bruit

### 3. 2. Capacités à deux îlots

Travail effectué par A. Valentin.

### 3. 3. Transistor à un électron (SET) à îlot Silicium

Travail effectué par V. Talbo. Les fichiers se trouvent dans le dossier [SET](https://gitlab.com/sens_simulator/SENS/-/tree/main/SET).

#### 3. 3. 1. Structure électronique du Silicium
Les programmes permettant le calcul de la structure électronique des boîtes quantiques polarisées sont en langage C. Il s’agit d’un programme principal Hschpoi3D.c appelant plusieurs sous programmes. Il faut donc, pour compiler ces programmes, taper : _gcc –lm Hschpoi3D.c –o NomExecutable_
L’option _–lm_, indispensable à la compilation, permet de lier les bibliothèques à tous les sous-programmes.

La structure électronique de la boîte quantique polarisée est calculée par résolution des équations couplées de Schrödinger et Poisson à l’aide de la méthode de Hartree. L’équation de Schrödinger est résolue dans le script schrod.c qui appelle le script [eigval.c](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Elec_Struc/eigval.c) pour la résolution de l’équation aux valeurs propres. L’équation de Poisson pour la détermination de la polarisation du nanocristal puis pour l’équation de Poisson sans charge, couplée à l’équation de Schrödinger, est résolue dans le script [Hpoiss3D.c](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Elec_Struc/Hpoiss3D.c). Enfin, la plupart des tableaux créés pour ces résolutions sont initialisés dans les programmes contenus dans [matdiv.c](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Elec_Struc/matdiv.c).

##### 3. 3. 1. 1. Données d'entrée
L’essentiel des paramètres nécessaires aux calculs sont entrées directement dans les scripts Hschpoi3D.c (où on trouve essentiellement les paramètres contrôlant la simulation) et matdiv.c (où on trouve les données matériaux), ou par des fichiers batch : 
Dans [Hschpoi3D](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Elec_Struc/Hschpoi3D.c) : 

- Nptaxe : nombre de points de maillages suivant l’axe source-drain: il sera adapté dans l’axe dot-grille 

- TOL : tolérance pour la résolution de l’équation de Poisson - N’EST PLUS UTILISÉ

- Nbitermaxpoi : nombre d’itérations max pour la résolution de l’équation de Poisson. Rarement modifié

- Nbiterschropoi : nombre d’itérations max pour la résolution des équations couplées de Poisson et Schrödinger. Rarement modifié

- Vmin (Vmax): valeur minimale (maximale) de V_DS

- Vgmin (Vgmax) : valeur minimale (maximale) de V_GS

- pasV (pasVg) : pas de tension entre deux calculs V_DS (V_GS) 

- hmin, hmax, hg : épaisseur de barrière côté source, drain et grille respectivement (en Angstrom / abohr)

- a : diamètre du dot (en Angstrom/(2*abohr))

- Nemin, Nelec : nombre minimal (généralement, 1) et maximal d’électrons dans l' îlot.

Dans [matdiv.c](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Elec_Struc/matdiv.c) :


- msi, msio2 : masse effective du Silicium, de la Silice (0.27/0.5, à ne pas modifier)
- esi, esio2 : permittivité du Silicium, de la Silice (11.7/3.8, à ne pas modifier)

Dans le dossier, un fichier batch appelé [send_jobs_many_Vg_batch](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Elec_Struc/send_jobs_many_Vg_batch) permet de créer plusieurs exécutables, à une tension de grille fixe, afin de paralléliser les opérations. Ce fichier crée des dossiers séparés pour différents Vg précisés dans le fichier, et de lancer les jobs _“work.work”_ via la commande _qsub_. Il convient de voir avec l’administrateur réseau comment s’organiser. 

##### 3. 3. 1. 2. Fichiers de sortie
Le programme génère plusieurs fichiers de sortie :

- dispo.txt : récapitulatif de toutes les données d’entrée: nombre de points suivants V_DS, nombre de point suivant V_GS, diamètre du dot, épaisseur de barrière gauche, droite, de grille, nombre d’électrons max, nombre de points de polarisations V_DS, nombre de points de polarisations V_GS, liste de toutes les polarisations V_DS, liste de toutes les polarisations V_GS

- schpoiLDA_a%diametre%.txt : log du programme

- E/e_XXXXXXXX.txt  : énergies (1ere ligne: énergie d’un électron, 2e ligne énergie totale, on s’intéresse à la première ligne) 

- Psi/psi_XXXXXXX.txt : fonctions d’onde de l’électron dans la boîte quantique. 
- Vpol/Vpol_XXXXXX.txt : potentiel de polarisation (sans électron) dans la structure. dans un cube Nptaxe ^ 3
- Vpol2/Vpol2_XXXXXX.txt : potentiel de polarisation (sans électron) dans la structure. dans un cube Nptaxe^2 x - Npty2 (nombre de points selon l’axe dot-grille) 
- axe/axex(y,y2,z)_a%2.0f.txt : coordonnées du maillage suivant l’axe Ox (drain-source), Oy (dot-grille, limité à Nptaxe points pour axey, Npty2 pour axey2) et Oz 
- d’autres fichiers peuvent être créés, il suffit de décommenter les lignes correspondantes, mais ces fichiers ne sont pas nécessaires pour les calculs suivants

Si les calculs ont été effectués via le fichier batch présenté dans la section précédente, vous pouvez regrouper tous les fichiers obtenus grâce à _move_output_files_batch_.

##### 3. 3. 1. 3. Préparation des données pour Matlab

Une fois les calculs de la structure électronique effectués, on a besoin de préparer les données pour qu’elles soient utilisables pour nos calculs et nos figures dans Matlab.
Pour cela, il faut utiliser le code Matlab [dispoSET.m](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Matlab/dispoSET.m) : il suffit d’indiquer le type de dispo dans la variable _typedispo_ (vous pouvez donner le nom que vous voulez, en général j’utilise la convention _diamètre - épaisseur d’oxyde source - drain - grille_) les dossiers où se trouvent les fichiers de sortie de calcul (variable _dossierd_), et le programme créera directement les fichiers nécessaires.  Les axes et différentes variables calculées sont interpolées linéairement pour avoir 2x plus de points par axe. 

_ATTENTION_: Avant de lancer le calcul, vous devrez modifier le fichier _dispo.txt_ dans le cas où vous avez fait les calculs pour différentes polarisations en plusieurs fois, pour intégrer tous vos points de polarisation (ex : vous souhaitez checker d’autres tensions de grille, ou vous avez lancé les codes via le fichier batch). 
En sortie, nous obtenons les fichiers :

- #typedispo.mat (avec typedispo le nom donné auparavant) contenant les informations contenues dans dispo.txt
- V.mat : les différentes tensions drain-source et grille considérées
- Axe.mat : les points de maillages suivant les différents axes
- Psi.mat : les fonctions d’onde - dans un tableau à 3 dimensions 
- Vpol.mat : les potentiels de polarisation, obtenus par le calcul de la structure électronique, ou calculée théoriquement selon J. Sée [Ajouter la référence dans la thèse]. 
- Energies.mat : Energie du dot, niveau de Fermi dans les électrodes, niveau du haut de la bande de conduction dans les électrodes, valeur de la polarisation à mi-barrière, hauteur de barrière. 

#### 3. 3. 2. Calculs des courants (électriques, de chaleur) dépendant de la température
Les courants électriques et de chaleur pour différentes tensions sont calculés dans les codes [Courant_discret.m](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Matlab/Courant_discret.m) ou [Courant_elargi.m](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Matlab/Courant_elargi.m), dans le cas de niveaux d’énergie discret (élargis) dans l’îlot. Le programme se charge de calculer les fonctions d’onde à mi-barrière (cette étape, pouvant être fastidieuse, enregistre les résultats : cela évite, dans le cas d’une petite modification du calcul, à avoir à les recalculer), les fréquences de transfert tunnel, puis les courants. 
En entrée, les variables à renseigner sont : 
- dispos : nom donné au dispositif
- T : température moyenne du dispositif
- dT : différence de température entre drain et source (dT = 0 pour un dispositif à température constante) 
- alpha (cas élargi) : élargissement des niveaux d’énergie ( x kBT)

En sortie les fichiers matlab suivant sont créés : 
- PSI_MIBAT.mat : les valeurs de la fonction d’onde de l’électron à mi-barrière tunnel 
- (élargi : A#alpha /) IMA_T#t_dT_#dt.mat avec #alpha l’élargissement du niveau, #t la température et #dt la différence de température : contient le courant électrique, les tensions drain et grille, les fréquences de transferts tunnel, la puissance, et les courants de chaleur (électrodes <-> dot) + (cas élargi) élargissement du niveau et énergies associées. Dans les fichiers Matlab, IMA correspond au courant électronique. 

#### 3. 3. 3. Propriétés thermoélectriques

Les propriétés thermoélectriques des SETs sont calculées dans [Calculs_ZT.m. ](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Matlab/Calculs_ZT.m)
En entrée, nous renseignons : 
- dispos : nom du dispo
- alpha : l’élargissement choisi. En pratique, il sert à se repérer dans le dossier où le fichier _IMA_Txxx.mat_ est stocké. le dossier est repéré la variable _dossierm_. 
T et dT : température et différence de température

En sortie, le fichier  _ZT_T#t.mat_ avec t la température globale contient :  
- la conductance Ge
- les tensions V et Vg
- les différences de température dT
- les coefficients Seebeck alpha
- les conductances thermiques à gauche et à droite
- le facteur de puissance(?) f 
- la figure de mérite thermoelectrique ZT
- la tension open-circuit (I=0) Voc
Le programme [rendement.m](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Matlab/rendement.m) permet de calculer et d’afficher les figures Puissance et rendements (divisé par le rendement max) en fonction de V et Vg. 
Le programme [coeff_IQS_I.m](https://gitlab.com/sens_simulator/SENS/-/blob/main/SET/Matlab/coeff_IQS_I.m) est un exemple de calcul de fit de la courbe du courant de chaleur IQS en fonction du courant électrique I par un polynôme de second degré. L’étude des coefficients récupérés ainsi constitue un approfondissement intéressant du sujet. 

