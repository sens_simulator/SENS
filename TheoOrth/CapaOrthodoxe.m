%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%|                             Script CapaOrthodoxe                       |
%|                                                                        |
%| Calcule le courant dans une capacité à 1 dot par la théorie orthodoxe  |
%| Entrer les capacités et résistances des jonctions tunnel, et la        |
%| température                                                            |
%|                                                                        |
%| Audrey Valentin, le 27/08/09 
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

clear
clc

%--------------------------------------------------------------------------
% Param�tres � modifier
R1=1e6;R2=10e6;C1=1e-18;C2=1e-18;
T=300

%--------------------------------------------------------------------------

disp(['C1= ',num2str(C1*1e18),' aF'])
disp(['C2= ',num2str(C2*1e18),' aF'])
disp(['R1= ',num2str(R1*1e-6),' MOhms'])
disp(['R2= ',num2str(R2*1e-6),' MOhms'])

% Constantes
qe=1.6e-19;kb=1.38e-23;
epsSiO2=3.8;eps0=8.8e-12;

% Initialisation
Ceq=C1+C2;
N=4;n=(-N:N)';
V=linspace(-3*qe/Ceq,3*qe/Ceq,100);
P0=zeros(length(n),1);P0(1)=1;
I=zeros(1,length(V));
st=5000;

tic
for iv=1:length(V),

    % Fr�quences de transfert
    DEgp=qe/(2*Ceq)*((2*n+1)*qe+2*V(iv)*C2);
    Gammagp=-DEgp./((qe^2*R1)*(1-exp(DEgp/(kb*T))));
    Gammagp(logical(DEgp==0))=0;

    DEgm=qe/(2*Ceq)*(-(2*n-1)*qe-2*V(iv)*C2);
    Gammagm=-DEgm./((qe^2*R1)*(1-exp(DEgm/(kb*T))));
    Gammagm(logical(DEgm==0))=0;

    DEdp=qe/(2*Ceq)*(-(2*n-1)*qe+2*V(iv)*C1);
    Gammadp=-DEdp./((qe^2*R2)*(1-exp(DEdp/(kb*T))));
    Gammadp(logical(DEdp==0))=0;

    DEdm=qe/(2*Ceq)*((2*n+1)*qe-2*V(iv)*C1);
    Gammadm=-DEdm./((qe^2*R2)*(1-exp(DEdm/(kb*T))));
    Gammadm(logical(DEdm==0))=0;

    Gt=Gammagp+Gammagm+Gammadp+Gammadm;

    % Courant
    in=N+1;
    if Gt(in)==0,
        I=0;
    else
        P=zeros(2*N+1,4);
        tvl=zeros(1,st);tvln=zeros(2*N+1,st);
        P(:,1)=Gammagp./Gt;P(:,2)=Gammagm./Gt;
        P(:,3)=Gammadp./Gt;P(:,4)=Gammadm./Gt;
        for it=1:st,
            X=rand(1);tvl(it)=-log(1-X)/Gt(in);Y=rand(1);
            if Y<P(in,1),
                tvln(in,it)=tvl(it);in=in+1;
            elseif Y<sum(P(in,1:2)),
                tvln(in,it)=tvl(it);in=in-1;
            elseif Y<sum(P(in,1:3)),
                tvln(in,it)=tvl(it);in=in-1;
            else
                tvln(in,it)=tvl(it);in=in+1;
            end
        end
        tf=sum(tvl(10:st));Pn=sum(tvln(:,10:st),2)/tf;
        I(iv)=-qe*sum(Pn.*(Gammadp-Gammadm));
    end
end

t=toc;
disp(['Temps �coul� : ',num2str(t),' s'])


figure(20)
clf
plot(V*1e3,I*1e9,'k','LineWidth',2);
xlabel('Vds (mV)','FontSize',20);
ylabel('I (nA)','FontSize',20);
set(gca,'FontSize',20);
grid on
