%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%|                             Script SETOrthodoxe                        |
%|                                                                        |
%| Calcule le courant dans un SET par la théorie orthodoxe                |
%| Entrer les capacités et résistances des jonctions tunnel, et la        |
%| température                                                            |
%|                                                                        |
%| Audrey Valentin, le 27/08/09                                           |
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

clear
clc

%--------------------------------------------------------------------------
% Param�tres � modifier
R1=1e6;R2=10e6;C1=1e-19;C2=1e-19;Cg=1e-19;
T=30;
%--------------------------------------------------------------------------

disp(['Cg= ',num2str(Cg*1e18),' aF'])
disp(['C1= ',num2str(C1*1e18),' aF'])
disp(['C2= ',num2str(C2*1e18),' aF'])
disp(['R1= ',num2str(R1*1e-6),' MOhms'])
disp(['R2= ',num2str(R2*1e-6),' MOhms'])
disp(['T= ',num2str(T),' K'])

% Constantes
qe=1.6e-19;kb=1.38e-23;
epsSiO2=3.8;eps0=8.8e-12;

% Initialisation
Ceq=C1+C2+Cg;
N=3;n=(-N:N)';

% Definition des tensions de grille
Vgs=linspace(-1.5*qe/Cg,1.5*qe/Cg,100);
% Definition des tensions drain source
V=linspace(-2*qe/Ceq,2*qe/Ceq,100);


P0=zeros(length(n),1);P0(1)=1;
I=zeros(length(V),length(Vgs));
st=5000;

tic
for iv=1:length(V),
    for ivg=1:length(Vgs),

        % Fr�quences de transfert
        DEgp=qe/(2*Ceq)*((2*n+1)*qe-2*Cg*Vgs(ivg)+2*V(iv)*(C2+Cg));
        Gammagp=-DEgp./((qe^2*R1)*(1-exp(DEgp/(kb*T))));
        Gammagp(logical(DEgp==0))=0;

        DEgm=qe/(2*Ceq)*(-(2*n-1)*qe+2*Cg*Vgs(ivg)-2*V(iv)*(C2+Cg));
        Gammagm=-DEgm./((qe^2*R1)*(1-exp(DEgm/(kb*T))));
        Gammagm(logical(DEgm==0))=0;

        DEdp=qe/(2*Ceq)*(-(2*n-1)*qe+2*Cg*Vgs(ivg)+2*V(iv)*C1);
        Gammadp=-DEdp./((qe^2*R2)*(1-exp(DEdp/(kb*T))));
        Gammadp(logical(DEdp==0))=0;

        DEdm=qe/(2*Ceq)*((2*n+1)*qe-2*Cg*Vgs(ivg)-2*V(iv)*C1);
        Gammadm=-DEdm./((qe^2*R2)*(1-exp(DEdm/(kb*T))));
        Gammadm(logical(DEdm==0))=0;

        Gt=Gammagp+Gammagm+Gammadp+Gammadm;

        % Calcul du courant par Monte-Carlo
        in=N+1;
        if Gt(in)==0,
            I=0;
        else
            P=zeros(2*N+1,4);
            tvl=zeros(1,st);tvln=zeros(2*N+1,st);
            P(:,1)=Gammagp./Gt;P(:,2)=Gammagm./Gt;
            P(:,3)=Gammadp./Gt;P(:,4)=Gammadm./Gt;
            for it=1:st,
              % Tirage au sort d'un temps de vol entre deux evenements tunnels
                X=rand(1);
                tvl(it)=-log(1-X)/Gt(in);
                %Tirage au sort de l'évènement tunnel entre Gd, dG, dD, Dd
                Y=rand(1);
                if Y<P(in,1),
                    tvln(in,it)=tvl(it);in=in+1;
                elseif Y<sum(P(in,1:2)),
                    tvln(in,it)=tvl(it);in=in-1;
                elseif Y<sum(P(in,1:3)),
                    tvln(in,it)=tvl(it);in=in-1;
                else
                    tvln(in,it)=tvl(it);in=in+1;
                end
            end
            tf=sum(tvl(10:st));Pn=sum(tvln(:,10:st),2)/tf;
            I(iv,ivg)=-qe*sum(Pn.*(Gammadp-Gammadm));
        end
    end
end

Vgs2=linspace(Vgs(1),Vgs(end),100);
V2=linspace(V(1),V(end),100);
[VV2,VVgs2]=meshgrid(V2,Vgs2);[VV,VVgs]=meshgrid(V,Vgs);
I2=interp2(VV,VVgs,I,VV2,VVgs2);
I=I2;V=V2;Vgs=Vgs2;
t=toc;
disp(['Temps �coul� : ',num2str(t),' s'])


figure(30)
clf
surf(V,Vgs,I','EdgeColor','none');
xlabel('Vds (V)','FontSize',20);
ylabel('Vgs (V)','FontSize',20);
set(gca,'FontSize',20);
axis square tight
shading flat

figure(31)
clf
pcolor(Vgs,V,I*1e9);
xlabel('Vgs (V)','FontSize',20);
ylabel('Vds (V)','FontSize',20);
set(gca,'FontSize',20);
axis  square tight
shading flat
colorbar('FontSize',20);
title('I (nA)','FontSize',20);
