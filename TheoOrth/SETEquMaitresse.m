%--------------------------------------------------------------------------
%|                             Script SETOrthodoxe                        |
%|                                                                        |
%| Calcule le courant dans un SET par la théorie orthodoxe                |
%| Entrer les capacités et résistances des jonctions tunnel, et la        |
%| température                                                            |
%|                                                                        |
%| Audrey Valentin, le 27/08/09                                           |
%|                                                                        |
%| Modification du script pour calculer les courants par l'équation       |
%| maîtresse (et non Monte Carlo)                                         |
%|                                                                        |
%| Gianina Frimu, le 29/07/21                                             |
%--------------------------------------------------------------------------

clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
R1=1e6; %Ohms Résistance tunnel à gauche
R2=10e6; %Ohms Résistance tunnel à droite
C1=1e-19; %F Capa à gauche
C2=1e-19; %F Capa à droite
Cg=1e-19; %F Capa côté grille
T=30; %température (partout)
tmax=10; %temps max pour le calcul par l'équation maîtresse
%--------------------------------------------------------------------------

disp(['Cg= ',num2str(Cg*1e18),' aF'])
disp(['C1= ',num2str(C1*1e18),' aF'])
disp(['C2= ',num2str(C2*1e18),' aF'])
disp(['R1= ',num2str(R1*1e-6),' MOhms'])
disp(['R2= ',num2str(R2*1e-6),' MOhms'])
disp(['T= ',num2str(T),' K'])

% Constantes
qe=1.6e-19;kb=1.38e-23;
epsSiO2=3.8;eps0=8.8e-12;

% Initialisation
Ceq=C1+C2+Cg;
N=2; %nombre max d'électrons dans le dot
n=(-N:N)';

% Definition des tensions de grille
Vgs=linspace(-1.5*qe/Cg,1.5*qe/Cg,100); %V
% Definition des tensions drain source
V=linspace(-2*qe/Ceq,2*qe/Ceq,100); %V


P0=zeros(length(n),1); %P(t=0)
P0(ceil(length(n)/2))=1; % P0(n=0)=1
I=zeros(length(V),length(Vgs));

tic
for iv=1:length(V),
    for ivg=1:length(Vgs),

        % Fréquences de transfert
        % gp:gauche plus, gm:gauche moins, dp:droite plus, dm:droite moins
        DEgp=qe/(2*Ceq)*((2*n+1)*qe-2*Cg*Vgs(ivg)+2*V(iv)*(C2+Cg)); %J
        Gammagp=-DEgp./((qe^2*R1)*(1-exp(DEgp/(kb*T)))); %Hz
        Gammagp(logical(DEgp==0))=0;

        DEgm=qe/(2*Ceq)*(-(2*n-1)*qe+2*Cg*Vgs(ivg)-2*V(iv)*(C2+Cg));
        Gammagm=-DEgm./((qe^2*R1)*(1-exp(DEgm/(kb*T))));
        Gammagm(logical(DEgm==0))=0;

        DEdp=qe/(2*Ceq)*(-(2*n-1)*qe+2*Cg*Vgs(ivg)+2*V(iv)*C1);
        Gammadp=-DEdp./((qe^2*R2)*(1-exp(DEdp/(kb*T))));
        Gammadp(logical(DEdp==0))=0;

        DEdm=qe/(2*Ceq)*((2*n+1)*qe-2*Cg*Vgs(ivg)-2*V(iv)*C1);
        Gammadm=-DEdm./((qe^2*R2)*(1-exp(DEdm/(kb*T))));
        Gammadm(logical(DEdm==0))=0;

        Gt=Gammagp+Gammagm+Gammadp+Gammadm;

        % Calcul du courant par l'équation maîtresse
        
            %Creation de la matrice GAMMA
            GAMMA=zeros(length(n));
            for ii=1:length(GAMMA),
                GAMMA(ii,ii)=-Gt(ii);
            end
            for ii=1:length(n)-1,
                GAMMA(ii,ii+1)=Gammadp(ii+1)+Gammagm(ii+1);
                GAMMA(ii+1,ii)=Gammadm(ii)+Gammagp(ii);
            end

            %Equation maîtresse
            P0=zeros(length(n),1);P0(1,:)=1;
            GAMMAt=GAMMA.*tmax;
            PI=expm(GAMMAt)*P0;
            
            I(iv,ivg)=-qe*sum(PI.*(Gammadp-Gammadm));
        end
end

Vgs2=linspace(Vgs(1),Vgs(end),100);
V2=linspace(V(1),V(end),100);
[VV2,VVgs2]=meshgrid(V2,Vgs2);[VV,VVgs]=meshgrid(V,Vgs);
I2=interp2(VV,VVgs,I,VV2,VVgs2);
I=I2;V=V2;Vgs=Vgs2;
t=toc;
disp(['Temps écoulé : ',num2str(t),' s'])


figure(30)
clf
surf(V,Vgs,I','EdgeColor','none');
xlabel('Vds (V)','FontSize',20);
ylabel('Vgs (V)','FontSize',20);
set(gca,'FontSize',20);
axis square tight
shading flat

figure(31)
clf
pcolor(Vgs,V,I*1e9);
xlabel('Vgs (V)','FontSize',20);
ylabel('Vds (V)','FontSize',20);
set(gca,'FontSize',20);
axis  square tight
shading flat
colorbar('FontSize',20);
title('I (nA)','FontSize',20);

[Z,iv]=min(abs(V-15e-3)); %V(iv)=15mV

figure(32)
plot(Vgs,I(iv,:)*1e9,'-b','LineWidth',1)
hold on
plot(Vgs(1:5:end),I(iv,1:5:end)*1e9,'ob','MarkerSize',4,'LineWidth',1)
xlabel('gate voltage (V)','FontSize',15,'FontName','Avenir');
ylabel('electronic current (nA)','FontSize',15,'FontName','Avenir');
%title('Conductance peaks','FontName','Avenir','FontSize',20)
