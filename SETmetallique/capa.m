
function C=capa(r,d,eps) %Shallal
eps0=8.8e-12; %F/m
syms k;
a=1+sqrt(1+2*r/d);
b=-1+sqrt(1+2*r/d);
eta=log(a/b);
c=8*pi*eps0*eps*(d+r)*symsum(1/(exp((2*k+1)*eta)-1),k,1,Inf);
C=double(c);
end