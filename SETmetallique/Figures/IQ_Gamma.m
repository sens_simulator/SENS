% IQ et fréquences de transitions en circuit ouvert

clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m diamètre si sphérique ou taille de l'arrête si cubique
hG=1.2e-9; %m épaisseur de l'oxyde de source
hD=1.5e-9; %m épaisseur de l'oxyde de drain
hg=5e-9; %m épaisseur de l'oxyde de grille
T = 100; %K % Témperature du dot
deltaT = 5; %K gradient de température

% dossier où se trouvent les scripts pour les calculs de courant
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';
%--------------------------------------------------------------------------

% Chargement des fichiers utiles
typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];

load([dossier,typedispo,'/discret/ZT_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(deltaT),'.mat']); 

IQGoc=zeros(length(Vgs),1);
GammaGdoc=zeros(length(Vgs),2);
GammadGoc=zeros(length(Vgs),2);
GammaDdoc=zeros(length(Vgs),2);
GammadDoc=zeros(length(Vgs),2);

for ivg=1:length(Vgs)
    IQGoc(ivg)=interp1(V*1e3,IQG(:,ivg),-Voc(ivg),'linear'); %W
    GammaGdoc(ivg,:)=interp1(V*1e3,GammaGd(:,ivg,4:5),-Voc(ivg),'pchip'); %Hz
    GammadGoc(ivg,:)=interp1(V*1e3,GammadG(:,ivg,5:6),-Voc(ivg),'pchip');
    GammaDdoc(ivg,:)=interp1(V*1e3,GammaDd(:,ivg,4:5),-Voc(ivg),'pchip');
    GammadDoc(ivg,:)=interp1(V*1e3,GammadD(:,ivg,5:6),-Voc(ivg),'pchip');
end

V2=linspace(V(1),V(end),1e3);
Vgs2=linspace(Vgs(1),Vgs(end),1e3);
IQGoc2=interp1(Vgs,IQGoc,Vgs2,'pchip');
GGdoc=interp1(Vgs,GammaGdoc(:,1:2),Vgs2,'pchip');
GdGoc=interp1(Vgs,GammadGoc(:,1:2),Vgs2,'pchip');
GDdoc=interp1(Vgs,GammaDdoc(:,1:2),Vgs2,'pchip');
GdDoc=interp1(Vgs,GammadDoc(:,1:2),Vgs2,'pchip');

% FigureFormat(6,6,16,2);
%subplot(2,1,1)
%subplot('Position',[0.1 0.5 0.80 0.45])
subplot('Position',[0.1 0.5 0.80 0.4])
L(1)=plot(Vgs2,IQGoc2*1e12,'k','LineWidth',2,'DisplayName','I_Q^S (I = 0)');
set(gca,'XTickLabel',{})
xlim([0 13])
ylim([0 3.5])
ylabel('heat current (pW)','FontName','Latin Modern Roman','FontSize',16)
legend(L)
legend('Location','north','FontName','Latin Modern Roman','FontSize',12)
legend boxoff



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.1 0.1 0.80 0.4])
%subplot('Position',[0.1 0.05 0.80 0.45])
%subplot(2,1,2)
semilogy(Vgs2,GGdoc(:,1),'Color',[0 0.4470 0.7410],'LineWidth',2)
hold on
semilogy(Vgs2,GGdoc(:,2),'--','Color',[0 0.4470 0.7410],'LineWidth',2)
semilogy(Vgs2,GdGoc(:,1),'r','LineWidth',2)
semilogy(Vgs2,GdGoc(:,2),'r--','LineWidth',2)
semilogy(Vgs2,GDdoc(:,1),'k','LineWidth',2)
semilogy(Vgs2,GDdoc(:,2),'k--','LineWidth',2)
semilogy(Vgs2,GdDoc(:,1),'Color',[0 0.5 0],'LineWidth',2)
semilogy(Vgs2,GdDoc(:,2),'--','Color',[0 0.5 0],'LineWidth',2)

% G(1)=semilogy(Vgs2,GGdoc(:,2),'-','Color',[0 0.4470 0.7410],'LineWidth',2,'DisplayName','\Gamma_{Sd}');
% G(2)=semilogy(Vgs2,GdGoc(:,1),'-r','LineWidth',2,'DisplayName','\Gamma_{dS}');
% G(3)=semilogy(Vgs2,GDdoc(:,1),'-k','LineWidth',2,'DisplayName','\Gamma_{Dd}');
% G(4)=semilogy(Vgs2,GdDoc(:,1),'-','Color',[0 0.5 0],'LineWidth',2,'DisplayName','\Gamma_{dD}');

%G(1)=semilogy(Vgs2,GGdoc(:,2),'-','Color',[.5 0 .5],'LineWidth',2,'DisplayName','\Gamma (0 \leftrightarrow 1)');
%G(2)=semilogy(Vgs2,GdGoc(:,2),'--','Color',[.5 0 .5],'LineWidth',2,'DisplayName','\Gamma (1 \leftrightarrow 2)');
xlabel('gate voltage (V)','FontName','Latin Modern Roman','FontSize',16)
ylabel('transfer rates (Hz)','FontName','Latin Modern Roman','FontSize',16)
ylim([1e8 1e13])
xlim([0 13])
%legend(G)
%legend boxoff
%legend('FontSize',12,'FontName','Latin Modern Roman','Location','southwest')

