% Caractéristiques I-Vgs à un Vds donné

clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m diamètre si sphérique ou taille de l'arrête si cubique
hG=1.2e-9; %m épaisseur de l'oxyde de source
hD=1.5e-9; %m épaisseur de l'oxyde de drain
hg=5e-9; %m épaisseur de l'oxyde de grille
T = 100; %K % Témperature du dot
dT = 5; %K gradient de température
VDS=5e-3 %V
% dossier où se trouvent les scripts pour les calculs de courants
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';

%--------------------------------------------------------------------------
typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(dT),'.mat'])

Vgs2=linspace(Vgs(1),Vgs(end),1000);
V2=linspace(V(1),V(end),1000);
[VV2,VVgs2]=meshgrid(V2,Vgs2);[VV,VVgs]=meshgrid(V,Vgs);
I2=interp2(VV,VVgs,I,VV2,VVgs2);
%I=I2;V=V2;Vgs=Vgs2;

[Z,iv]=min(abs(V2-VDS));
figure(32)
plot(Vgs2,I2(iv,:)*1e9,'-b','LineWidth',1)
hold on
plot(Vgs2(1:end),I2(iv,1:end)*1e9,'ob','MarkerSize',4,'LineWidth',1)
xlabel('gate voltage (V)','FontSize',15,'FontName','Avenir');
ylabel('electronic current (nA)','FontSize',15,'FontName','Avenir');
%title('Conductance peaks','FontName','Avenir','FontSize',20)