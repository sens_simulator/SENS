% Puissance et rendement

clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m diamètre si sphérique ou taille de l'arrête si cubique
hG=1.2e-9; %m épaisseur de l'oxyde de source
hD=1.5e-9; %m épaisseur de l'oxyde de drain
hg=5e-9; %m épaisseur de l'oxyde de grille
T = 100; %K % Témperature du dot
deltaT = 5; %K gradient de température (3 deltaT différents)

% dossier où se trouvent les scripts pour les calculs de courant
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';
%--------------------------------------------------------------------------

% Chargement des fichiers utiles
typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];

load([dossier,typedispo,'/discret/ZT_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossier,typedispo,'/discret/Coeff_IQfit_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(deltaT),'.mat']); 

% Calcul des différentes puissances à comparer
for iv=1:length(V)
    for ivg=1:length(Vgs)
        Puiss(iv,ivg)=V(iv)*I(iv,ivg)*1e12; %pW
        Puiss_Joule(iv,ivg)=(I(iv,ivg)^2)/(Ge(iv,ivg)*1e-6)*1e12; %pW
        Puiss_Seebeck(iv,ivg)=(-Voc(ivg)*1e-3)*I(iv,ivg)*1e12; %pW
        dIQ(iv,ivg)=(IQD(iv,ivg)-IQG(iv,ivg))*1e12; %pW
        dIQ_poly(iv,ivg)=IQD_poly(iv,ivg)-IQG_poly(iv,ivg); %pW
    end
end

Puiss_fournie=-Puiss; Puiss_fournie(Puiss_fournie<0)=NaN; %pW
eta=(Puiss_fournie*1e-12)./IQG; 
etaC=1-TD/TG;
eff_norm=eta./etaC;

% efficacité maximale
eff_max=max(max(eff_norm));
disp(['efficacité maximale: ',num2str(eff_max)])

% puissance maximale
[Ml,Il]=max(Puiss_fournie);
[Mc,Ic]=max(Ml);
max_P=Puiss_fournie(Il(Ic),Ic);
eff_Pmax=eff_norm(Il(Ic),Ic);
disp(['Puissance maximale: ',num2str(max_P*1e3),' fW'])
disp(['efficacité à la puissance max: ',num2str(eff_Pmax)])

%%
V2=linspace(V(1),V(end),2e3);
Vgs2=linspace(Vgs(1),Vgs(end),2e3);
[VV2,VVgs2]=meshgrid(V2,Vgs2);[VV,VVgs]=meshgrid(V,Vgs);
Puiss2=interp2(VV,VVgs,Puiss_fournie,VV2,VVgs2,'linear');
eff2=interp2(VV,VVgs,eff_norm,VV2,VVgs2,'linear');

[Z,iv]=min(abs(V2));
DEGd=1/(2*Ceq).*((2*n+1)*qe-2*Cg.*Vgs2+2*V2(iv)*(CD+Cg)); %eV
Vg1=interp1(DEGd(3,:),Vgs2,0);
Vg2=interp1(DEGd(4,:),Vgs2,0);

subplot(1,2,1)
%subplot('Position',[0.1 0.5 0.80 0.4])
pcolor(V2*1e3,Vgs2,Puiss2'*1e3);
hold on
plot([V2(1) V2(end)]*1e3,[Vg1 Vg1],'k--','LineWidth',1)
plot([V2(1) V2(end)]*1e3,[Vg2 Vg2],'k--','LineWidth',1)
plot(-Voc,Vgs,'r--','LineWidth',2)
%xlabel('drain voltage (mV)','FontSize',20,'FontName','Latin Modern Roman');
%set(gca,'XTickLabel',{})
xlabel('drain voltage (mV)','FontSize',12,'FontName','Latin Modern Roman');
ylabel('gate voltage (V)','FontSize',12,'FontName','Latin Modern Roman');
set(gca,'FontName','Latin Modern Roman','FontSize',17);
colormap('jet')
axis  square tight
shading flat
ylabel(colorbar,'generated power (fW)')
ylim([0 10])


subplot(1,2,2)
%subplot('Position',[0.1 0.1 0.80 0.4])
pcolor(V2*1e3,Vgs2,eff2');
hold on
plot([V2(1) V2(end)]*1e3,[Vg1 Vg1],'k--','LineWidth',1)
plot([V2(1) V2(end)]*1e3,[Vg2 Vg2],'k--','LineWidth',1)
plot(-Voc,Vgs,'r--','LineWidth',2)
xlabel('drain voltage (mV)','FontSize',12,'FontName','Latin Modern Roman');
ylabel('gate voltage (V)','FontSize',12,'FontName','Latin Modern Roman');
set(gca,'FontSize',17,'FontName','Latin Modern Roman');
axis  square tight
shading flat
colormap('jet')
ylabel(colorbar,'efficiency/Carnot efficiency')
ylim([0 10])
