% alpha en fonction de alpha idéal et comparaison entre alpha=Voc/dT et le
% alpha obtenu par régression quadratique (Peltier coeff)


clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m diamètre si sphérique ou taille de l'arrête si cubique
hG=1.2e-9; %m épaisseur de l'oxyde de source
hD=1.5e-9; %m épaisseur de l'oxyde de drain
hg=5e-9; %m épaisseur de l'oxyde de grille
T = 100; %K % Témperature du dot
deltaT = 5; %K gradient de température
VGS = 3.5; %V

% dossier où se trouvent les scripts pour les calculs de courant
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';
%--------------------------------------------------------------------------

% Chargement des fichiers utiles
typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];

load([dossier,typedispo,'/discret/ZT_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(deltaT),'.mat']); 
load([dossier,typedispo,'/discret/Coeff_IQfit_T',num2str(T),'dT',num2str(deltaT),'.mat']); 

V2=linspace(V(1),V(end),1e3);
Vgs2=linspace(Vgs(1),Vgs(end),1e3);
alpha_id2=interp1(Vgs,alpha_ideal,Vgs2,'pchip');
alpha2=interp1(Vgs,alpha,Vgs2,'pchip');
Peltier2=interp1(Vgs,Peltier,Vgs2,'pchip');


figure(2)
plot(alpha_id2,alpha2,'b','LineWidth',2);
hold on
plot(alpha_id2(1:10:end),alpha2(1:10:end),'^b','LineWidth',1);
plot(alpha_id2,alpha_id2,':k','LineWidth',1);
xlabel('ideal Seebeck (mV/K)','FontName','Latin Modern Roman','FontSize',14)
ylabel('Seebeck coefficient (mV/K)','FontName','Latin Modern Roman','FontSize',14)
xlim([-1.25 1.25])

figure(3)
plot(Vgs2,alpha2,'r','LineWidth',2)
hold on
plot(Vgs2(1:10:end),Peltier2(1:10:end),'xb','MarkerSize',6,'LineWidth',2)
xlim([0 13])
xlabel('gate voltage (V)','FontName','Latin Modern Roman','FontSize',14)
ylabel('(mV/K)','FontName','Latin Modern Roman','FontSize',14)
legend('Seebeck coefficient','Peltier coefficient','FontName','Latin Modern Roman','FontSize',14)
legend boxoff
legend('Location','northwest')




