% Caractéristiques IQ-I, à un Vgs donné

clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m        diamètre du dot
hG=1.2e-9; %m       épaisseur de l'oxyde côté source
hD=1.5e-9; %m       épaisseur de l'oxyde côté drain
hg=5e-9; %m         épaisseur de l'oxyde de grille
T =100; %K %        Témperature du dot
deltaT = 5; %K  Gradient de température TG-TD
VGS = 6; %V      Tension de grille

% dossier où se trouvent les scripts pour les calculs de courant
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';
%--------------------------------------------------------------------------

% Chargement du courant pour dT=0K
typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(deltaT),'.mat']);

% Extraction des courants pour Vgs=VGS
[Z,ivg]=min(abs(Vgs-VGS)); % ivg tq Vgs(ivg)=VGS

%--------------------------------------------------------------------------
clf
plot(I(:,ivg)*1e9,IQG(:,ivg)*1e12,'r','LineWidth',2);
hold on
G(1)=plot(I(1:10:end,ivg)*1e9,IQG(1:10:end,ivg)*1e12,'-^r','MarkerSize',5,'LineWidth',1,'DisplayName','I_Q^S');
plot(I(:,ivg)*1e9,IQD(:,ivg)*1e12,'b','LineWidth',2);
G(2)=plot(I(1:10:end,ivg)*1e9,IQD(1:10:end,ivg)*1e12,'-ob','MarkerSize',5,'LineWidth',1,'DisplayName','I_Q^D');


xlabel('electronic current (nA)','FontSize',20,'FontName','Latin Modern Roman');
ylabel('heat current (pW)','FontSize',20,'FontName','Latin Modern Roman');
%title(['I-Vds pour Vgs=',num2str(VGS),'V']);
%xlim([-5 5])
legend(G)
legend boxoff
legend('Location','northwest')
set(gca,'FontSize',20);
ax=gca;
ax.FontName='Latin Modern Roman';
grid on