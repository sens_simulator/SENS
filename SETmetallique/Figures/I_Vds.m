% Caractéristiques I-Vds pour différents dT, à un Vgs donné

clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m        diamètre du dot
hG=1.2e-9; %m       épaisseur de l'oxyde côté source
hD=1.5e-9; %m       épaisseur de l'oxyde côté drain
hg=5e-9; %m         épaisseur de l'oxyde de grille
T =100; %K %        Témperature du dot
deltaT = [0 5 10]; %K  Gradient de température TG-TD
VGS = 3.5; %V      Tension de grille

% dossier où se trouvent les scripts pour les calculs de courant
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';
%--------------------------------------------------------------------------

% Chargement du courant pour dT=0K
typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT0.mat']);

% Vecteur contenant les courants pour les différents dT
IT=zeros(length(deltaT),length(V));

% Extraction des courants pour Vgs=VGS
[Z,ivg]=min(abs(Vgs-VGS)); % ivg tq Vgs(ivg)=VGS

for iT=1:length(deltaT)
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(deltaT(iT)),'.mat'])
IT(iT,:)=I(:,ivg);
end

%--------------------------------------------------------------------------
clf

hold on
G(1)=plot(V(1:2:end)*1e3,IT(1,1:2:end)*1e9,'-bo','LineWidth',2,'MarkerSize',4,'DisplayName',['\DeltaT=',num2str(deltaT(1)),'K']);

hold on
G(2)=plot(V(1:2:end)*1e3,IT(2,1:2:end)*1e9,'-rx','LineWidth',2,'MarkerSize',6,'DisplayName',['\DeltaT=',num2str(deltaT(2)),'K']);

hold on
G(3)=plot(V(1:2:end)*1e3,IT(3,1:2:end)*1e9,'-o','Color',[0 0.5 0],'LineWidth',2,'MarkerSize',4,'DisplayName',['\DeltaT=',num2str(deltaT(3)),'K']);

plot([0,0],[-0.3,0.3],'k--','LineWidth',2)
plot([-1,1],[0,0],'k--','LineWidth',2)


x1=[-1 0 0 -1];y1=[0 0 0.3 0.3];
x2=[0 1 1 0];y2=[0 0 0.3 0.3];
x3=[-1 0 0 -1];y3=[-0.3 -0.3 0 0];
x4=[0 1 1 0];y4=[-0.3 -0.3 0 0];

patch(x1,y1,'g','FaceAlpha',0.1)
patch(x2,y2,'r','FaceAlpha',0.1)
patch(x3,y3,'b','FaceAlpha',0.1)
patch(x4,y4,'g','FaceAlpha',0.1)

xlabel('drain voltage (mV)','FontSize',20,'FontName','Latin Modern Roman');
ylabel('electronic current (nA)','FontSize',20,'FontName','Latin Modern Roman');
%title(['I-Vds pour Vgs=',num2str(VGS),'V']);
xlim([-1 1])
legend(G)
legend('FontName','Latin Modern Roman','Location','northwest')
legend boxoff
set(gca,'FontSize',20);
ax=gca;
ax.FontName='Latin Modern Roman';
grid off