% alpha et Ge en fonction de Vgs

clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m diamètre si sphérique ou taille de l'arrête si cubique
hG=1.2e-9; %m épaisseur de l'oxyde de source
hD=1.5e-9; %m épaisseur de l'oxyde de drain
hg=5e-9; %m épaisseur de l'oxyde de grille
T = 100; %K % Témperature du dot
deltaT = 5; %K gradient de température

% dossier où se trouvent les scripts pour les calculs de courant
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';
%--------------------------------------------------------------------------

% Chargement des fichiers utiles
typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];

load([dossier,typedispo,'/discret/ZT_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(deltaT),'.mat']); 

[Z,iv]=min(abs(V));
DEGd=1/(2*Ceq).*((2*n+1)*qe-2*Cg.*Vgs+2*V(iv)*(CD+Cg)); %eV
Vg1=interp1(DEGd(3,:),Vgs,0);
Vg2=interp1(DEGd(4,:),Vgs,0);

V2=linspace(V(1),V(end),1e3);
Vgs2=linspace(Vgs(1),Vgs(end),1e3);
alpha2=interp1(Vgs,alpha,Vgs2,'pchip');
Ge2=interp1(Vgs,Ge(iv,:),Vgs2,'pchip');

% FigureFormat(6,6,16,2);
%subplot(2,1,1)
%subplot('Position',[0.1 0.5 0.80 0.45])
subplot('Position',[0.1 0.5 0.80 0.4])
plot(Vgs2,Ge2,'r','LineWidth',2,'DisplayName','I_Q^S (I = 0)');
hold on
plot([Vg1 Vg1],[0 0.25],'k--','LineWidth',1)
plot([Vg2 Vg2],[0 0.25],'k--','LineWidth',1)
xlim([0 13])
set(gca,'XTickLabel',{})
ylabel('electronic conductance (\muS)','FontName','Latin Modern Roman','FontSize',14)




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('Position',[0.1 0.1 0.80 0.4])
%subplot('Position',[0.1 0.05 0.80 0.45])
%subplot(2,1,2)
plot(Vgs2,alpha2,'b','LineWidth',2,'DisplayName','I_Q^S (I = 0)');
hold on
plot([Vg1 Vg1],[-1.5 1.5],'k--','LineWidth',1)
plot([Vg2 Vg2],[-1.5 1.5],'k--','LineWidth',1)
plot([Vgs(1) Vgs(end)],[0 0],'k:','LineWidth',1)
xlim([0 13])
xlabel('gate voltage (V)','FontName','Latin Modern Roman','FontSize',14)
ylabel('Seebeck coefficient (mV/K)','FontName','Latin Modern Roman','FontSize',14)
%legend(G)
%legend boxoff
%legend('FontSize',12,'FontName','Latin Modern Roman','Location','southwest')

