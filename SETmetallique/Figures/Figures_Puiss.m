% quelques figures pour comparer les différentes puissances entre elles

clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m diamètre si sphérique ou taille de l'arrête si cubique
hG=1.2e-9; %m épaisseur de l'oxyde de source
hD=1.5e-9; %m épaisseur de l'oxyde de drain
hg=5e-9; %m épaisseur de l'oxyde de grille
T = 100; %K % Témperature du dot
deltaT = 5; %K gradient de température (3 deltaT différents)
VGS = 3.5; %V

% dossier où se trouvent les scripts pour les calculs de courant
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';
%--------------------------------------------------------------------------

% Chargement des fichiers utiles
typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];

load([dossier,typedispo,'/discret/ZT_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossier,typedispo,'/discret/Coeff_IQfit_T',num2str(T),'dT',num2str(deltaT),'.mat']);
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(deltaT),'.mat']); 

% Calcul des différentes puissances à comparer
for iv=1:length(V)
    for ivg=1:length(Vgs)
        Puiss(iv,ivg)=V(iv)*I(iv,ivg)*1e12; %pW
        Puiss_Joule(iv,ivg)=(I(iv,ivg)^2)/(Ge(iv,ivg)*1e-6)*1e12; %pW
        Puiss_Seebeck(iv,ivg)=(-Voc(ivg)*1e-3)*I(iv,ivg)*1e12; %pW
        dIQ(iv,ivg)=(IQD(iv,ivg)-IQG(iv,ivg))*1e12; %pW
        dIQ_poly(iv,ivg)=IQD_poly(iv,ivg)-IQG_poly(iv,ivg); %pW
    end
end

%% Ge & beta

[Z,iv]=min(abs(V)); %Vds=0V

% figure(1)
% plot(Vgs,betaD(1,:)-betaG(1,:),Vgs,1./Ge(iv,:));
% %semilogy(Vgs,betaD-betaS,Vgs,1./Ge(1,:));
% xlabel('Vgs (V)')
% ylabel('MOhms')
% legend('betaD-betaS','1/Ge')

% figure(2)
% plot(Vgs,(1./Ge(iv,:)-(betaD(1,:)-betaG(1,:)))./(1./Ge(iv,:)));
% xlabel('Vgs (V)')
% ylabel('(1/Ge-(betaD-betaS))/(1/Ge)')

[Z,iv]=min(abs(V-(5e-3))); %Vds=5mV


% figure
% semilogy(Vgs,betaS,Vgs,betaD,Vgs,betaD-betaS)
% legend('betaS','betaD','betaD-betaS')
% xlabel('Vgs (V)')
% ylabel('GOhm')

figure
plot(Vgs,abs(betaG./betaD))
xlabel('Vgs (V)')
ylabel('|betaG/betaD|')

%% Puissance vs IQD-IQS

VDS=2e-3;
[Z,iv]=min(abs(V-VDS)); %Vds=5mV

figure
clf
surf(V*1e3,Vgs,abs((Puiss-dIQ)./Puiss)','EdgeColor','none');
xlabel('Vds (mV)','FontSize',20);
ylabel('Vgs (V)','FontSize',20);
xlim([-5 5])
axis square tight
shading flat
colorbar('FontSize',20);
title('(Puiss-(IQD-IQG))/Puiss')




%% Puiss vs Puiss_Joule & Puiss_Peltier

[Z,ivg]=min(abs(Vgs-VGS));

figure
plot(V(1:5:end)*1e3,Puiss(1:5:end,ivg),'-rx','LineWidth',2)
hold on
plot(V(1:4:end)*1e3,Puiss_Joule(1:4:end,ivg)+Puiss_Seebeck(1:4:end,ivg),'--bo','LineWidth',2,'MarkerSize',5)
hold on
plot(V(1:4:end)*1e3,dIQ(1:4:end,ivg),'-o','color',[0 0.5 0],'LineWidth',2,'MarkerSize',4)
xlabel('drain voltage (mV)','FontName','Latin Modern Roman','FontSize',16)
ylabel('(pW)','FontName','Latin Modern Roman','FontSize',16)
legend('Vds \times I','I^2/G_e - V_{oc}\timesI','I_Q^D - I_Q^S','FontName',...
    'Latin Modern Roman','FontSize',12,'Location','north')
legend boxoff
%title(['Vgs=',num2str(VGS),'V'])

% figure
% clf
% surf(V*1e3,Vgs,((dIQ-(Puiss_Joule+Puiss_Seebeck))./dIQ),'EdgeColor','none');
% xlabel('Vds (mV)','FontSize',20);
% ylabel('Vgs (V)','FontSize',20);
% axis square tight
% shading flat
% colorbar('FontSize',20);
% %title('(IQD-IQS-(P_{Joule}+P_{Seebeck}))/(IQD-IQS)')
% title('écart relatif entre IQD-IQS et (I^2/Ge-Voc\timesI)')

figure
clf
surf(V*1e3,Vgs,(abs((Puiss-(Puiss_Joule+Puiss_Seebeck))./Puiss))','EdgeColor','none');
xlabel('drain voltage (mV)','FontSize',20,'FontName','Latin Modern Roman');
ylabel('gate voltage (V)','FontSize',20,'FontName','Latin Modern Roman');
axis square tight
shading flat
colorbar('FontSize',20,'FontName','Latin Modern Roman');
title('écart relatif entre Vds\timesI et (I^2/Ge-Voc\timesI)')