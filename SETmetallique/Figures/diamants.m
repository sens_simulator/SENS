% Caractéristiques en diamants à un T et un dT donnés

clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m diamètre si sphérique ou taille de l'arrête si cubique
hG=1.2e-9; %m épaisseur de l'oxyde de source
hD=1.5e-9; %m épaisseur de l'oxyde de drain
hg=5e-9; %m épaisseur de l'oxyde de grille
T = 100; %K % Témperature du dot
dT = 5; %K gradient de température

% dossier où se trouvent les scripts pour les calculs de courant
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';
%--------------------------------------------------------------------------

typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(dT),'.mat'])

Vgs2=linspace(Vgs(1),Vgs(end),600);
V2=linspace(V(1),V(end),600);
[VV2,VVgs2]=meshgrid(V2,Vgs2);[VV,VVgs]=meshgrid(V,Vgs);
I2=interp2(VV,VVgs,I,VV2,VVgs2);
Pn2=interp2(VV,VVgs,Pn,VV2,VVgs2);
I=I2;V=V2;Vgs=Vgs2;Pn=Pn2;

figure(30)
clf
surf(V,Vgs,I','EdgeColor','none');
xlabel('drain voltage (V)','FontSize',20);
ylabel('gate voltage (V)','FontSize',20);
set(gca,'FontSize',20);
axis square tight
shading flat

figure(31)
clf
pcolor(Vgs,V,I*1e6);
hold on
%plot([Vgs(1) Vgs(end)],[0.05 0.05],'k--','LineWidth',1.5)
xlabel('gate voltage (V)','FontSize',15,'FontName','Latin Modern Roman','FontSize',14);
ylabel('drain voltage (V)','FontSize',15,'FontName','Latin Modern Roman','FontSize',14);
c=colorbar;
c.Label.String='electronic current (\muA)';
c.Label.FontSize=20;
c.Label.FontName='Latin Modern Roman';
set(gca,'FontSize',20);
ax=gca;
ax.FontName='Latin Modern Roman';
axis  square tight 
shading flat
%title('Diamond diagram','FontName','Avenir','FontSize',20)
%colorbar('FontSize',20);
%title('electronic current (nA)','FontSize',20);

figure(32)
clf
pcolor(Vgs,V,Pn);
hold on
%plot([Vgs(1) Vgs(end)],[0.05 0.05],'k--','LineWidth',1.5)
xlabel('gate voltage (V)','FontSize',15,'FontName','Latin Modern Roman','FontSize',14);
ylabel('drain voltage (V)','FontSize',15,'FontName','Latin Modern Roman','FontSize',14);
c=colorbar;
c.Label.String='number of elecrons in the dot';
c.Label.FontSize=20;
c.Label.FontName='Latin Modern Roman';
set(gca,'FontSize',20);
ax=gca;
ax.FontName='Latin Modern Roman';
axis  square tight 
shading flat
%title('Diamond diagram','FontName','Avenir','FontSize',20)
%colorbar('FontSize',20);
%title('electronic current (nA)','FontSize',20);

