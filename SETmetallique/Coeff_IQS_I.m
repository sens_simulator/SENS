%--------------------------------------------------------------------------
%| Calcul des coefficients Peltier, et des coefficients de non-linéarité  |
%| bêtas, côté gauche et côté droit pour un SET  métallique (+qq figures).|
%|                                                                        |
%| Gianina Frimu, le 29/07/21                                             |
%--------------------------------------------------------------------------

clear
clc

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m        diamètre du dot
hG=1.2e-9; %m       épaisseur de l'oxyde côté source
hD=1.5e-9; %m       épaisseur de l'oxyde côté drain
hg=5e-9; %m         épaisseur de l'oxyde de grille
T =100; %K %        Témperature du dot
deltaT = [0 5]; %K  Gradient de température TG-TD
VGS = 3.5; %V      Tension de grille
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';
%--------------------------------------------------------------------------

typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];

for iT=1:length(deltaT)
    
    load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(deltaT(iT)),'.mat']);
    
    IQG_coeff=zeros(length(Vgs),3); % coeff régression quadratique 
    IQD_coeff=zeros(length(Vgs),3);
    dIQ_coeff=zeros(length(Vgs),3);
    
    betaG=zeros(1,length(Vgs)); % coefficient beta à gauche
    betaD=zeros(1,length(Vgs)); % coefficient beta à droite
    PeltierG=zeros(1,length(Vgs)); % coefficient de Peltier à gauche
    PeltierD=zeros(1,length(Vgs)); % coefficient de Peltier à droite
    Peltier=zeros(1,length(Vgs)); % (PeltierD+Peltier)/2
    
    Peltier_dIQ=zeros(1,length(Vgs)); 
    beta_dIQ=zeros(1,length(Vgs)); 
    IQD_poly=zeros(length(V),length(Vgs));
    IQG_poly=zeros(length(V),length(Vgs));
    
    dIQ=IQD-IQG;
    
    
    for ivg=1:length(Vgs)
        
        IQG_coeff(ivg,:)=polyfit(I(:,ivg)*1e9,IQG(:,ivg)*1e12,2);
        IQD_coeff(ivg,:)=polyfit(I(:,ivg)*1e9,IQD(:,ivg)*1e12,2);
        dIQ_coeff(ivg,:)=polyfit(I(:,ivg)*1e9,dIQ(:,ivg)*1e12,2);
        
        betaD(ivg)=IQD_coeff(ivg,1); %MOhms
        PeltierD(ivg)=IQD_coeff(ivg,2)/T; %mV/K
        
        betaG(ivg)=-IQG_coeff(ivg,1); %MOhms
        PeltierG(ivg)=IQG_coeff(ivg,2)/T; %mV/K
        
        Peltier(ivg)=(PeltierG(ivg)+PeltierD(ivg))/2; %mV/K
        
        % normalement: IQD-IQG=-alpha*dT*I+Ge*I^2
        
        Peltier_dIQ(ivg)=-dIQ_coeff(ivg,2)/(TG-TD); %mV/K 
        beta_dIQ(ivg)=dIQ_coeff(ivg,1); %MOhms, normalement égal à 1/Ge
        
        IQD_poly(:,ivg)=polyval(IQD_coeff(ivg,:),I(:,ivg)*1e9); %pW
        IQG_poly(:,ivg)=polyval(IQG_coeff(ivg,:),I(:,ivg)*1e9); %pW
    end
    
    save([dossier,typedispo,'/discret/Coeff_IQfit_T',num2str(T),'dT',num2str(deltaT(iT)),'.mat'],...
        'IQG_coeff','IQD_coeff','IQG_poly','IQD_poly','betaG','betaD','V','Vgs','Peltier');
end

%% IQ-I pour dT=5K

load([dossier,typedispo,'/discret/Coeff_IQfit_T',num2str(T),'dT5.mat'])
load([dossier,typedispo,'/discret/ZT_T',num2str(T),'dT5.mat'])

[Z,ivg]=min(abs(Vgs-VGS)); %Vgs(ivg)=VGS

IQm=(IQG+IQG)./2;

figure(61)
plot(I(:,ivg)*1e9,IQG(:,ivg)*1e12,'b',I(:,ivg)*1e9,...
    IQD(:,ivg)*1e12,'r',I(:,ivg)*1e9,IQm(:,ivg)*1e12,'k:');
hold on
%plot(I(:,ivg)*1e9,IQG_poly(:,ivg),'bx',I(:,ivg)*1e9,...
%    IQD_poly(:,ivg),'rx');
xlabel('courant électronique (nA)'); 
ylabel('courant de chaleur (pW)');
title(['I-IQ pour T=100K, dT=5K, Vgs=',num2str(VGS),'V']);
legend('IQS','IQD','(IQS+IQD)/2');
grid on


%% Comparaison Seebeck/Peltier
figure(43)
plot(alpha_ideal,alpha,alpha_ideal,Peltier,'x');
xlabel('alpha ideal (mV/K)');
ylabel('mV/K');
legend('Seebeck','Peltier')

figure(44)
plot(Vgs,alpha,Vgs,PeltierG,Vgs,PeltierD,Vgs,Peltier);
xlabel('gate voltage (V)');
ylabel('mV/K');
legend('Seebeck','PeltierG','PeltierD','(Peltier G+Peltier D)/2')

figure(444)
plot(Vgs,Peltier,Vgs,Peltier_dIQ)
xlabel('gate voltage (V)');
ylabel('mV/K');
legend('(Peltier G+Peltier D)/2','-(IQD-IQG)/(I\timesdT)')

Z=(Peltier'-alpha)./alpha;
figure(45)
plot(Vgs,Z);
xlabel('gate voltage (V)');
ylabel('écart relatif Seebeck Peltier');


Y=PeltierG./PeltierD;
figure(46)
plot(Vgs,Y)
xlabel('gate voltage (V)')
ylabel('PeltierG/PeltierD')

%% Comparaison des Ge

[Z,iv]=min(abs(V));

figure (100)
plot(Vgs,Ge(iv,:),Vgs,1./beta_dIQ,Vgs,1./(betaD+betaG),'x')
xlabel('Vgs (V)')
ylabel('\muS')
legend('dI/dVds','fit IQD-IQG','1/(\beta_G+\beta_D)')

figure
semilogy(Vgs,betaG,Vgs,betaD)
xlabel('Vgs (V)')
ylabel('M\Omega')
legend('\beta_G','\beta_D')

figure
plot(Vgs,abs(betaG./betaD))
xlabel('Vgs (V)')
ylabel('|\beta_G/\beta_D|')
