%--------------------------------------------------------------------------                                          |
%| Calcul de la tension en circuit ouvert, de la conductance électronique,| 
%| de la conductance thermique, du facteur de puissance, du coefficient   |
%| Seebeck et de la figure de mérite.                                     |                                    |
%|                                                                        |
%| Gianina Frimu, le 29/07/21                                             |
%--------------------------------------------------------------------------


clear

%--------------------------------------------------------------------------
% Paramètres à modifier
d=4.4e-9; %m        diamètre du dot
hG=1.2e-9; %m       épaisseur de l'oxyde côté source
hD=1.5e-9; %m       épaisseur de l'oxyde côté drain
hg=5e-9; %m         épaisseur de l'oxyde de grille
T =100; %K %        Témperature du dot
deltaT = [0 5 10]; %K  Gradient de température TG-TD
% Dossier où sont stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';

%--------------------------------------------------------------------------

typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',num2str(hg*1e10)];

% Calculs conductance électronique et coefficient de Seebeck idéal

load([dossier,typedispo,'/discret/I_T',num2str(T),'dT0.mat']);

Ge=zeros(length(V),length(Vgs)); % conductance électronique
I_coeff=zeros(length(Vgs),3); % Coefficients de la régression quadratique de I
Ge_coeff=zeros(length(Vgs),2); % Coefficients de la régression quadratique de Ge
Ge=zeros(length(V),length(Vgs));
alpha_ideal=zeros(length(Vgs),1);

n=0; Vds=0; % Calcul de DEGd pour n=0 (ok avec tout n) et Vds=0V

for ivg=1:length(Vgs)
    I_coeff(ivg,:)=polyfit(V*1e3,I(:,ivg)*1e9,2); %V en mV et I en nA
    Ge_coeff(ivg,:)=polyder(I_coeff(ivg,:)); % Ge=dI/dVds (dT=0K)
    Ge(:,ivg)=polyval(Ge_coeff(ivg,:),V*1e3); %microS
    DEGd(ivg)=qe/(2*Ceq).*((2*n+1)*qe-2*Cg*Vgs(ivg)+2*Vds*(CD+Cg)); %J
    alpha_ideal(ivg)=-DEGd(ivg)/2/qe/TG*1e3; %mV/K
end


% Calculs coefficient de Seebeck, conductance thermique, facteur de
% puissance et figure de mérite

for iT=2:length(deltaT) % Attention, ne pas inclure deltaT=0
    
    dT=deltaT(iT);
    
    load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(deltaT(iT)),'.mat']);
    
    alpha=zeros(length(Vgs),1); % coefficient de Seebeck
    KG=zeros(length(Vgs),1); % conductance thermique à gauche
    KD=zeros(length(Vgs),1); % conductance thermique à droite
    f=zeros(length(V),length(Vgs)); % facteur de puissance
    ZT=zeros(length(V),length(Vgs)); %figure de mérite
    Voc=zeros(length(Vgs),1); % tension en circuit ouvert I(Vds=-Voc)=0

    for ivg=1:length(Vgs)
        Voc(ivg)=-interp1(I(:,ivg)*1e9,V*1e3,0,'pchip'); %en mV
        if (-Voc(ivg)<V(1)*1e3||-Voc(ivg)>V(end)*1e3), Voc(ivg)=NaN; end
        alpha(ivg)=(Voc(ivg))/dT; %mV/K
        KG(ivg)=interp1(V*1e3,IQG(:,ivg)*1e12,-Voc(ivg),'pchip')/dT; %pW/K
        KD(ivg)=interp1(V*1e3,IQD(:,ivg)*1e12,-Voc(ivg),'pchip')/dT; %pW/K
        for iv=1:length(V)
            f(iv,ivg)=Ge(iv,ivg)*alpha(ivg)*alpha(ivg); %pW/K^2
            ZT(iv,ivg)=T*Ge(iv,ivg)*alpha(ivg)*alpha(ivg)/(KG(ivg));
        end
    end
    
    save([dossier,typedispo,'/discret/ZT_T',num2str(T),'dT',num2str(dT),'.mat'],...
    'Ge','Voc','KD','KG','f','ZT','dT','TG','TD','alpha','alpha_ideal',...
    'V','Vgs');
           
end



%% Ge, alpha pour dT=5K
load([dossier,typedispo,'/discret/I_T',num2str(T),'dT5.mat'])
load([dossier,typedispo,'/discret/ZT_T',num2str(T),'dT5.mat'])

[Z,iv]=min(abs(V));

figure(36)
plot(Vgs,Ge(iv,:));
xlabel('Vgs (V)');
ylabel('Ge (nS)');
title('Vds=0V');

figure(40)
plot(Vgs,alpha);
xlabel('Vgs (V)');
ylabel('alpha (mV/K)');

figure(43)
plot(alpha_ideal,alpha);
xlabel('alpha ideal (mV/K)');
ylabel('alpha (mV/K)');

figure(45)
plot(Vgs,f(iv,:));
xlabel('Vgs (V)')
ylabel('facteur de puissance (pW/K^2)')
%legend('5K','10K');

figure(49)
plot(Vgs,KD,Vgs,KG);
xlabel('Vgs (V)')
ylabel('K (pW/K)')
legend('KD','KG')

figure(50)
plot(Vgs,ZT(iv,:));
xlabel('Vgs (V)')
ylabel('ZT')

figure
plot(Vgs,Voc)
xlabel('Vgs (V)')
ylabel('Voc (mV)')

%% Voc en fonction de dT
VGS=3.5;
[Z,ivg]=min(abs(Vgs-VGS)); %Vgs(ivg)=3.55V
VocdT=zeros(1,length(deltaT));

for iT=2:length(deltaT)
    dT=deltaT(iT);
    load([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(dT),'.mat'])
load([dossier,typedispo,'/discret/ZT_T',num2str(T),'dT',num2str(dT),'.mat'])
VocdT(iT)=Voc(ivg,1); %mV

end

% figure(12)
% s=plot(deltaT,VocdT,'ro');
% set(s,'MarkerSize',8,'Marker','diamond','LineWidth',1.5,...
%     'LineStyle',':',...
%     'Color',[1 0 0]);
% xlabel('dT (K)')
% ylabel('Voc (mV)')
% title(['Vgs=',num2str(VGS),'V'])

figure
plot(deltaT,VocdT*1e3,'-or','LineWidth',2,'MarkerSize',4)
xlabel('temperature gradient (K)','FontName','Latin Modern Roman','FontSize',15)
ylabel('open circuit voltage (mV)','FontName','Latin Modern Roman','FontSize',15)


