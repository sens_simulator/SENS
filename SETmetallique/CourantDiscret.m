%--------------------------------------------------------------------------                                          |
%| Calcul du courant électronique et des flux de chaleur côté source et   |
%| côté drain pour un SET métallique par l'équation maîtresse. Un         |
%| gradient de température dT est appliqué entre la source et le drain.   |                                      |
%|                                                                        |
%| Gianina Frimu, le 29/07/21                                             |
%--------------------------------------------------------------------------

clear;close all;clc;tic;

%--------------------------------------------------------------------------
% Paramètres à modifier

N=3; %              nombre maximal d'électrons dans le dot
RG=1e6; %Ohm        Résistance tunnel côté gauche
RD=1.25e6; %Ohm       Résistance tunnel côté droit
d=4.4e-9; %m        diamètre du dot
hG=1.2e-9; %m       épaisseur de l'oxyde côté source
hD=1.5e-9; %m       épaisseur de l'oxyde côté drain
hg=5e-9; %m         épaisseur de l'oxyde de grille
T =100; %K %        Témperature du dot
deltaT = [0]; %K  Gradient de température TG-TD
% Dossier où vont être stockés les fichiers de données Matlab
dossier='/Users/gianina.frimu/Documents/data_SENS/SET_metallique/';

%--------------------------------------------------------------------------

% Création du dossier où vont être sauvegardées les données
typedispo=[num2str(d*1e10),'-',num2str(hG*1e10),'-',num2str(hD*1e10),'-',...
    num2str(hg*1e10)];
if isdir([dossier,typedispo,'/'])==0,mkdir([dossier,typedispo,'/']),end
if isdir([dossier,typedispo,'/discret'])==0,mkdir([dossier,typedispo,'/discret']),end


disp(['hG= ',num2str(hG*1e9),' nm'])
disp(['hD= ',num2str(hD*1e9),' nm'])
disp(['hg= ',num2str(hg*1e9),' nm'])
disp(['RG= ',num2str(RG*1e-6),' MOhms'])
disp(['RD= ',num2str(RD*1e-6),' MOhms'])
disp(['T= ',num2str(T),' K'])
disp(['dT= ',num2str(deltaT),' K'])

% Constantes
qe=1.602e-19; %C
kb=1.381e-23/qe; %eV/K
epsSiO2=3.8;
eps0=8.854e-12; %F/m
h=6.626e-34/qe; %eV.s
Efmetal=4.1; %eV électrodes en aluminium (la valeur n'a pas d'importance en vérité)

% Calcul des capacités sphère-plan
CG=capa(d/2,hG,epsSiO2);
CD=capa(d/2,hD,epsSiO2);
Cg=capa(d/2,hg,epsSiO2);

% Initialisation
Ceq=CG+CD+Cg;
n=(-N:N)'; % vecteur contenant le nombre d'électrons dans le dot
EfG = Efmetal; %eV niveau de Fermi côté gauche
tmax=10; % temps max pour le calcul par l'équation maîtresse
fermi = @(E,Ef,T) 1./(1+exp((E-Ef)./(kb*T)));

% Definition des tensions de grille
Vgs=linspace(-1.5*qe/Cg,1.5*qe/Cg,300); %V
%Vgs=linspace(0,10,300); %V
%Vgs=linspace(3,4,100);

% Definition des tensions de drain
V=linspace(-7e-3,7e-3,300); %V
%V=linspace(-2*qe/Ceq,2*qe/Ceq,100); %V

for iT=1:length(deltaT)
    
    dT=deltaT(iT);
    TG = T+dT/2; % Témperature de l'électrode gauche
    TD = T-dT/2; % Témperature de l'électrode droite
    Tdot=T; % Température dans le dot
    
    % Initialisation des vecteurs
    I=zeros(length(V),length(Vgs)); % courant électronique
    IQG=zeros(length(V),length(Vgs)); % flux de chaleur côté gauche
    IQD=zeros(length(V),length(Vgs)); % flux de chaleur côté droit
    IQm=zeros(length(V),length(Vgs)); % flux de chaleur moyen
    
    GammaGd=zeros(length(V),length(Vgs),length(n));
    GammadG=zeros(length(V),length(Vgs),length(n));
    GammaDd=zeros(length(V),length(Vgs),length(n));
    GammadD=zeros(length(V),length(Vgs),length(n));
    
    Pn=zeros(length(V),length(Vgs)); %  nombre d'électrons dans le dot
    
    for iv=1:length(V)
     
        EfD = EfG - V(iv); %eV niveau de Fermi côté droit
        
        for ivg=1:length(Vgs)
            
            %disp(['Vds=',num2str(V(iv)),'V,   Vgs=',num2str(Vgs(ivg)),'V'])
            
            % Energies à fournir pour chaque transition (G = gauche, D =
            % droite, d = dot)
            
            DEGd=1/(2*Ceq).*((2*n+1)*qe-2*Cg*Vgs(ivg)+2*V(iv)*(CD+Cg)); %eV
            DEdG=1/(2*Ceq).*(-(2*n-1)*qe+2*Cg*Vgs(ivg)-2*V(iv)*(CD+Cg));
            DEdD=1/(2*Ceq).*(-(2*n-1)*qe+2*Cg*Vgs(ivg)+2*V(iv)*CG);
            DEDd=1/(2*Ceq).*((2*n+1)*qe-2*Cg*Vgs(ivg)-2*V(iv)*CG);
            
            % Fréquences de transfert
            
            GGd=zeros(length(n),1);GQGd=zeros(length(n),1);
            GdG=zeros(length(n),1);GQdG=zeros(length(n),1);
            GDd=zeros(length(n),1);GQDd=zeros(length(n),1);
            GdD=zeros(length(n),1);GQdD=zeros(length(n),1);
            
            
            for in=2:length(n)
                
                % intervalle en énergie pour les calculs des fréquences de
                % transfert côté gauche
                epsGmin=min(EfG+DEGd(in-1),EfG); % Rq: DEGd(n-1)=-DEdG(in)
                epsGmax=max(EfG+DEGd(in-1),EfG);
                epsG=[epsGmin-10*kb*T:kb*T/50:epsGmax+10*kb*T];

                fGd=fermi(epsG,EfG,TG).*(1-fermi(epsG,EfG+DEGd(in-1),Tdot));
                GGd(in-1)=1/RG/qe*trapz(epsG,fGd); %Hz
                GQGd(in-1)=1/RG*trapz(epsG,(epsG-EfG).*fGd); %Hz/J
                
                fdG=fermi(epsG,EfG-DEdG(in),Tdot).*(1-fermi(epsG,EfG,TG));
                GdG(in)=1/RG/qe*trapz(epsG,fdG); %Hz
                GQdG(in)=1/RG*trapz(epsG,(epsG-EfG).*fdG); %Hz/J
                
                % intervalle en énergie pour les calculs des fréquences de
                % transfert côté droit
                epsDmin=min(EfD+DEDd(in-1),EfD); % Rq: DEDd(n-1)=-DEdD(in)
                epsDmax=max(EfD+DEDd(in-1),EfD);
                epsD=[epsDmin-10*kb*T:kb*T/50:epsDmax+10*kb*T];
                
                fDd=fermi(epsD,EfD,TD).*(1-fermi(epsD,EfD+DEDd(in-1),Tdot));
                GDd(in-1)=1/RD/qe*trapz(epsD,fDd); %Hz
                GQDd(in-1)=1/RD*trapz(epsD,(epsD-EfD).*fDd); %Hz/J
                
                fdD=fermi(epsD,EfD-DEdD(in),Tdot).*(1-fermi(epsD,EfD,TD));
                GdD(in)=1/RD/qe*trapz(epsD,fdD); %Hz
                GQdD(in)=1/RD*trapz(epsD,(epsD-EfD).*fdD); %Hz/J
                
            end
            
            Gt=GGd+GdG+GdD+GDd;
            
            % Calcul du courant par l'équation maîtresse
            
            % Création de la matrice GAMMA
            GAMMA=zeros(length(n),length(n));
            for ii=1:length(n)
                GAMMA(ii,ii)=-Gt(ii);
            end
            for ii=1:length(n)-1
                GAMMA(ii,ii+1)=GdD(ii+1)+GdG(ii+1);
                GAMMA(ii+1,ii)=GDd(ii)+GGd(ii);
            end
            

            % Résolution de l'équation maîtresse
            P0=zeros(length(n),1);P0(ceil(length(n)/2))=1;
            P=zeros(length(n),1);
            P=expm(GAMMA.*tmax)*P0;
            Pn(iv,ivg)=sum(P.*n);
            
            
            I(iv,ivg)=-qe*sum(P.*(GGd-GdG)); % somme sur le nombre d'électrons dans le dot
            IQG(iv,ivg)=sum(P.*(GQGd-GQdG));
            IQD(iv,ivg)=sum(P.*(GQdD-GQDd));
            
            GammaGd(iv,ivg,:)=GGd;
            GammadG(iv,ivg,:)=GdG;
            GammaDd(iv,ivg,:)=GDd;
            GammadD(iv,ivg,:)=GdD;
            
        end
    end

    save([dossier,typedispo,'/discret/I_T',num2str(T),'dT',num2str(dT),'.mat'],...
        'CD','Ceq','Cg','CG','d','eps0','epsSiO2','h','hD','hg','hG',...
        'I','IQD','IQG','kb','n','N','qe','RD','RG','T','TD','TG',...
        'dT','typedispo','V','Vgs','Pn','GammaGd','GammadG','GammaDd','GammadD');

end

t=toc;

disp(['Temps écoulé : ',num2str(t),' s'])
